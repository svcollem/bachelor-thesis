From iris.heap_lang Require Export notation lang.
From iris.proofmode Require Export proofmode.
From iris.heap_lang Require Import proofmode.
From iris.prelude Require Import options.

Section spec.

  Reserved Infix "≺" (at level 70, no associativity).
  Structure chainable_barrier_spec Σ `{!heapGS Σ} (N : namespace) := chainable_barrier {
    (* Operations *)
    new_barrier : val;
    signal : val;
    wait : val;
    extend : val;
    clone : val;
    (* Predicates *)
    send (b : val) (P : iProp Σ) : iProp Σ;
    recv (b : val) (P : iProp Σ) : iProp Σ;
    earlier : val -> val -> iProp Σ
      where "x ≺ y" := (earlier x y) : stdpp_scope;
    (* General properties of the predicates *)
    earlier_persistent (b1 b2 : val) : Persistent (b1 ≺ b2);
    earlier_trans (b1 b2 b3 : val) : b1 ≺ b2 -∗ b2 ≺ b3 -∗ b1 ≺ b3;
    earlier_timeless (b1 b2 : val) : Timeless (b1 ≺ b2);
    renunciation (b1 b2 : val) (P Q : iProp Σ) (E : coPset) :
      ↑N ⊆ E →
      recv b1 P -∗ send b2 Q -∗ b1 ≺ b2
        ={E}=∗ send b2 (P -∗ Q);
    recv_split (b : val) (P1 P2 : iProp Σ) (E : coPset) :
      ↑N ⊆ E → recv b (P1 ∗ P2) ={E}=∗ recv b P1 ∗ recv b P2;
    recv_weaken (b : val) (P Q : iProp Σ) :
      (P -∗ Q) -∗ recv b P -∗ recv b Q;
    send_strengthen (b : val) (P Q : iProp Σ) :
      (Q -∗ P) -∗ send b P -∗ send b Q;
    (* Program specs *)
    new_barrier_spec (P : iProp Σ) :
      {{{ True }}}
        new_barrier #()
      {{{ b, RET b; send b P ∗ recv b P }}};
    signal_spec (b : val) (P : iProp Σ) :
      {{{ send b P ∗ P }}}
        signal b
      {{{ RET #(); True }}};
    wait_spec (b : val) (P : iProp Σ) :
      {{{ recv b P }}}
        wait b
      {{{ RET #(); P }}};
    extend_spec (Q : iProp Σ) (E : gset val) (P : iProp Σ) (b : val) :
      {{{ send b P ∗ ([∗ set] e ∈ E, e ≺ b) }}}
        extend b
      {{{ (b' : val), RET b';
          send b' Q ∗ recv b' Q ∗ send b P ∗ b' ≺ b ∗ ([∗ set] e ∈ E, e ≺ b') }}};
    clone_spec (b : val) (P1 P2 : iProp Σ) :
      {{{ send b (P1 ∗ P2) }}} clone b {{{ RET #(); send b P1 ∗ send b P2 }}};
  }.
  Global Existing Instance earlier_persistent.
  Global Existing Instance earlier_timeless.

End spec.
