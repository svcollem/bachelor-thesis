From iris.heap_lang Require Export notation lang.
From iris.proofmode Require Export proofmode.
From iris.heap_lang Require Import proofmode.
From iris.algebra Require Import auth excl gmap gset.
From iris.base_logic Require Import invariants saved_prop.
From iris.prelude Require Import options.

From barriers.chainable Require Import order_auth specification.

Set Printing Projections.
Unset Printing Records.

(* Iris implementation of barrier with receive splitting and chains. 
   This implementation is based on [1].
   
   [1]: Mike Dodds, Suresh Jagannathan, Matthew J. Parkinson, Kasper Svendsen,
        and Lars Birkedal. 2016. Verifying Custom Synchronization Constructs
        Using Higher-Order Separation Logic. ACM Trans. Program. Lang. Syst. 38,
        2, Article 4 (January 2016), 72 pages. https://doi.org/10.1145/2818638 *)

Definition new_barrier : val :=
  λ: <>,
    let: "l" := AllocN #2 #() in
    "l" <- #1;;
    "l" +ₗ #1 <- NONEV;;
    "l".

Definition signal : val :=
  λ: "b",
    FAA "b" #(-1);;
    #().

Definition wait : val :=
  rec: "wait" "b" :=
    if: ! "b" = #0
      then
        match: ! ("b" +ₗ #1) with
          NONE => #()
        | SOME "b'" => "wait" "b'"
        end
      else "wait" "b".

Definition extend : val :=
  rec: "extend" "b" :=
    let: "b'" := ! ("b" +ₗ #1) in
    let: "l" := AllocN #2 #() in
    "l" <- #1;;
    "l" +ₗ #1 <- "b'";;
    if: CAS ("b" +ₗ #1) "b'" (SOME "l")
      then "l"
      else
        Free "l";;
        Free ("l" +ₗ #1);;
        "extend" "b".

Definition clone : val :=
  λ: "b",
    FAA "b" #1;;
    #().

Class barrierG Σ := BarrierG {
  barrier_gnamesG :> inG Σ (authR (gmapUR loc (gset_disjR gnameO)));
  barrier_orderG :> order_authG Σ loc;
  barrier_savedPropG :> savedPropG Σ;
}.

Definition barrierΣ : gFunctors :=
  #[ GFunctor (authR (gmapUR loc (gset_disjR gnameO)));
     order_authΣ loc;
     savedPropΣ ].

Global Instance subG_barrierΣ {Σ} : subG barrierΣ Σ → barrierG Σ.
Proof. solve_inG. Qed.

Section definitions.
  Context `{!heapGS Σ, !barrierG Σ}.

  Inductive node := mk_node {
    location : loc;
    γRs : gset gname; (* Identifiers of the resources we still can receive *)
    γPs : gset gname; (* Identifiers of the resources we still have to send *)
    prev : option node;
  }.
  Global Instance node_inhabited : Inhabited node :=
    populate (mk_node inhabitant inhabitant inhabitant inhabitant).

  Fixpoint node_prev_ind
    (P : node → Prop)
    (Hbase : ∀ l' γRs' γPs', P (mk_node l' γRs' γPs' None))
    (IH : ∀ n' l' γRs' γPs', P n' → P (mk_node l' γRs' γPs' (Some n')))
    (n : node)
    : P n.
  Proof.
    destruct n. destruct prev0 as [n'|]; last first.
    - apply Hbase.
    - apply IH.
      apply (node_prev_ind P Hbase IH).
  Defined.

  Record barrier_name := mk_barrier_name {
    barrier_name_sends : gname;
    barrier_name_recvs : gname;
    barrier_name_order : gname;
  }.
  Global Instance barrier_name_eq_dec : EqDecision barrier_name.
  Proof. solve_decision. Defined.
  Global Instance barrier_name_countable : Countable barrier_name.
  Proof.
    set (enc γ := (γ.(barrier_name_sends), γ.(barrier_name_recvs), γ.(barrier_name_order))).
    set (dec p := let '(γ1, γ2, γ3) := p in mk_barrier_name γ1 γ2 γ3).
    apply (inj_countable' enc dec). by intros [].
  Qed.

  Fixpoint used_γRs (n : node) : gset gname :=
    n.(γRs) ∪ default ∅ (used_γRs <$> n.(prev)).

  Fixpoint γRs_disjoint (n : node) : Prop :=
    match n.(prev) with
    | None => True
    | Some n' => n.(γRs) ## used_γRs n' ∧ γRs_disjoint n'
    end.

  Fixpoint resources(n : node) (acc : iProp Σ) : iProp Σ :=
    let acc' := (
        ([∗ set] γP ∈ n.(γPs), ∃ (P : iProp Σ),
          saved_prop_own γP DfracDiscarded P ∗ ▷ P) -∗
        ([∗ set] γR ∈ n.(γRs), ∃ (R : iProp Σ),
          saved_prop_own γR DfracDiscarded R ∗ ▷ R)
        ∗ acc)%I
      in match n.(prev) with
      | None      => acc'
      | Some prev => resources prev acc'
      end.

  Fixpoint to_locations (n : node) : list loc :=
    n.(location) :: default [] (to_locations <$> n.(prev)).

  Fixpoint to_γPs (n : node) : gmap loc (gset_disj gname) :=
    <[n.(location) := GSet n.(γPs)]>
      (default ∅ (to_γPs <$> n.(prev))).

  Fixpoint to_γRs (n : node) : gmap loc (gset_disj gname) :=
    <[n.(location) := GSet n.(γRs)]>
      (default ∅ (to_γRs <$> n.(prev))).

  Fixpoint to_counter_prevs (n : node) : gmap loc (nat * option loc) :=
    <[n.(location) := (size n.(γPs), location <$> n.(prev))]>
      (default ∅ (to_counter_prevs <$> n.(prev))).

  Definition option_loc_to_val (o : option loc) : val :=
    match o with
    | None => NONEV
    | Some l => SOMEV #l
    end.

  Definition counter_to_dfrac (c : nat) : dfrac :=
    match c with
    | 0   => DfracDiscarded
    | S _ => DfracOwn 1
    end.

  Definition node_chain (n : node) : iProp Σ :=
    [∗ map] l ↦ counter_prev ∈ to_counter_prevs n,
        l ↦{counter_to_dfrac counter_prev.1} #counter_prev.1
        ∗ (l +ₗ 1) ↦ option_loc_to_val counter_prev.2.

  Definition barrierN := nroot .@ "chainable_barrier".
  Definition barrier_inv (γ : barrier_name) : iProp Σ :=
    ∃ (n : node),
      ⌜γRs_disjoint n⌝
      ∗ own γ.(barrier_name_sends) (● to_γPs n)
      ∗ own γ.(barrier_name_recvs) (● to_γRs n)
      ∗ ●≺[γ.(barrier_name_order)] (to_locations n)
      ∗ node_chain n
      ∗ resources n True.

  (* Note that we assert `l' ◯≺ l`,
     as to_locations returns a reversed list,
     i.e. the "earliest location" is last in the list. *)
  Definition earlier (b b' : val) : iProp Σ :=
    ∃ (γ : barrier_name) (l l' : loc),
      ⌜b = #l⌝ ∗ ⌜b' = #l'⌝
      ∗ meta l barrierN γ
      ∗ meta l' barrierN γ
      ∗ l' ◯≺[γ.(barrier_name_order)] l.

  Definition send (b : val) (P : iProp Σ) : iProp Σ :=
    ∃ (γ : barrier_name) (l : loc) (γP : gname) (P' : iProp Σ),
      ⌜b = #l⌝
        ∗ own γ.(barrier_name_sends) (◯ {[ l := GSet {[γP]} ]})
        ∗ (P -∗ P')
        ∗ saved_prop_own γP DfracDiscarded P'
        ∗ meta l barrierN γ
        ∗ inv barrierN (barrier_inv γ).

  Definition recv (b : val) (R : iProp Σ) : iProp Σ :=
    ∃ (γ : barrier_name) (l : loc) (γR : gname) (R' : iProp Σ),
      ⌜b = #l⌝
        ∗ own γ.(barrier_name_recvs) (◯ {[ l := GSet {[γR]} ]})
        ∗ (R' -∗ R)
        ∗ saved_prop_own γR DfracDiscarded R'
        ∗ meta l barrierN γ
        ∗ inv barrierN (barrier_inv γ).
End definitions.

Infix "≺" := earlier (at level 70) : stdpp_scope.
Notation "(≺)" := earlier (only parsing) : stdpp_scope.

Section boring_lemmas.
  Context `{!heapGS Σ, !barrierG Σ}.

  Lemma wp_alloc_2 :
    {{{ True }}} (AllocN #2 #()) {{{ l, RET #l; l ↦ #() ∗ (l +ₗ 1) ↦ #() ∗ meta_token l ⊤ }}}.
  Proof.
    iIntros (Φ) "_ HΦ".
    wp_apply (wp_allocN with "[//]"); first lia.
    iIntros (l) "[Hl [Hmeta _]]". iApply "HΦ".
    iDestruct (array_cons with "Hl") as "[$ Hl]".
    rewrite array_singleton loc_add_0. iFrame.
  Qed.

  Lemma dom_to_γPs (l : loc) (n : node) :
    l ∈ dom (to_γPs n) ↔ l ∈ to_locations n.
  Proof.
    induction n using node_prev_ind; simpl;
      rewrite dom_insert_L; [rewrite dom_empty_L|]; set_solver.
  Qed.

  Lemma dom_to_γRs (l : loc) (n : node) :
    l ∈ dom (to_γRs n) ↔ l ∈ to_locations n.
  Proof.
    induction n using node_prev_ind; simpl;
      rewrite dom_insert_L; [rewrite dom_empty_L|]; set_solver.
  Qed.

  Lemma dom_to_counter_prevs (l : loc) (n : node) :
    l ∈ dom (to_counter_prevs n) ↔ l ∈ to_locations n.
  Proof.
    induction n using node_prev_ind; simpl;
      rewrite dom_insert_L; [rewrite dom_empty_L|]; set_solver.
  Qed.

  Lemma NoDup_lookup_neq {A : Type} (l : list A) (i j : nat) (x y : A) :
    NoDup l → l !! i = Some x → l !! j = Some y → i ≠ j → x ≠ y.
  Proof.
    intros H Hi Hj Hneq ->.
    apply Hneq. apply (NoDup_lookup l i j y H Hi Hj).
  Qed.

  Lemma to_counter_prevs_lookup_sender (n : node) (l : loc) (γP γ : gname) :
    own γ (● to_γPs n) -∗
    own γ (◯ {[l := GSet {[γP]}]}) -∗
    ∃ (c : nat) (prev : option loc),
      ⌜to_counter_prevs n !! l = Some (S c, prev)⌝.
  Proof.
    iIntros "H● H◯".
    iDestruct (own_valid_2 with "H● H◯")
      as %[([γPs|] & Hlookup & Hincl)%singleton_included_l
            Hvalid]%auth_both_valid_discrete; last first.
    { by apply lookup_valid_Some in Hlookup as []. }
    fold_leibniz. iPureIntro. clear Hvalid.
    rewrite Some_included_total gset_disj_included in Hincl.
    apply subseteq_size in Hincl as Hle.
    rewrite size_singleton in Hle.
    destruct (size γPs) as [|c] eqn:Hsize; first lia.
    exists c. rewrite -Hsize.
    induction n using node_prev_ind; simpl in *.
    - rewrite insert_empty lookup_singleton_Some in Hlookup.
      destruct Hlookup as [<- [= <-]].
      exists None. by rewrite insert_empty lookup_singleton.
    - rewrite lookup_insert_Some in Hlookup.
      destruct Hlookup as [[<- [= <-]] | [Hneq Hlookup]].
      + exists (Some n.(location)). by rewrite lookup_insert.
      + rewrite lookup_insert_ne; [|apply Hneq].
        by apply IHn.
  Qed.

  Lemma to_γPs_lookup (l : loc) (γ : gname) (γPs' : gset gname) (n : node) :
    own γ (● to_γPs n) -∗
    own γ (◯ {[l := GSet γPs']}) -∗
      ∃ γPs, ⌜γPs' ⊆ γPs⌝ ∗ ⌜to_γPs n !! l = Some (GSet γPs)⌝.
  Proof.
    iIntros "H● H◯".
    iDestruct (own_valid_2 with "H● H◯")
      as %[([γPs|] & Hlookup & Hincl)%singleton_included_l
            Hvalid]%auth_both_valid_discrete; last first.
    { by apply lookup_valid_Some in Hlookup as []. }
    iPureIntro. rewrite Some_included_total gset_disj_included in Hincl.
    exists γPs. by fold_leibniz.
  Qed.

  Lemma to_γRs_lookup (l : loc) (γ : gname) (γRs' : gset gname) (n : node) :
    own γ (● to_γRs n) -∗
    own γ (◯ {[l := GSet γRs']}) -∗
      ∃ γRs, ⌜γRs' ⊆ γRs⌝ ∗ ⌜to_γRs n !! l = Some (GSet γRs)⌝.
  Proof.
    iIntros "H● H◯".
    iDestruct (own_valid_2 with "H● H◯")
      as %[([γRs|] & Hlookup & Hincl)%singleton_included_l
            Hvalid]%auth_both_valid_discrete; last first.
    { by apply lookup_valid_Some in Hlookup as []. }
    iPureIntro. rewrite Some_included_total gset_disj_included in Hincl.
    exists γRs. by fold_leibniz.
  Qed.

  Lemma lookup_prev_γRs (l prev : loc) (n : node) :
    NoDup (to_locations n) →
    (∃ i : nat, to_locations n !! i = Some l ∧ to_locations n !! S i = Some prev) →
    ∃ γRs : gset gname, to_γRs n !! prev = Some (GSet γRs).
  Proof.
    intros Hnodup (i & Hl & Hprev).
    generalize dependent i.
    induction n using node_prev_ind; simpl; intros.
    { by rewrite lookup_nil in Hprev. }
    destruct i; simpl in *.
    - destruct n; simpl in *.
      injection Hl as ->.
      injection Hprev as ->.
      exists γRs0.
      rewrite lookup_insert_ne; last first.
      { eapply (NoDup_lookup_neq _ 0 1 _ _ Hnodup); done. }
      by rewrite lookup_insert.
    - simpl in Hnodup. edestruct IHn as [γRs Hlookup].
      { by eapply NoDup_cons_1_2. }
      { apply Hl. }
      { apply Hprev. }
      exists γRs. rewrite lookup_insert_ne; [apply Hlookup|].
      apply (NoDup_lookup_neq _ 0 (S (S i)) _ _ Hnodup); done.
  Qed.

  Lemma to_locations_prev_node (l prev : loc) (c : nat) (n : node) :
    to_counter_prevs n !! l = Some (c, Some prev) →
    ∃ i : nat, to_locations n !! i = Some l ∧ to_locations n !! S i = Some prev.
  Proof.
    induction n as [|n'] using node_prev_ind; simpl.
    { rewrite insert_empty lookup_singleton_Some.
      intros [_ [=]]. }
    rewrite lookup_insert_Some.
    intros [[<- [= <- <-]] | [Hneq Hlookup]].
    - exists 0. split; first auto.
      destruct n' as [l'' γRs'' γPs'' n''].
      reflexivity.
    - destruct (IHn Hlookup) as (i & Hl & Hprev).
      by exists (S i).
  Qed.

  Lemma γRs_are_used (l : loc) (γRs : gset gname) (n : node) :
    to_γRs n !! l = Some (GSet γRs) →
    γRs ⊆ used_γRs n.
  Proof.
    induction n using node_prev_ind; simpl.
    - rewrite insert_empty lookup_singleton_Some. set_solver.
    - rewrite lookup_insert_Some. set_solver.
  Qed.

  Lemma γRs_are_disjoint (l l' : loc) (γRs γRs' : gset gname) (n : node) :
    l ≠ l' →
    to_γRs n !! l = Some (GSet γRs) →
    to_γRs n !! l' = Some (GSet γRs') →
    γRs_disjoint n →
    γRs ## γRs'.
  Proof.
    intros Hneq HγRs HγRs' Hdisjoint.
    induction n using node_prev_ind; simpl in *.
    { rewrite !insert_empty !lookup_singleton_Some in HγRs HγRs'.
      destruct HγRs as [<- _]. destruct HγRs' as [<- _].
      by destruct Hneq. }
    rewrite !lookup_insert_Some in HγRs HγRs'.
    destruct HγRs as [[-> [= ->]] | [Hneq' HγRs]].
    - destruct HγRs' as [[[]%Hneq _] | [_ HγRs']].
      destruct Hdisjoint as [Hdisjoint _].
      apply γRs_are_used in HγRs'. set_solver.
    - destruct HγRs' as [[-> [= ->]] | [_ HγRs']].
      + destruct Hdisjoint as [Hdisjoint _].
        apply γRs_are_used in HγRs. set_solver.
      + destruct Hdisjoint as [_ Hdisjoint]. by apply IHn.
  Qed.

  Lemma optional_locs_compare_safe (o o' : option loc) :
    vals_compare_safe (option_loc_to_val o) (option_loc_to_val o').
  Proof. destruct o; destruct o'; simpl; auto. Qed.

  Global Instance option_loc_to_val_inj : Inj (=) (=) option_loc_to_val.
  Proof. intros [] []; try intros [= ->]; auto. Qed.
End boring_lemmas.

Section earlier.
  Context `{!heapGS Σ, !barrierG Σ}.

  Lemma earlier_trans (b b' b'' : val) :
    b ≺ b' -∗ b' ≺ b'' -∗ b ≺ b''.
  Proof.
    iDestruct 1 as (γ l l' -> ->) "(#Hl_meta & #Hl'_meta & H)".
    iDestruct 1 as (γ' ? l'' [= <-] ->) "(#Hl'_meta' & #Hl''_meta & H')".
    iDestruct (meta_agree with "Hl'_meta Hl'_meta'") as %<-.
    iExists γ, l, l''. repeat iSplit; auto.
    iApply (order_own_ordered_trans with "H' H").
  Qed.

  Global Instance earlier_persistent (b b' : val) :
    Persistent (b ≺ b').
  Proof. apply _. Qed.

  Global Instance earlier_timeless (b b' : val) :
    Timeless (b ≺ b').
  Proof. apply _. Qed.
End earlier.

Section node_chain.
  Context `{!heapGS Σ, !barrierG Σ}.

  Global Instance node_chain_timeless (n : node) :
    Timeless (node_chain n).
  Proof. apply _. Qed.

  Lemma node_chain_singleton (l : loc) (γRs : gset gname) (γP : gname) :
    node_chain (mk_node l γRs {[γP]} None) ⊣⊢
      l ↦ #1 ∗ (l +ₗ 1) ↦ NONEV.
  Proof.
    rewrite /node_chain. cbn.
    by rewrite size_singleton insert_empty big_sepM_singleton.
  Qed.

  Lemma node_chain_lookup_acc (l : loc) (c : nat) (prev : option loc) (n : node) :
    to_counter_prevs n !! l = Some (c, prev) →
    node_chain n -∗
    let dq := counter_to_dfrac c in
      l ↦{dq} #c
      ∗ (l +ₗ 1) ↦ option_loc_to_val prev
      ∗ match c with
        | 0   => (l +ₗ 1) ↦ option_loc_to_val prev -∗
                 node_chain n
        | S _ =>
            l ↦ #c -∗
            (l +ₗ 1) ↦ option_loc_to_val prev -∗
            node_chain n
        end.
  Proof.
    iIntros (Hlookup) "Hchain".
    iDestruct (big_sepM_lookup_acc with "Hchain")
      as "[[Hl_counter Hl_prev] Hchain]"; first apply Hlookup.
    destruct c; simpl.
    - iDestruct "Hl_counter" as "#Hl_counter".
      iFrame "Hl_counter Hl_prev".
      iIntros "Hl_prev".
      iApply "Hchain". by iFrame.
    - iFrame "Hl_counter Hl_prev".
      iIntros "Hl_prev Hl_counter".
      iApply "Hchain". by iFrame.
  Qed.
End node_chain.

Section resources.
  Context `{!heapGS Σ, !barrierG Σ}.

  Lemma resources_weaken (n : node) (Q R : iProp Σ) :
    resources n Q -∗ (Q -∗ R) -∗ resources n R.
  Proof.
    iIntros "Hres H".
    iInduction n as [|] "IH" using node_prev_ind forall (Q R); simpl.
    - iIntros "HPs".
      iDestruct ("Hres" with "HPs") as "[HRs HQ]".
      iFrame "HRs". by iApply "H".
    - iApply ("IH" with "Hres").
      iIntros "Hres HPs".
      iDestruct ("Hres" with "HPs") as "[HRs HQ]".
      iFrame "HRs". by iApply "H".
  Qed.

  Lemma resources_singleton (n : node) :
    n.(prev) = None →
    resources n True ⊣⊢
      (([∗ set] γP ∈ n.(γPs), ∃ (P : iProp Σ),
          saved_prop_own γP DfracDiscarded P ∗ ▷ P) -∗
      ([∗ set] γR ∈ n.(γRs), ∃ (R : iProp Σ),
        saved_prop_own γR DfracDiscarded R ∗ ▷ R)).
  Proof.
    destruct n; simpl; intros ->.
    by rewrite bi.sep_True.
  Qed.
End resources.

Section new_barrier_spec.
  Context `{!heapGS Σ, !barrierG Σ}.

  Lemma new_barrier_spec (P : iProp Σ) :
    {{{ True }}}
      new_barrier #()
    {{{ b, RET b; send b P ∗ recv b P }}}.
  Proof.
    iIntros (Φ) "_ HΦ".
    wp_lam. wp_apply (wp_alloc_2 with "[//]").
    iIntros (l) "(Hl_counter & Hl_prev & Hmeta)".
    wp_let. wp_store. wp_store. iApply "HΦ".
    iMod (saved_prop_alloc P DfracDiscarded) as (γP) "#HγP"; first done.
    set (n := mk_node l {[γP]} {[γP]} None).
    iMod (own_alloc (● to_γPs n ⋅ ◯ {[l := GSet {[γP]}]}))
      as (γs) "[H●s H◯s]".
    { apply auth_both_valid_discrete. simpl.
      by rewrite insert_empty singleton_valid. }
    iMod (own_alloc (● to_γRs n ⋅ ◯ {[l := GSet {[γP]}]})) as
      (γr) "[H●r H◯r]".
    { apply auth_both_valid_discrete. simpl.
      by rewrite insert_empty singleton_valid. }
    iMod (order_own_auth_alloc [l]) as (γo) "H●≺".
    { apply NoDup_singleton. }
    set (γ := mk_barrier_name γs γr γo).
    iMod (meta_set _ l γ barrierN with "Hmeta") as "#Hmeta"; first done.
    iMod (inv_alloc barrierN _ (barrier_inv γ)
      with "[H●s H●r H●≺ Hl_counter Hl_prev]") as "#Hinv".
    { iExists n. iFrame "H●s H●r H●≺". iSplitL.
      { iApply node_chain_singleton. iFrame. }
      iApply resources_singleton; first done; simpl.
      rewrite big_sepS_singleton. auto. }
    iSplitL "H◯s".
    - iExists γ, l, γP, P. auto 10 with iFrame.
    - iExists γ, l, γP, P. auto 10 with iFrame.
  Qed.
End new_barrier_spec.

Section signal_spec.
  Context `{!heapGS Σ, !barrierG Σ}.

  (* We signal a node by providing a resource P,
     meaning we can remove γP from the set of
     resource identifiers we still have to wait on. *)
  Fixpoint signal_node (l : loc) (γP : gname) (n : node) : node :=
    if bool_decide (l = n.(location))
      then mk_node n.(location) n.(γRs) (n.(γPs) ∖ {[γP]}) n.(prev)
      else mk_node n.(location) n.(γRs)  n.(γPs)           (signal_node l γP <$> n.(prev)).

  Lemma signal_node_location (l : loc) (γP : gname) (n : node) :
    (signal_node l γP n).(location) = n.(location).
  Proof.
    destruct n. simpl. destruct (bool_decide (l = location0)); reflexivity.
  Qed.

  Lemma to_γRs_signal_node (l : loc) (γP : gname) (n : node) :
    to_γRs (signal_node l γP n) = to_γRs n.
  Proof.
    induction n using node_prev_ind; simpl.
    - destruct (bool_decide (l = l')); reflexivity.
    - destruct (bool_decide (l = l')); simpl;
        [|rewrite IHn]; reflexivity.
  Qed.

  Lemma to_locations_signal_node (l : loc) (γP : gname) (n : node) :
    to_locations (signal_node l γP n) = to_locations n.
  Proof.
    induction n using node_prev_ind; simpl.
    - destruct (bool_decide (l = l')); reflexivity.
    - destruct (bool_decide (l = l')); simpl;
        [|rewrite IHn]; reflexivity.
  Qed.

  Lemma to_counter_prevs_signal_node (l : loc) (c : nat)
      (γP : gname) (γPs : gset gname) (prev : option loc) (n : node) :
    to_γPs n !! l = Some (GSet γPs) →
    γP ∈ γPs →
    to_counter_prevs n !! l = Some (S c, prev) →
    to_counter_prevs (signal_node l γP n) =
      <[l := (c, prev)]> (to_counter_prevs n).
  Proof.
    intros HγPs HγP Hlookup. induction n using node_prev_ind; simpl in *.
    - rewrite !insert_empty !lookup_singleton_Some in HγPs, Hlookup.
      destruct HγPs as [<- [= <-]].
      destruct Hlookup as [_ [= Hsize <-]].
      rewrite bool_decide_eq_true_2; last auto; simpl.
      rewrite insert_insert. rewrite size_difference; last set_solver.
      rewrite Hsize size_singleton. simpl. by rewrite Nat.sub_0_r.
    - rewrite lookup_insert_Some in HγPs.
      destruct HγPs as [[<- [= <-]] | [Hneq HγPs]].
      + rewrite lookup_insert in Hlookup.
        injection Hlookup as Hsize <-.
        rewrite bool_decide_eq_true_2; last auto; simpl.
        rewrite insert_insert. rewrite size_difference; last set_solver.
        rewrite Hsize size_singleton. simpl. by rewrite Nat.sub_0_r.
      + rewrite lookup_insert_ne in Hlookup; last auto.
        rewrite bool_decide_eq_false_2; last auto; simpl.
        rewrite insert_commute; last auto.
        rewrite signal_node_location. by rewrite IHn.
  Qed.

  Lemma to_γPs_signal_node (l : loc) (γP : gname) (γPs : gset gname) (n : node) :
    to_γPs n !! l = Some (GSet γPs) →
    to_γPs (signal_node l γP n)
      = <[l := GSet (γPs ∖ {[γP]})]> (to_γPs n).
  Proof.
    intros Hlookup. induction n using node_prev_ind; simpl in *.
    - rewrite insert_empty lookup_singleton_Some in Hlookup.
      destruct Hlookup as [<- [= <-]].
      rewrite bool_decide_eq_true_2; last done; simpl.
      by rewrite insert_insert insert_empty.
    - destruct (decide (l = l')) as [<- | Hneq].
      + rewrite bool_decide_eq_true_2; last done; simpl.
        rewrite lookup_insert in Hlookup.
        injection Hlookup as <-.
        by rewrite insert_insert.
      + rewrite bool_decide_eq_false_2; last done; simpl.
        rewrite lookup_insert_ne in Hlookup; last done.
        rewrite insert_commute; last done.
        rewrite IHn; auto with set_solver.
  Qed.

  Lemma update_signal_node (n : node) (l : loc) (γP : gname) (γ : gname) :
    own γ (● to_γPs n) -∗
    own γ (◯ {[l := GSet {[γP]}]}) ==∗
    own γ (● to_γPs (signal_node l γP n)).
  Proof.
    iIntros "H● H◯".
    iDestruct (to_γPs_lookup with "H● H◯") as %(γPs & Hsubset & Hlookup).
    iMod (own_update_2 _ _ _ (● _ ⋅ ◯ _)
      with "H● H◯") as "[H● _]"; last iApply "H●".
    rewrite (to_γPs_signal_node _ _ _ _ Hlookup).
    apply auth_update. eapply insert_local_update.
    - apply Hlookup.
    - by rewrite lookup_singleton.
    - apply gset_disj_dealloc_local_update.
  Qed.

  Lemma signal_node_resources (l : loc) (γP : gname) (γPs : gset gname)
      (n : node) (P Qacc : iProp Σ) :
    to_γPs n !! l = Some (GSet γPs) →
    γP ∈ γPs →
    saved_prop_own γP DfracDiscarded P -∗
    P -∗
    resources n Qacc -∗
    resources (signal_node l γP n) Qacc.
  Proof.
    iIntros (Hlookup Hin) "#HγP HP Hres".
    iInduction n as [|n'] "IH" using node_prev_ind forall (Qacc); simpl in *.
    - rewrite insert_empty lookup_singleton_Some in Hlookup.
      destruct Hlookup as [<- [= <-]].
      rewrite bool_decide_eq_true_2; auto; simpl.
      iIntros "HPs". iApply "Hres".
      iApply big_sepS_delete; first apply Hin.
      iFrame "HPs". iExists P. by iFrame.
    - rewrite lookup_insert_Some in Hlookup.
      destruct Hlookup as [[<- [= <-]] | [Hneq Hlookup]].
      + rewrite bool_decide_eq_true_2; last auto; simpl.
        iApply (resources_weaken with "Hres").
        iIntros "Hres HPs". iApply "Hres".
        iApply big_sepS_delete; first apply Hin.
        iFrame "HPs". iExists P. by iFrame.
      + rewrite bool_decide_eq_false_2; last auto; simpl.
        iApply ("IH" $! Hlookup with "HP Hres").
  Qed.

  Lemma node_chain_signal_acc (l : loc) (c : nat) (prev : option loc)
      (γP : gname) (γPs : gset gname) (n : node) :
    to_γPs n !! l = Some (GSet γPs) →
    γP ∈ γPs →
    to_counter_prevs n !! l = Some (S c, prev) →
    node_chain n -∗
    l ↦ #(S c)
    ∗ (l ↦ #c ==∗ node_chain (signal_node l γP n)).
  Proof.
    iIntros (HγPs HγP Hlookup) "Hchain".
    iDestruct (big_sepM_insert_acc with "Hchain")
      as "[[Hl_counter Hl_prev] Hchain]"; first apply Hlookup; simpl.
    iFrame "Hl_counter". iIntros "Hl_counter".
    iAssert (|==> l ↦{counter_to_dfrac c} #c)%I
      with "[Hl_counter]" as "Hl_counter".
    { destruct c; last by iFrame.
      by iMod (mapsto_persist with "Hl_counter"). }
    iMod "Hl_counter" as "Hl_counter".
    iSpecialize ("Hchain" $! (c, prev) with "[$Hl_counter $Hl_prev]").
    by rewrite -(to_counter_prevs_signal_node _ _ _ _ _ _ HγPs HγP Hlookup).
  Qed.

  Lemma signal_node_γRs_disjoint (l : loc) (γP : gname) (n : node) :
    γRs_disjoint n → γRs_disjoint (signal_node l γP n).
  Proof.
    cut (used_γRs (signal_node l γP n) = used_γRs n
          ∧ (γRs_disjoint n → γRs_disjoint (signal_node l γP n)));
      first by intros [_ ?].
    induction n using node_prev_ind; simpl;
      destruct bool_decide; simpl; split; set_solver.
  Qed.

  Lemma signal_spec (b : val) (P : iProp Σ) :
    {{{ send b P ∗ P }}} signal b {{{ RET #(); True }}}.
  Proof.
    iIntros (Φ) "[Hs HP] HΦ".
    iDestruct "Hs" as (γ l γP P' ->) "(H◯s & H & #HγP & _ & #Hinv)".
    iDestruct ("H" with "HP") as "HP'".
    wp_lam. wp_bind (FAA _ _).

    iInv barrierN as (n)
      "(>%Hdisjoint & >H●s & >H●r & >H●≺ & >Hchain & Hres)" "Hclose".
    iDestruct (to_counter_prevs_lookup_sender with "H●s H◯s")
      as %(c & prev & Hlookup).
    iDestruct (to_γPs_lookup with "H●s H◯s") as %(γPs & HγP%singleton_subseteq_l & HγPs).
    iDestruct (node_chain_signal_acc with "Hchain")
      as "(Hl_counter & Hchain)"; [apply HγPs | apply HγP | apply Hlookup |].
    wp_faa. replace (S c + -1)%Z with (c : Z) by lia.
    iMod ("Hchain" with "Hl_counter") as "Hchain".
    iMod (update_signal_node with "H●s H◯s") as "H●s".
    iMod ("Hclose" with "[H●s H●r H●≺ Hchain HP' Hres]") as "_".
    { iExists (signal_node l γP n).
      rewrite to_γRs_signal_node to_locations_signal_node.
      iFrame "H●s H●r H●≺ Hchain".
      iSplit. { iPureIntro. by apply signal_node_γRs_disjoint. }
      by iApply (signal_node_resources with "HγP HP' Hres"). }
    iModIntro. wp_seq. by iApply "HΦ".
  Qed.
End signal_spec.

Section extend_spec.
  Context `{!heapGS Σ, !barrierG Σ}.

  (* Extend the node corresponding to l, where new_l is the new location
     and γQ is the identifier of the resource which will be sent. *)
  Fixpoint extend_node (l new_l : loc) (γQ : gname) (n : node) :=
    if bool_decide (l = n.(location))
      then mk_node n.(location) n.(γRs) n.(γPs)
            (Some (mk_node new_l {[γQ]} {[γQ]} n.(prev)))
      else mk_node n.(location) n.(γRs) n.(γPs) (extend_node l new_l γQ <$> n.(prev)).

  Lemma extend_node_location (l new_l : loc) (γQ : gname) (n : node) :
    (extend_node l new_l γQ n).(location) = n.(location).
  Proof.
    destruct n. simpl. destruct (bool_decide (l = location0)); reflexivity.
  Qed.

  Lemma to_γPs_extend_node (l new_l : loc) (γQ : gname) (n : node) :
    l ∈ to_locations n →
    new_l ∉ to_locations n →
    to_γPs (extend_node l new_l γQ n)
      = <[new_l := GSet {[γQ]}]> (to_γPs n).
  Proof.
    intros Hin Hfresh. induction n using node_prev_ind; simpl in *.
    - assert (l = l') as -> by set_solver.
      rewrite bool_decide_eq_true_2; last done; simpl.
      rewrite insert_commute; set_solver.
    - destruct (decide (l = l')) as [-> | Hneq'].
      + rewrite bool_decide_eq_true_2; last done; simpl.
        rewrite insert_commute; set_solver.
      + rewrite bool_decide_eq_false_2; last done; simpl.
        rewrite (IHn _ _); [| set_solver | set_solver].
        rewrite insert_commute; set_solver.
  Qed.

  Lemma to_γRs_extend_node (l new_l : loc) (γQ : gname) (n : node) :
    new_l ∉ to_locations n →
    l ∈ to_locations n →
    to_γRs (extend_node l new_l γQ n)
      = <[new_l:=GSet {[γQ]}]> (to_γRs n).
  Proof.
    intros Hfresh Hin. induction n using node_prev_ind; simpl in *.
    - assert (l = l') as -> by set_solver.
      rewrite bool_decide_eq_true_2; last set_solver.
      simpl. rewrite insert_commute; auto with set_solver.
    - destruct (decide (l = l')) as [<- | Hneq'].
      + rewrite bool_decide_eq_true_2; last done. simpl.
        rewrite insert_commute; auto with set_solver.
      + rewrite bool_decide_eq_false_2; last done. simpl.
        rewrite IHn; [|set_solver|set_solver].
        rewrite insert_commute; auto with set_solver.
  Qed.

  Lemma to_counter_prevs_extend_node (l new_l : loc) (γQ : gname)
      (c : nat) (prev : option loc) (n : node) :
    new_l ∉ to_locations n →
    to_counter_prevs n !! l = Some (c, prev) →
    to_counter_prevs (extend_node l new_l γQ n)
      = <[new_l := (1, prev)]>
          (<[l := (c, Some new_l)]> (to_counter_prevs n)).
  Proof.
    intros Hfresh Hlookup.
    induction n using node_prev_ind; simpl in *.
    - rewrite !insert_empty !lookup_singleton_Some in Hlookup.
      destruct Hlookup as [<- [= <- <-]].
      rewrite bool_decide_eq_true_2; last auto; cbn.
      rewrite size_singleton. rewrite insert_commute; last set_solver.
      by rewrite insert_insert.
    - rewrite lookup_insert_Some in Hlookup.
      destruct Hlookup as [[<- [= <- <-]] | [Hneq Hlookup]].
      + rewrite bool_decide_eq_true_2; last auto; cbn.
        rewrite insert_commute; last set_solver.
        rewrite size_singleton. by rewrite insert_insert.
      + rewrite bool_decide_eq_false_2; last auto; simpl.
        rewrite extend_node_location.
        rewrite IHn; [| set_solver | apply Hlookup].
        rewrite insert_commute; last set_solver.
        f_equal. by rewrite insert_commute.
  Qed.

  Lemma to_locations_extend_node (l new_l : loc) (γQ : gname) (ls ls' : list loc) (n : node) :
    to_locations n = ls ++ l :: ls' →
    NoDup (to_locations n) →
    to_locations (extend_node l new_l γQ n)
      = ls ++ l :: new_l :: ls'.
  Proof.
    generalize dependent ls'.
    generalize dependent ls.
    induction n using node_prev_ind;
      simpl in *; intros ls ls' Hchain Hnodup.
    - pose proof (f_equal length Hchain) as Hlen.
      rewrite (length_Permutation_proper (ls ++ l :: ls') (l :: ls ++ ls')) in Hlen; last first.
      { by rewrite Permutation_middle. }
      destruct ls; last first.
      { simpl in Hlen. discriminate. }
      destruct ls'; last first.
      { simpl in Hlen. discriminate. }
      injection Hchain as <-.
      rewrite bool_decide_eq_true_2; last done.
      reflexivity.
    - destruct ls.
      + injection Hchain as <- <-.
        rewrite bool_decide_eq_true_2; last done.
        reflexivity.
      + injection Hchain as <- Hchain. simpl in Hnodup.
        apply NoDup_cons in Hnodup as [? Hnodup].
        rewrite bool_decide_eq_false_2; last first.
        { intros ->. apply H.
          apply elem_of_list_lookup.
          exists (length ls). rewrite Hchain.
          by apply list_lookup_middle. }
        simpl. rewrite (IHn ls ls'); auto.
  Qed.

  Lemma update_to_γRs_extend_node (new_l : loc) (γQ : gname) (l : loc)
      (n : node) (γ : barrier_name) :
    l ∈ to_locations n →
    new_l ∉ to_locations n →
    own γ.(barrier_name_recvs) (● to_γRs n) ==∗
      own γ.(barrier_name_recvs) (● to_γRs (extend_node l new_l γQ n))
      ∗ own γ.(barrier_name_recvs) (◯ {[new_l := GSet {[γQ]}]}).
  Proof.
    iIntros (Hin Hfresh) "H●".
    iMod (own_update _ _ (● _ ⋅ ◯ {[new_l := GSet {[γQ]}]})
      with "H●") as "[H● $]".
    { apply auth_update_alloc.
      apply alloc_local_update; last done.
      rewrite -not_elem_of_dom. by rewrite dom_to_γRs. }
    by rewrite to_γRs_extend_node.
  Qed.

  Lemma update_to_γPs_extend_node (new_l : loc) (γQ : gname) (l : loc) (γ : gname) (n : node) :
    l ∈ to_locations n →
    new_l ∉ to_locations n →
    own γ (● to_γPs n) ==∗
      own γ (● to_γPs (extend_node l new_l γQ n))
      ∗ own γ (◯ {[new_l := GSet {[γQ]}]}).
  Proof.
    iIntros (Hin Hfresh) "H●".
    iMod (own_update _ _ (● _ ⋅ ◯ {[new_l := GSet {[γQ]}]})
      with "H●") as "[$ $]"; last done.
    apply auth_update_alloc.
    rewrite (to_γPs_extend_node _ _ _ _ Hin Hfresh).
    apply alloc_local_update; last done.
    apply not_elem_of_dom. by rewrite dom_to_γPs.
  Qed.

  Lemma extend_node_resources (l new_l : loc) (γQ : gname) (Racc : iProp Σ)
      (n : node) :
    resources n Racc -∗
    resources (extend_node l new_l γQ n) Racc.
  Proof.
    iIntros "Hres".
    iInduction n as [|n'] "IH" using node_prev_ind forall (Racc); simpl.
    - destruct (bool_decide (l = l')); simpl.
      + rewrite big_sepS_singleton.
        iIntros "$ HPs". by iApply "Hres".
      + iApply "Hres".
    - destruct (bool_decide (l = l')); simpl.
      + iApply (resources_weaken with "Hres").
        rewrite big_sepS_singleton.
        iIntros "$ $".
      + iApply ("IH" with "Hres").
  Qed.

  (* Depending on whether the race is won,
     the node may have to be extended, or left as it is.
     In the first case the extend resource can be instantiated with `Some (l', γQ)`,
     and in latter one it's instantiated with None. *)
  Lemma node_chain_extend_acc (l : loc) (prev : option loc)
      (c : nat) (n : node) :
    to_counter_prevs n !! l = Some (S c, prev) →
    node_chain n -∗
    (l +ₗ 1) ↦ option_loc_to_val prev
    ∗ ∀ (loc_γQ : option (loc * gname)),
        match loc_γQ with
        | None =>
            (l +ₗ 1) ↦ option_loc_to_val prev -∗
            node_chain n
        | Some (l', γQ) =>
            ⌜l' ∉ to_locations n⌝ -∗
            (l +ₗ 1) ↦ option_loc_to_val (Some l') -∗
            l' ↦ #1 -∗
            (l' +ₗ 1) ↦ option_loc_to_val prev -∗
            node_chain (extend_node l l' γQ n)
        end.
  Proof.
    iIntros (Hlookup) "Hchain".
    iDestruct (big_sepM_insert_acc with "Hchain")
      as "[[Hl_counter Hl_prev] Hchain]"; first apply Hlookup; simpl.
    iFrame "Hl_prev".
    iIntros ([[l' γQ]|]).
    - iIntros (Hfresh) "Hl_prev Hl'_counter Hl'_prev".
      iSpecialize ("Hchain" $! (S c, Some l')
        with "[$Hl_counter $Hl_prev]").
      rewrite /node_chain.
      rewrite (to_counter_prevs_extend_node _ _ _ _ _ _ Hfresh Hlookup).
      iApply big_sepM_insert; last by iFrame.
      apply not_elem_of_dom_1. rewrite dom_insert_L.
      rewrite not_elem_of_union. rewrite dom_to_counter_prevs.
      split; last auto.
      assert (l ∈ to_locations n); last set_solver.
      rewrite -dom_to_counter_prevs. by eapply elem_of_dom_2.
    - iIntros "Hl_prev".
      iSpecialize ("Hchain" $! (S c, prev)
        with "[$Hl_counter $Hl_prev]").
      by rewrite insert_id.
  Qed.

  Lemma extend_node_γRs_disjoint (l l' : loc) (γQ : gname) (n : node) :
    γQ ∉ used_γRs n →
    γRs_disjoint n →
    γRs_disjoint (extend_node l l' γQ n).
  Proof.
    intros Hfresh.
    cut (used_γRs (extend_node l l' γQ n) ⊆ {[γQ]} ∪ used_γRs n
          ∧ (γRs_disjoint n → γRs_disjoint (extend_node l l' γQ n)));
      first by intros [_ ?].
    induction n using node_prev_ind; simpl in *;
      destruct bool_decide; simpl; split; set_solver.
  Qed.

  Lemma new_loc_is_fresh (l : loc) (v : val) (n : node) :
    l ↦ v -∗
    node_chain n -∗
    ⌜l ∉ to_locations n⌝.
  Proof.
    iIntros "Hl Hchain".
    iIntros (Hlookup%dom_to_counter_prevs%elem_of_dom%lookup_lookup_total).
    destruct (to_counter_prevs n !!! l).
    iDestruct (node_chain_lookup_acc with "Hchain") as "[Hl' _]"; first apply Hlookup.
    by iDestruct (mapsto_ne with "Hl Hl'") as %[].
  Qed.

  Lemma extend_spec (Q : iProp Σ) (E : gset val) (P : iProp Σ) (b : val) :
    {{{ send b P ∗ ([∗ set] e ∈ E, e ≺ b) }}}
      extend b
    {{{ (b' : val), RET b';
      send b' Q ∗ recv b' Q ∗ send b P
      ∗ b' ≺ b ∗ ([∗ set] e ∈ E, e ≺ b') }}}.
  Proof.
    iIntros (Φ) "[Hs #HE] HΦ".
    iDestruct "Hs" as (γ l γP P' ->) "(H◯l & HP' & #HγP & #Hl_meta & #Hinv)".
    iLöb as "IH".
    wp_lam. wp_op. wp_bind (! _)%E.
    (* Read the previous node *)
    iInv barrierN as (n)
      "(>%Hdisjoint & >H●s & >H●r & >H●≺ & >Hchain & Hres)" "Hclose".
    iDestruct (to_counter_prevs_lookup_sender with "H●s H◯l")
      as %(c & prev & Hlookup).
    iDestruct (node_chain_lookup_acc with "Hchain")
      as "(Hl_counter & Hl_prev & Hchain)"; first apply Hlookup; simpl.
    wp_load. iSpecialize ("Hchain" with "Hl_counter Hl_prev").
    iMod ("Hclose" with "[H●s H●r H●≺ Hchain Hres]") as "_".
    { iExists n. by iFrame. }
    clear n Hlookup Hdisjoint. iModIntro.

    wp_let. wp_apply (wp_alloc_2 with "[//]").
    iIntros (l') "(Hl'_counter & Hl'_prev & Hl'_meta)".
    wp_let. wp_store. wp_store.
    iMod (meta_set _ l' γ barrierN with "Hl'_meta") as "#Hl'_meta"; first done.
    wp_inj. wp_op. wp_bind (CmpXchg _ _ _).

    (* Open invariant to check if we won the extend race *)
    iInv barrierN as (n)
      "(>%Hdisjoint & >H●s & >H●r & >H●≺ & >Hchain & Hres)" "Hclose".
    iDestruct (to_counter_prevs_lookup_sender with "H●s H◯l")
      as %(c' & prev' & Hlookup).
    iDestruct (new_loc_is_fresh with "Hl'_counter Hchain") as %Hfresh.
    iDestruct (to_γPs_lookup with "H●s H◯l") as %(γPs & ? & HγPs).
    iDestruct (order_own_auth_no_dup with "H●≺") as %Hnodup.
    iDestruct (node_chain_extend_acc with "Hchain")
      as "[Hl_prev Hchain]"; [apply Hlookup |].
    wp_cmpxchg as ->%option_loc_to_val_inj | _;
      first apply optional_locs_compare_safe; last first.
    { (* We've lost the race.
         We free the locations and try again. *)
      iSpecialize ("Hchain" $! None with "Hl_prev").
      iMod ("Hclose" with "[H●s H●r H●≺ Hchain Hres]") as "_".
      { iExists n. by iFrame. }
      iModIntro. wp_proj. wp_if_false.
      wp_free. wp_seq. wp_free. wp_seq.
      iApply ("IH" with "H◯l HP' HΦ"). }

    (* Assert some stuff up front, which we'll need later:
       - l ∈ to_location n
       - All elements in E are part of the chain *)
    iAssert (⌜l ∈ to_locations n⌝)%I as %Hin.
    { iDestruct (own_valid_2 with "H●s H◯l")
        as %[Hdom%dom_included _]%auth_both_valid_discrete.
      by rewrite dom_singleton_L singleton_subseteq_l dom_to_γPs in Hdom. }
    iAssert (⌜∀ (l'' : loc), #l'' ∈ E → l'' ∈ to_locations n⌝)%I as %HE.
    { iIntros (l'' Hin'').
      iDestruct (big_sepS_elem_of _ E (#l'') with "HE") as "H≺"; first done.
      iDestruct "H≺" as (γ' ? ? [= <-] [= <-]) "(_ & Hl_meta' & Horder)".
      iDestruct (meta_agree with "Hl_meta Hl_meta'") as %<-.
      iDestruct (order_own_ordered_elem_of with "H●≺ Horder") as "[_ $]". }

    (* We've won the race meaning we can extend the node *)
    iMod (saved_prop_alloc_cofinite (used_γRs n) Q DfracDiscarded)
      as (γQ HγQ_fresh) "#HγQ"; first done.
    iMod (update_to_γPs_extend_node l' γQ with "H●s")
      as "(H●s & H◯l')"; [apply Hin | apply Hfresh |].
    iMod (update_to_γRs_extend_node l' γQ l with "H●r")
      as "[H●r H◯r]"; [apply Hin | apply Hfresh |].
    iMod (order_own_auth_update_insert_after l' l with "H●≺")
      as (ls ls' Hchain) "[H●≺ #Horder']"; [apply Hfresh | apply Hin |].
    iSpecialize ("Hchain" $! (Some (l', γQ)) Hfresh with "Hl_prev Hl'_counter Hl'_prev").

    (* This is one of the postconditions we need to prove.
       We need to prove it before we give up H●≺ to the invariant.
       We can prove this without giving up H●≺ in the rest of the proof,
       as the goal is persistent. *)
    iAssert ([∗ set] e ∈ E, e ≺ #l')%I as "#HE'".
    { iApply big_sepS_forall. iIntros (e Hin').
      iDestruct (big_sepS_elem_of _ E e with "HE") as "H≺"; first done.
      iDestruct "H≺" as (γ' l'' ? -> [= <-]) "(Hl''_meta & Hl_meta' & Horder'')".
      iDestruct (meta_agree with "Hl_meta Hl_meta'") as %<-.
      iExists γ, l'', l'; repeat iSplit; auto.
      iDestruct (order_own_auth_adjacent with "H●≺ Horder''") as "[-> | $]".
      iExFalso. iPureIntro. apply Hfresh.
      apply HE. apply Hin'. } iClear "HE".

    iMod ("Hclose" with "[H●s H●r H●≺ Hchain Hres]") as "_".
    { iExists (extend_node l l' γQ n).
      rewrite (to_locations_extend_node _ _ _ _ _ _ Hchain Hnodup).
      iFrame "H●s H●r H●≺". iSplit.
      { iPureIntro. by apply extend_node_γRs_disjoint. }
      iFrame "Hchain".
      iApply (extend_node_resources with "Hres"). }

    iModIntro. wp_proj. wp_if_true.
    iApply "HΦ". iFrame "HE'".
    iSplitL "H◯l'".
    { iExists γ, l', γQ, Q. auto 10 with iFrame. }
    iSplitL "H◯r".
    { iExists γ, l', γQ, Q. auto 10 with iFrame. }
    iSplitL "H◯l HP'".
    { iExists γ, l, γP, P'. auto 10 with iFrame. }
    { iExists γ, l', l. auto 10 with iFrame. }
  Qed.
End extend_spec.

Section recv_split.
  Context `{!heapGS Σ, !barrierG Σ}.

  Fixpoint recv_split_node (l : loc) (γR γR1 γR2 : gname) (n : node) : node :=
    if bool_decide (l = n.(location))
      then mk_node n.(location)
            ({[γR1; γR2]} ∪ (n.(γRs) ∖ {[γR]})) n.(γPs) n.(prev)
      else mk_node n.(location) n.(γRs) n.(γPs) (recv_split_node l γR γR1 γR2 <$> n.(prev)).

  Lemma recv_split_node_location (l : loc) (γR γR1 γR2 : gname) (n : node) :
    (recv_split_node l γR γR1 γR2 n).(location) = n.(location).
  Proof.
    destruct n. simpl. destruct (bool_decide (l = location0)); reflexivity.
  Qed.

  Lemma to_γPs_recv_split_node (l : loc) (γR γR1 γR2 : gname) (n : node) :
    to_γPs (recv_split_node l γR γR1 γR2 n) = to_γPs n.
  Proof.
    induction n using node_prev_ind; simpl.
    - destruct (bool_decide (l = l')); reflexivity.
    - destruct (bool_decide (l = l')); simpl;
        [|rewrite IHn]; reflexivity.
  Qed.

  Lemma to_locations_recv_split_node (l : loc) (γR γR1 γR2 : gname) (n : node) :
    to_locations (recv_split_node l γR γR1 γR2 n) = to_locations n.
  Proof.
    induction n using node_prev_ind; simpl.
    - destruct (bool_decide (l = l')); reflexivity.
    - destruct (bool_decide (l = l')); simpl;
        [|rewrite IHn]; reflexivity.
  Qed.

  Lemma to_counter_prevs_recv_split_node (l : loc) (γR γR1 γR2 : gname) (n : node) :
    to_counter_prevs (recv_split_node l γR γR1 γR2 n) = to_counter_prevs n.
  Proof.
    induction n using node_prev_ind; simpl.
    - destruct (bool_decide (l = l')); reflexivity.
    - destruct (bool_decide (l = l')); simpl;
        [|rewrite IHn recv_split_node_location]; reflexivity.
  Qed.

  Lemma node_chain_recv_split_node (l : loc) (γR γR1 γR2 : gname) (n : node) :
    node_chain (recv_split_node l γR γR1 γR2 n) ⊣⊢ node_chain n.
  Proof.
    by rewrite /node_chain to_counter_prevs_recv_split_node.
  Qed.

  Lemma to_γRs_recv_split_node (l : loc) (γR γR1 γR2 : gname) (γRs : gset gname) (n : node) :
    to_γRs n !! l = Some (GSet γRs) →
    to_γRs (recv_split_node l γR γR1 γR2 n)
      = <[l := GSet ({[γR1; γR2]} ∪ (γRs ∖ {[γR]}))]> (to_γRs n).
  Proof.
    intros Hlookup. induction n using node_prev_ind; simpl in *.
    - rewrite insert_empty in Hlookup.
      rewrite lookup_singleton_Some in Hlookup.
      destruct Hlookup as [<- [= <-]].
      rewrite bool_decide_eq_true_2; last done.
      simpl. by rewrite insert_insert insert_empty.
    - destruct (decide (l = l')) as [<- | Hneq].
      + rewrite bool_decide_eq_true_2; last done. simpl.
        rewrite lookup_insert in Hlookup.
        injection Hlookup as ->.
        by rewrite insert_insert.
      + rewrite bool_decide_eq_false_2; last done. simpl.
        rewrite lookup_insert_ne in Hlookup; last done.
        rewrite insert_commute; last done.
        rewrite IHn; auto with set_solver.
  Qed.

  Lemma recv_split_node_resources (l : loc) (γR γR1 γR2 : gname)
      (γRs : gset gname) (R R1 R2 Qacc : iProp Σ) (n : node) :
    to_γRs n !! l = Some (GSet γRs) →
    γR ∈ γRs →
    γR1 ∉ γRs →
    γR2 ∉ γRs →
    γR1 ≠ γR2 →
    (R -∗ R1 ∗ R2) -∗
    saved_prop_own γR DfracDiscarded R -∗
    saved_prop_own γR1 DfracDiscarded R1 -∗
    saved_prop_own γR2 DfracDiscarded R2 -∗
    resources n Qacc -∗
    resources (recv_split_node l γR γR1 γR2 n) Qacc.
  Proof.
    iIntros (Hlookup HγR HγR1 HγR2 Hneq) "H #HγR #HγR1 #HγR2 Hres".
    iInduction n as [|] "IH" using node_prev_ind forall (Qacc); simpl in *.
    - rewrite insert_empty lookup_singleton_Some in Hlookup.
      destruct Hlookup as [<- [= <-]] .
      rewrite bool_decide_eq_true_2; last auto; simpl.
      iIntros "HPs".
      rewrite big_sepS_union; last set_solver.
      rewrite big_sepS_union; last set_solver.
      rewrite !big_sepS_singleton.
      iDestruct ("Hres" with "HPs") as "[HRs $]".
      iDestruct (big_sepS_delete with "HRs") as "[HR $]"; first apply HγR.
      iDestruct "HR" as (R') "[#HγR' HR']".
      iDestruct (saved_prop_agree with "HγR HγR'") as "#Heq".
      iAssert (▷ (R1 ∗ R2))%I with "[H HR']" as "[HR1 HR2]".
      { iNext. iApply "H". by iRewrite "Heq". }
      iSplitL "HR1"; eauto with iFrame.
    - rewrite lookup_insert_Some in Hlookup.
      destruct Hlookup as [[<- [= <-]] | [Hneq' Hlookup]].
      + rewrite bool_decide_eq_true_2; last auto; simpl.
        iApply (resources_weaken with "Hres").
        iIntros "Hres HPs".
        rewrite big_sepS_union; last set_solver.
        rewrite big_sepS_union; last set_solver.
        rewrite !big_sepS_singleton.
        iDestruct ("Hres" with "HPs") as "[HRs $]".
        iDestruct (big_sepS_delete with "HRs") as "[HR $]"; first apply HγR.
        iDestruct "HR" as (R') "[#HγR' HR']".
        iDestruct (saved_prop_agree with "HγR HγR'") as "#Heq".
        iAssert (▷ (R1 ∗ R2))%I with "[H HR']" as "[HR1 HR2]".
        { iNext. iApply "H". by iRewrite "Heq". }
        iSplitL "HR1"; eauto with iFrame.
      + rewrite bool_decide_eq_false_2; last auto; simpl.
        iApply ("IH" $! Hlookup with "H Hres").
  Qed.

  Lemma recv_split_node_γRs_disjoint (l : loc) (γR γR1 γR2 : gname) (n : node) :
    γR1 ≠ γR2 →
    γR1 ∉ used_γRs n →
    γR2 ∉ used_γRs n →
    γRs_disjoint n →
    γRs_disjoint (recv_split_node l γR γR1 γR2 n).
  Proof.
    intros Hneq Hfresh1 Hfresh2.
    cut (used_γRs (recv_split_node l γR γR1 γR2 n) ⊆ {[γR1; γR2]} ∪ used_γRs n
          ∧ (γRs_disjoint n → γRs_disjoint (recv_split_node l γR γR1 γR2 n)));
      first by intros [_ ?].
    induction n using node_prev_ind; simpl in *;
      destruct bool_decide; simpl; split; set_solver.
  Qed.

  Lemma recv_split (b : val) (R1 R2 : iProp Σ) (E : coPset) :
      ↑barrierN ⊆ E → recv b (R1 ∗ R2) ={E}=∗ recv b R1 ∗ recv b R2.
  Proof.
    iIntros (?).
    iDestruct 1 as (γ l γR R ->) "(H◯r & HR & #HγR & #Hmeta & #Hinv)".
    iInv barrierN as (n)
      "(>%Hdisjoint & >H●s & >H●r & >H●≺ & >Hchain & Hres)" "Hclose".
    iDestruct (to_γRs_lookup with "H●r H◯r") as %(γRs & Hin & Hlookup).

    (* Allocate the new saved props and replace γR with them *)
    iMod (saved_prop_alloc_cofinite (used_γRs n) R1 DfracDiscarded)
      as (γR1 Hfresh1) "#HγR1"; first done.
    iMod (saved_prop_alloc_cofinite ({[γR1]} ∪ used_γRs n) R2 DfracDiscarded)
      as (γR2 Hfresh2) "#HγR2"; first done.
    iMod (own_update_2 with "H●r H◯r") as "[H●r H◯r]".
    { apply auth_update. eapply singleton_local_update.
      - apply Hlookup.
      - apply gset_disj_dealloc_local_update. }
    iMod (own_update_2 _ _ _
            (● _ ⋅ (◯ {[l := GSet {[γR1]} ⋅ GSet {[γR2]}]}))
      with "H●r H◯r") as "(H●r & H◯r1 & H◯r2)".
    { apply auth_update. eapply singleton_local_update.
      - apply lookup_insert.
      - rewrite gset_disj_union; last set_solver.
        apply gset_disj_alloc_empty_local_update.
        apply γRs_are_used in Hlookup. set_solver. }
    iEval (rewrite insert_insert) in "H●r".

    (* Close the invariant again *)
    iMod ("Hclose" with "[H●s H●r H●≺ Hchain HR Hres]") as "_".
    { iExists (recv_split_node l γR γR1 γR2 n).
      rewrite to_γPs_recv_split_node to_locations_recv_split_node
              node_chain_recv_split_node.
      rewrite (to_γRs_recv_split_node _ _ _ _ _ _ Hlookup).
      iFrame. iSplit.
      { iPureIntro. apply recv_split_node_γRs_disjoint; set_solver. }
      iApply (recv_split_node_resources with "HR HγR HγR1 HγR2 Hres");
        first apply Hlookup; apply γRs_are_used in Hlookup; set_solver. }

    iSplitL "H◯r1".
    - iExists γ, l, γR1. auto 10 with iFrame.
    - iExists γ, l, γR2. auto 10 with iFrame.
  Qed.
End recv_split.

Section wait_spec.
  Context `{!heapGS Σ, !barrierG Σ}.

  Fixpoint recv_node (l : loc) (γR : gname) (n : node) : node :=
    if bool_decide (l = n.(location))
      then mk_node n.(location) (n.(γRs) ∖ {[γR]}) n.(γPs) n.(prev)
      else mk_node n.(location)  n.(γRs)           n.(γPs) (recv_node l γR <$> n.(prev)).

  (* When the node corresponding with l has a previous node,
     we pass γR on to that node. *)
  Fixpoint pass_on_γR (l : loc) (γR : gname) (n : node) : node :=
    match n.(prev) with
    | None => n
    | Some n' =>
        if (bool_decide (l = n.(location)))
          then mk_node n.(location) (n.(γRs) ∖ {[γR]}) n.(γPs)
                (Some (mk_node n'.(location) ({[γR]} ∪ n'.(γRs)) n'.(γPs) n'.(prev)))
          else mk_node n.(location)  n.(γRs) n.(γPs) (pass_on_γR l γR <$> n.(prev))
    end.

  Lemma recv_node_location (l : loc) (γR : gname) (n : node) :
    (recv_node l γR n).(location) = n.(location).
  Proof.
    destruct n. simpl. destruct (bool_decide (l = location0)); reflexivity.
  Qed.

  Lemma to_γPs_recv_node (l : loc) (γR : gname) (n : node) :
    to_γPs (recv_node l γR n) = to_γPs n.
  Proof.
    induction n using node_prev_ind; simpl.
    - destruct (bool_decide (l = l')); reflexivity.
    - destruct (bool_decide (l = l')); simpl;
        [|rewrite IHn]; reflexivity.
  Qed.

  Lemma to_locations_recv_node (l : loc) (γR : gname) (n : node) :
    to_locations (recv_node l γR n) = to_locations n.
  Proof.
    induction n using node_prev_ind; simpl.
    - destruct (bool_decide (l = l')); reflexivity.
    - destruct (bool_decide (l = l')); simpl;
        [|rewrite IHn]; reflexivity.
  Qed.

  Lemma to_counter_prevs_recv_node (l : loc) (γR : gname) (n : node) :
    to_counter_prevs (recv_node l γR n) = to_counter_prevs n.
  Proof.
    induction n using node_prev_ind; simpl.
    - destruct (bool_decide (l = l')); reflexivity.
    - destruct (bool_decide (l = l')); simpl;
        [|rewrite IHn recv_node_location]; reflexivity.
  Qed.

  Lemma node_chain_recv_node (l : loc) (γR : gname) (n : node) :
    node_chain (recv_node l γR n) ⊣⊢ node_chain n.
  Proof.
    by rewrite /node_chain to_counter_prevs_recv_node.
  Qed.

  Lemma to_γRs_recv_node (l : loc) (γR : gname) (γRs : gset gname) (n : node) :
    to_γRs n !! l = Some (GSet γRs) →
    to_γRs (recv_node l γR n)
      = <[l := GSet (γRs ∖ {[γR]})]> (to_γRs n).
  Proof.
    intros Hlookup. induction n using node_prev_ind; simpl in *.
    - rewrite insert_empty lookup_singleton_Some in Hlookup.
      destruct Hlookup as [<- [= <-]].
      rewrite bool_decide_eq_true_2; last done; simpl.
      by rewrite insert_insert.
    - destruct (decide (l = l')) as [<- | Hneq].
      + rewrite bool_decide_eq_true_2; last done; simpl.
        rewrite lookup_insert in Hlookup.
        injection Hlookup as ->.
        by rewrite insert_insert.
      + rewrite bool_decide_eq_false_2; last done; simpl.
        rewrite lookup_insert_ne in Hlookup; last done.
        rewrite insert_commute; last done.
        by rewrite (IHn Hlookup).
  Qed.

  Lemma recv_node_resources_last (lr : loc) (γR : gname)
      (γRs : gset gname) (R Qacc : iProp Σ) (n : node) :
    to_counter_prevs n !! lr = Some (0, None) →
    to_γRs n !! lr = Some (GSet γRs) →
    γR ∈ γRs →
    saved_prop_own γR DfracDiscarded R -∗
    resources n Qacc -∗
    ▷ R ∗ resources (recv_node lr γR n) Qacc.
  Proof.
    iIntros (Hcounter_prev HγRs HγR) "#HγR Hres".
    iInduction n as [|] "IH" using node_prev_ind forall (Qacc); simpl in *.
    - rewrite !insert_empty !lookup_singleton_Some in HγRs Hcounter_prev.
      destruct HγRs as [-> [= <-]].
      destruct Hcounter_prev as [_ [= ->%size_empty_inv%leibniz_equiv]].
      rewrite bool_decide_eq_true_2; auto; simpl.
      iDestruct ("Hres" with "[//]") as "[HRs HQ]".
      iDestruct (big_sepS_delete with "HRs") as "[HR HRs]"; first apply HγR.
      iDestruct "HR" as (R') "[#HγR' HR]". iSplitL "HR".
      { iDestruct (saved_prop_agree with "HγR HγR'") as "#Heq".
        iNext. by iRewrite "Heq". }
      iIntros "_". iFrame.
    - rewrite !lookup_insert_Some in Hcounter_prev HγRs.
      destruct Hcounter_prev as [[_ [=]] | [Hneq Hcounter_prev]].
      destruct HγRs as [[[]%Hneq _] | [_ HγRs]].
      rewrite bool_decide_eq_false_2; last auto; simpl.
      iApply ("IH" $! Hcounter_prev HγRs with "Hres").
  Qed.

  Lemma update_recv_node (n : node) (l : loc) (γR γ : gname) (γRs : gset gname) :
    to_γRs n !! l = Some (GSet γRs) →
    own γ (● to_γRs n) -∗
    own γ (◯ {[l := GSet {[γR]}]}) ==∗
    own γ (● to_γRs (recv_node l γR n)).
  Proof.
    iIntros (Hlookup) "H● H◯".
    iMod (own_update_2 _ _ _
      (● to_γRs (recv_node l γR n) ⋅ ◯ _)
      with "H● H◯") as "[H● _]"; last iApply "H●".
    rewrite (to_γRs_recv_node _  _ _ _ Hlookup).
    apply auth_update. eapply insert_local_update.
    - apply Hlookup.
    - by rewrite lookup_singleton.
    - apply gset_disj_dealloc_local_update.
  Qed.

  Lemma pass_on_γR_location (l : loc) (γR : gname) (n : node) :
    (pass_on_γR l γR n).(location) = n.(location).
  Proof.
    destruct n. destruct prev0;
      simpl; try destruct bool_decide; reflexivity.
  Qed.

  Lemma to_locations_pass_on_γR (l : loc) (γR : gname) (n : node) :
    to_locations (pass_on_γR l γR n) = to_locations n.
  Proof.
    induction n using node_prev_ind; simpl.
    - destruct (bool_decide (l = l')); reflexivity.
    - destruct (bool_decide (l = l')); simpl;
        [destruct n|rewrite IHn]; reflexivity.
  Qed.

  Lemma to_counter_prevs_pass_on_γR (l : loc) (γR : gname) (n : node) :
    to_counter_prevs (pass_on_γR l γR n) = to_counter_prevs n.
  Proof.
    induction n using node_prev_ind; simpl.
    - destruct (bool_decide (l = l')); reflexivity.
    - destruct (bool_decide (l = l')); simpl;
        [destruct n|rewrite IHn pass_on_γR_location]; reflexivity.
  Qed.

  Lemma node_chain_pass_on_γR (l : loc) (γR : gname) (n : node) :
    node_chain (pass_on_γR l γR n) ⊣⊢ node_chain n.
  Proof. by rewrite /node_chain to_counter_prevs_pass_on_γR. Qed.

  Lemma to_γPs_pass_on_γR (l : loc) (γR : gname) (n : node) :
    to_γPs (pass_on_γR l γR n) = to_γPs n.
  Proof.
    induction n using node_prev_ind; simpl.
    - destruct (bool_decide (l = l')); reflexivity.
    - destruct (bool_decide (l = l')); simpl;
        [destruct n|rewrite IHn]; reflexivity.
  Qed.

  Lemma to_γRs_pass_on_γR (l prev : loc) (γR : gname)
      (γRs γRs' : gset gname) (n : node) :
    NoDup (to_locations n) →
    (∃ (i : nat), to_locations n !! i = Some l ∧ to_locations n !! (S i) = Some prev) →
    to_γRs n !! l = Some (GSet γRs) →
    to_γRs n !! prev = Some (GSet γRs') →
    to_γRs (pass_on_γR l γR n) =
      <[prev:=GSet ({[γR]} ∪ γRs')]>
        (<[l:=GSet (γRs ∖ {[γR]})]> (to_γRs n)).
  Proof.
    intros Hnodup (i & Hl & Hprev) HγRs HγRs'.
    assert (l ≠ prev) as Hneq.
    { by apply (NoDup_lookup_neq (to_locations n) i (S i)). }
    revert i Hl Hprev.
    induction n as [|n'] using node_prev_ind; intros; simpl in *.
    { by rewrite lookup_nil in Hprev. }
    destruct i.
    - clear IHn.
      injection Hl as ->.
      rewrite bool_decide_eq_true_2; auto; simpl.
      rewrite lookup_insert in HγRs. injection HγRs as ->.
      destruct n' as [l'' γRs'' γPs'' n'']; simpl in *.
      injection Hprev as ->.
      rewrite lookup_insert_ne in HγRs'; last apply Hneq.
      rewrite lookup_insert in HγRs'. injection HγRs' as ->.
      rewrite insert_insert.
      rewrite (insert_commute _ prev l); last auto.
      by rewrite insert_insert.
    - assert (l' ≠ l) as Hneq'.
      { by apply (NoDup_lookup_neq (l' :: to_locations n') 0 (S i)). }
      assert (l' ≠ prev) as Hneq''.
      { by apply (NoDup_lookup_neq (l' :: to_locations n') 0 (S (S i))). }
      rewrite bool_decide_eq_false_2; last auto; simpl.
      rewrite !lookup_insert_ne in HγRs HγRs'; auto.
      apply NoDup_cons_1_2 in Hnodup.
      rewrite (IHn Hnodup HγRs HγRs' i Hl Hprev).
      rewrite (insert_commute _ l' prev); last auto.
      by rewrite (insert_commute _ l' l).
  Qed.

  Lemma alloc_prev_singleton (prev l : loc) (n : node) (γ : gname) :
    NoDup (to_locations n) →
    (∃ i : nat, to_locations n !! i = Some l ∧ to_locations n !! S i = Some prev) →
    own γ (● to_γRs n) ==∗
    own γ (● to_γRs n) ∗ own γ (◯ {[prev := GSet ∅]}).
  Proof.
    iIntros (Hnodup Hlocs) "H●r".
    iMod (own_update _ _ (● _ ⋅ ◯ _) with "H●r") as "[H●r H◯prev]"; last by iFrame.
    apply auth_update_dfrac_alloc.
    - apply gmap_core_id. intros prev' X Hlookup.
      rewrite lookup_singleton_Some in Hlookup.
      destruct Hlookup as [_ <-].
      by rewrite core_id_total_L.
    - apply singleton_included_l.
      destruct (lookup_prev_γRs l prev n Hnodup Hlocs) as [γRs Hlookup].
      exists (GSet γRs). split; first by fold_leibniz.
      apply Some_included_2. set_solver.
  Qed.

  Lemma update_pass_on_γR (l prev : loc) (γR : gname) (γRs γRs' : gset gname)
      (n : node) (γ : barrier_name) :
    NoDup (to_locations n) →
    (∃ (i : nat), to_locations n !! i = Some l ∧ to_locations n !! (S i) = Some prev) →
    to_γRs n !! l = Some (GSet γRs) →
    to_γRs n !! prev = Some (GSet γRs') →
    γR ∉ γRs' →
    own γ.(barrier_name_recvs) (● to_γRs n) -∗
    own γ.(barrier_name_recvs) (◯ {[l := GSet {[γR]}]}) -∗
    own γ.(barrier_name_recvs) (◯ {[prev := GSet ∅]}) ==∗
      own γ.(barrier_name_recvs) (● to_γRs (pass_on_γR l γR n))
      ∗ own γ.(barrier_name_recvs) (◯ {[prev := GSet {[γR]}]}).
  Proof.
    iIntros (Hnodup Hlocs HγRs HγRs' Hfresh) "H● H◯l H◯prev".

    iMod (own_update_2 _ _ _ (● _ ⋅ ◯ {[l := GSet ∅]}) with "H● H◯l")
      as "[H● _]".
    { apply auth_update. eapply singleton_local_update.
      - apply HγRs.
      - apply gset_disj_dealloc_local_update. }
    iMod (own_update_2 _ _ _ (● _ ⋅ ◯ ({[prev := GSet {[γR]}]}))
      with "H● H◯prev") as "[H● H◯prev]".
    { apply auth_update. eapply singleton_local_update.
      - rewrite lookup_insert_ne; first apply HγRs'.
        intros ->. destruct Hlocs as (i & Hl & Hprev).
        by apply (NoDup_lookup_neq _ _ _ _ _ Hnodup Hl Hprev).
      - apply gset_disj_alloc_empty_local_update. set_solver. }

    rewrite (to_γRs_pass_on_γR _ _ _ _ _ _ Hnodup Hlocs HγRs HγRs').
    by iFrame.
  Qed.

  Lemma pass_on_γR_resources (l prev : loc) (γR : gname)
      (γRs γRs' : gset gname) (n : node) (Qacc : iProp Σ) :
    NoDup (to_locations n) →
    (∃ (i : nat), to_locations n !! i = Some l ∧ to_locations n !! (S i) = Some prev) →
    to_counter_prevs n !! l = Some (0, Some prev) →
    to_γRs n !! l = Some (GSet γRs) →
    to_γRs n !! prev = Some (GSet γRs') →
    γR ∈ γRs →
    γR ∉ γRs' →
    resources n Qacc -∗
    resources (pass_on_γR l γR n) Qacc.
  Proof.
    iIntros (Hnodup (i & Hl & Hprev) Hcounter_prev HγRs HγRs' HγR Hfresh)
      "Hres".
    assert (l ≠ prev) as Hneq.
    { by apply (NoDup_lookup_neq (to_locations n) i (S i)). }
    iInduction n as [|n'] "IH" using node_prev_ind forall (i Hl Hprev Qacc);
      simpl in *.
    { by rewrite lookup_nil in Hprev. }
    destruct i.
    - iClear "IH".
      injection Hl as ->.
      rewrite bool_decide_eq_true_2; auto; simpl.
      rewrite !lookup_insert in Hcounter_prev HγRs.
      injection Hcounter_prev as ->%size_empty_inv%leibniz_equiv <-.
      injection HγRs as ->.
      destruct n' as [l'' γRs'' γPs'' n'']; simpl in *.
      rewrite lookup_insert_ne in HγRs'; last apply Hneq.
      rewrite lookup_insert in HγRs'. injection HγRs' as ->.
      destruct n'' as [n''|].
      + iApply (resources_weaken with "Hres").
        rewrite big_sepS_empty.
        iIntros "Hres HPs''".
        iDestruct ("Hres" with "HPs''") as "[HRs' HRs]".
        iDestruct ("HRs" with "[//]") as "[HRs HQ]".
        iDestruct (big_sepS_delete with "HRs") as "[HR HRs]"; first apply HγR.
        rewrite big_sepS_insert; last apply Hfresh.
        iFrame "HR HRs'". iIntros "_". iFrame.
      + rewrite big_sepS_empty.
        iIntros "HPs''".
        iDestruct ("Hres" with "HPs''") as "[HRs' HRs]".
        iDestruct ("HRs" with "[//]") as "[HRs HQ]".
        iDestruct (big_sepS_delete with "HRs") as "[HR HRs]"; first apply HγR.
        rewrite big_sepS_insert; last apply Hfresh.
        iFrame "HR HRs'". iIntros "_". iFrame.
    - assert (l' ≠ l) as Hneq'.
      { by apply (NoDup_lookup_neq (l' :: to_locations n') 0 (S i)). }
      assert (l' ≠ prev) as Hneq''.
      { by apply (NoDup_lookup_neq (l' :: to_locations n') 0 (S (S i))). }
      rewrite bool_decide_eq_false_2; last auto; simpl.
      rewrite !lookup_insert_ne in Hcounter_prev HγRs HγRs'; auto.
      apply NoDup_cons_1_2 in Hnodup.
      iApply ("IH" $! Hnodup Hcounter_prev HγRs HγRs' i Hl Hprev with "Hres").
  Qed.

  Lemma to_counter_prevs_lookup_receiver (n : node) (l : loc) (γRs : gset gname) (γ : gname) :
    own γ (● to_γRs n) -∗
    own γ (◯ {[l := GSet γRs]}) -∗
      ∃ c prev, ⌜to_counter_prevs n !! l = Some (c, prev)⌝.
  Proof.
    iIntros "H● H◯".
    iDestruct (own_valid_2 with "H● H◯") as %[Hdom%dom_included _]%auth_both_valid_discrete.
    rewrite dom_singleton_L singleton_subseteq_l in Hdom.
    iPureIntro. induction n using node_prev_ind; simpl in *.
    - rewrite !insert_empty in Hdom |- *.
      rewrite dom_singleton_L in Hdom.
      assert (l = l') as -> by set_solver.
      exists (size γPs'), None. apply lookup_singleton.
    - rewrite dom_insert_L in Hdom.
      destruct (decide (l = l')) as [-> | Hneq].
      + exists (size γPs'), (Some n.(location)). apply lookup_insert.
      + rewrite lookup_insert_ne; last done.
        apply IHn. set_solver.
  Qed.

  Lemma pass_on_γR_γRs_disjoint (l : loc) (γR : gname) (γRs : gset gname) (n : node) :
    to_γRs n !! l = Some (GSet γRs) →
    γR ∈ γRs →
    γRs_disjoint n →
    γRs_disjoint (pass_on_γR l γR n).
  Proof.
    intros HγRs Hin.
    cut (used_γRs (pass_on_γR l γR n) = used_γRs n
          ∧ (γRs_disjoint n → γRs_disjoint (pass_on_γR l γR n)));
      first by intros [_ ?].
    induction n using node_prev_ind; simpl in *; first auto.
    rewrite lookup_insert_Some in HγRs.
    destruct HγRs as [[-> [= ->]] | [Hneq HγRs]].
    - rewrite bool_decide_eq_true_2; last auto; simpl.
      clear IHn. split.
      + destruct n. simpl.
        rewrite !union_assoc_L difference_union_L. set_solver.
      + destruct n; simpl.
        intros [? ?]. split; first set_solver.
        destruct prev0; set_solver.
    - rewrite bool_decide_eq_false_2; last auto; simpl.
      apply IHn in HγRs as [? ?].
      split; set_solver.
  Qed.

  Lemma recv_node_used_γRs (l : loc) (γR : gname) (n : node) :
    used_γRs (recv_node l γR n) ⊆ used_γRs n.
  Proof.
    induction n using node_prev_ind; simpl;
      destruct bool_decide; set_solver.
  Qed.

  Lemma recv_node_γRs_disjoint (l : loc) (γR : gname) (n : node) :
    γRs_disjoint n →
    γRs_disjoint (recv_node l γR n).
  Proof.
    induction n using node_prev_ind; simpl.
    - destruct bool_decide; auto with set_solver.
    - intros [? ?]. destruct bool_decide; simpl.
      + split; set_solver.
      + split; last auto.
        pose proof (recv_node_used_γRs l γR n). set_solver.
  Qed.

  Lemma wait_spec (b : val) (R : iProp Σ) :
    {{{ recv b R }}} wait b {{{ RET #(); R }}}.
  Proof.
    iIntros (Φ) "Hr HΦ".
    iDestruct "Hr" as (γ l γR R' ->) "(H◯r & HR & #HγR & _ & #Hinv)".
    iAssert (▷ (R' -∗ Φ #()))%I with "[HR HΦ]" as "HΦ".
    { iIntros "!> HR'". iApply "HΦ". by iApply "HR". }
    clear R. rename R' into R.
    iLöb as "IH" forall (l).
    wp_rec. wp_bind (! _)%E.
    iInv barrierN as (n)
      "(>%Hdisjoint & >H●s & >H●r & >H●≺ & >Hchain & Hres)" "Hclose".
    iDestruct (to_counter_prevs_lookup_receiver with "H●r H◯r")
      as %(c & prev & Hcounter_prev).
    iDestruct (node_chain_lookup_acc with "Hchain")
      as "(Hl_counter & Hl_prev & Hchain)"; first apply Hcounter_prev.
    wp_load.

    destruct c; last first.
    { (* We still have to wait for this node *)
      iSpecialize ("Hchain" with "Hl_counter Hl_prev").
      iMod ("Hclose" with "[H●s H●r H●≺ Hchain Hres]") as "_".
      { iExists n. by iFrame. }
      iModIntro. wp_op. wp_if_false. iApply ("IH" with "H◯r HΦ"). }

    (* We're done waiting for this node *)
    iDestruct "Hl_counter" as "#Hl_counter".
    iSpecialize ("Hchain" with "Hl_prev").
    iMod ("Hclose" with "[H●s H●r H●≺ Hchain Hres]") as "_".
    { iExists n. by iFrame. }
    clear n Hdisjoint Hcounter_prev prev.
    iModIntro. wp_op. wp_if_true. wp_op. wp_bind (! _)%E.

    (* Before we can do a recursive call, we need to open the invariant again,
       such that we can update the logical state. *)
    iInv barrierN as (n)
      "(>%Hdisjoint & >H●s & >H●r & >H●≺ & >Hchain & Hres)" "Hclose".
    iDestruct (to_γRs_lookup with "H●r H◯r") as %(γRs & HγR%singleton_subseteq_l & HγRs).
    iDestruct (to_counter_prevs_lookup_receiver with "H●r H◯r")
      as %(c & prev & Hcounter_prev).
    iDestruct (node_chain_lookup_acc with "Hchain")
      as "(Hl_counter' & Hl_prev & Hchain)"; first apply Hcounter_prev.
    (* We know that the counter hasn't been changed *)
    iDestruct (mapsto_agree with "Hl_counter Hl_counter'") as %[= <-%Nat2Z.inj].

    wp_load. iSpecialize ("Hchain" with "Hl_prev").
    destruct prev as [prev|].
    - (* There is a previous node we'll have to wait on.
         We pass on γR to the previous node and then wait on it. *)
      iDestruct (order_own_auth_no_dup with "H●≺") as %Hnodup.
      pose proof (to_locations_prev_node _ _ _ _ Hcounter_prev) as Hlocs.
      iMod (alloc_prev_singleton _ _ _ _ Hnodup Hlocs with "H●r") as "[H●r H◯prev]".
      iDestruct (to_γRs_lookup with "H●r H◯prev") as %(γRs' & _ & HγRs').

      assert (γRs ## γRs').
      { assert (l ≠ prev) as Hneq.
        { destruct Hlocs as (i & Hl & Hprev).
          by apply (NoDup_lookup_neq (to_locations n) i (S i)). }
        by apply (γRs_are_disjoint l prev _ _ n). }

      iMod (update_pass_on_γR with "H●r H◯r H◯prev") as "[H●r H◯prev]";
        [apply Hnodup | apply Hlocs | apply HγRs | apply HγRs' | set_solver |].

      iMod ("Hclose" with "[H●s H●r H●≺ Hchain Hres]") as "_".
      { iExists (pass_on_γR l γR n).
        rewrite to_γPs_pass_on_γR to_locations_pass_on_γR
                node_chain_pass_on_γR.
        iFrame. iSplit.
        { iPureIntro. eapply pass_on_γR_γRs_disjoint; set_solver. }
        iApply (pass_on_γR_resources with "Hres");
          [ apply Hnodup | apply Hlocs | apply Hcounter_prev
          | apply HγRs | apply HγRs' | apply HγR | set_solver]. }

      iModIntro. wp_match. iApply ("IH" with "H◯prev HΦ").
    - (* There is no previous node, we're done waiting.
         As this is the last node, we can pull out R from the resources predicate. *)
      iMod (update_recv_node with "H●r H◯r") as "H●r"; first apply HγRs.
      iDestruct (recv_node_resources_last with "HγR Hres") as "[HR Hres]";
        [apply Hcounter_prev | apply HγRs | apply HγR |].
      iMod ("Hclose" with "[H●s H●r H●≺ Hchain Hres]") as "_".
      { iExists (recv_node l γR n).
        rewrite to_γPs_recv_node to_locations_recv_node
                node_chain_recv_node.
        rewrite (to_γRs_recv_node _ _ _ _ HγRs).
        iFrame. iPureIntro. by apply recv_node_γRs_disjoint. }
      iModIntro. wp_match. by iApply "HΦ".
  Qed.
End wait_spec.

Section renunciation.
  Context `{!heapGS Σ, !barrierG Σ}.

  (* To renounce we need to edit two nodes in the chain.
     The sender node should always proceed the receiver node is the chain,
     hence why we first update the sender and then use recv_node to update the receiver.
     We could also have done this using a single fixpoint,
     but this would make our proofs with induction way more complex. *)
  Fixpoint renounce_node (lr ls : loc) (γR γP new_γP : gname) (n : node) : node :=
    if (bool_decide (ls = n.(location)))
      then mk_node n.(location) n.(γRs) (({[new_γP]} ∪ n.(γPs)) ∖ {[γP]})
            (recv_node lr γR <$> n.(prev))
      else mk_node n.(location) n.(γRs) n.(γPs)
            (renounce_node lr ls γR γP new_γP <$> n.(prev)).

  Lemma renounce_node_location (lr ls : loc) (γR γP new_γP : gname) (n : node) :
    (renounce_node lr ls γR γP new_γP n).(location) = n.(location).
  Proof.
    destruct n; simpl; destruct (bool_decide (ls = location0)); reflexivity.
  Qed.

  Lemma to_locations_renounce_node (lr ls : loc) (γR γP new_γP : gname) (n : node) :
    to_locations (renounce_node lr ls γR γP new_γP n) = to_locations n.
  Proof.
    induction n using node_prev_ind; simpl;
      destruct (bool_decide (ls = l')); simpl;
      try rewrite IHn; try rewrite to_locations_recv_node;
      reflexivity.
  Qed.

  Lemma to_counter_prevs_renounce_node (lr ls : loc) (γR γP new_γP : gname)
      (γPs : gset gname) (n : node) :
    to_γPs n !! ls = Some (GSet γPs) →
    γP ∈ γPs →
    new_γP ∉ γPs →
    to_counter_prevs (renounce_node lr ls γR γP new_γP n) = to_counter_prevs n.
  Proof.
    intros HγPs HγP Hnew_γP.
    induction n using node_prev_ind; simpl in *.
    - rewrite insert_empty lookup_singleton_Some in HγPs.
      destruct HγPs as [-> [= ->]].
      rewrite bool_decide_eq_true_2; auto; simpl.
      rewrite size_difference; last set_solver.
      rewrite size_union; last set_solver.
      rewrite !size_singleton. by rewrite minus_plus.
    - rewrite lookup_insert_Some in HγPs.
      destruct HγPs as [[-> [= ->]] | [Hneq HγPs]].
      + rewrite bool_decide_eq_true_2; auto; simpl.
        rewrite size_difference; last set_solver.
        rewrite size_union; last set_solver.
        rewrite !size_singleton. rewrite minus_plus.
        rewrite recv_node_location.
        by rewrite to_counter_prevs_recv_node.
      + rewrite bool_decide_eq_false_2; auto; simpl.
        rewrite renounce_node_location.
        by rewrite (IHn HγPs).
  Qed.

  Lemma node_chain_renounce_node (lr ls : loc) (γR γP new_γP : gname)
      (γPs : gset gname) (n : node) :
    to_γPs n !! ls = Some (GSet γPs) →
    γP ∈ γPs →
    new_γP ∉ γPs →
    node_chain (renounce_node lr ls γR γP new_γP n) ⊣⊢ node_chain n.
  Proof.
    intros HγPs HγP Hnew_γP. rewrite /node_chain.
    by rewrite (to_counter_prevs_renounce_node _ _ _ _ _ _ _ HγPs HγP Hnew_γP).
  Qed.

  Lemma to_γPs_renounce_node (lr ls : loc) (γR γP new_γP : gname)
      (γPs : gset gname) (n : node) :
    to_γPs n !! ls = Some (GSet γPs) →
    to_γPs (renounce_node lr ls γR γP new_γP n)
      = <[ls := GSet (({[new_γP]} ∪ γPs) ∖ {[γP]})]> (to_γPs n).
  Proof.
    intros HγPs.
    induction n using node_prev_ind; simpl in *.
    - rewrite insert_empty lookup_singleton_Some in HγPs.
      destruct HγPs as [-> [= ->]].
      rewrite bool_decide_eq_true_2; last auto; simpl.
      by rewrite insert_insert.
    - rewrite lookup_insert_Some in HγPs.
      destruct HγPs as [[-> [= ->]] | [Hneq HγPs]].
      + rewrite bool_decide_eq_true_2; last auto; simpl.
        rewrite insert_insert.
        by rewrite to_γPs_recv_node.
      + rewrite bool_decide_eq_false_2; last auto; simpl.
        rewrite insert_commute; last auto.
        by rewrite IHn.
  Qed.

  Lemma to_γRs_renounce_node (lr ls : loc) (γR γP new_γP : gname)
      (γRs : gset gname) (n : node) :
    NoDup (to_locations n) →
    (∃ (i j : nat), i < j ∧ to_locations n !! i = Some ls ∧ to_locations n !! j = Some lr) →
    to_γRs n !! lr = Some (GSet γRs) →
    to_γRs (renounce_node lr ls γR γP new_γP n)
      = <[lr:=GSet (γRs ∖ {[γR]})]> (to_γRs n).
  Proof.
    intros Hnodup (i & j & Hle & Hi & Hj) Hlookup.
    assert (ls ≠ lr) as Hneq.
    { apply (NoDup_lookup_neq (to_locations n) i j); auto with lia. }
    revert i j Hi Hj Hle.
    induction n using node_prev_ind; intros; simpl in *.
    { rewrite !list_lookup_singleton_Some in Hi, Hj. lia. }
    destruct i.
    - destruct j; first lia.
      injection Hi as ->.
      rewrite lookup_cons in Hj.
      rewrite bool_decide_eq_true_2; last auto; simpl.
      rewrite lookup_insert_ne in Hlookup; last auto.
      rewrite (to_γRs_recv_node _ _ _ _ Hlookup).
      by rewrite insert_commute.
    - destruct j; first lia.
      assert (l' ≠ ls) as Hneqs.
      { by apply (NoDup_lookup_neq _ 0 (S i) _ _ Hnodup (lookup_cons _ _ 0) Hi). }
      assert (l' ≠ lr) as Hneqr.
      { by apply (NoDup_lookup_neq _ 0 (S j) _ _ Hnodup (lookup_cons _ _ 0) Hj). }
      rewrite bool_decide_eq_false_2; last auto; simpl.
      rewrite lookup_insert_ne in Hlookup; last auto.
      apply NoDup_cons_1_2 in Hnodup.
      rewrite (IHn Hnodup Hlookup i j Hi Hj); last lia.
      by rewrite insert_commute.
  Qed.

  Lemma recv_node_resources (lr : loc) (γR : gname)
      (γRs : gset gname) (R Qacc : iProp Σ) (n : node) :
    to_γRs n !! lr = Some (GSet γRs) →
    γR ∈ γRs →
    saved_prop_own γR DfracDiscarded R -∗
    resources n Qacc -∗
    resources (recv_node lr γR n) (▷ R ∗ Qacc).
  Proof.
    iIntros (Hlookup HγR) "#HγR Hres".
    iInduction n as [|] "IH" using node_prev_ind forall (Qacc); simpl in *.
    - rewrite insert_empty lookup_singleton_Some in Hlookup.
      destruct Hlookup as [-> [= <-]].
      rewrite bool_decide_eq_true_2; auto; simpl.
      iIntros "HPs".
      iDestruct ("Hres" with "HPs") as "[HRs $]".
      iDestruct (big_sepS_delete with "HRs") as "[HR $]"; first apply HγR.
      iDestruct "HR" as (R') "[#HγR' HR]".
      iDestruct (saved_prop_agree with "HγR HγR'") as "#Heq".
      iNext. by iRewrite "Heq".
    - rewrite lookup_insert_Some in Hlookup.
      destruct Hlookup as [[-> [= <-]] | [Hneq Hlookup]].
      + rewrite bool_decide_eq_true_2; last auto; simpl.
        iApply (resources_weaken with "Hres").
        iIntros "Hres HPs".
        iDestruct ("Hres" with "HPs") as "[HRs $]".
        iDestruct (big_sepS_delete with "HRs") as "[HR $]"; first apply HγR.
        iDestruct "HR" as (R') "[#HγR' HR]".
        iDestruct (saved_prop_agree with "HγR HγR'") as "#Heq".
        iNext. by iRewrite "Heq".
      + rewrite bool_decide_eq_false_2; last auto; simpl.
        iSpecialize ("IH" $! Hlookup with "Hres").
        iApply (resources_weaken with "IH").
        iIntros "[HR Hres] HPs".
        by iDestruct ("Hres"with "HPs") as "[$ $]".
  Qed.

  Lemma renounce_node_resources (lr ls : loc) (γR γP new_γP : gname)
      (γRs γPs : gset gname) (P P' R R' Qacc : iProp Σ) (n : node) :
    NoDup (to_locations n) →
    (∃ (i j : nat), i < j ∧ to_locations n !! i = Some ls ∧ to_locations n !! j = Some lr) →
    to_γPs n !! ls = Some (GSet γPs) →
    to_γRs n !! lr = Some (GSet γRs) →
    γP ∈ γPs →
    new_γP ∉ γPs →
    γR ∈ γRs →
    (P -∗ P') -∗
    (R' -∗ R) -∗
    saved_prop_own γP DfracDiscarded P' -∗
    saved_prop_own new_γP DfracDiscarded (R -∗ P) -∗
    saved_prop_own γR DfracDiscarded R' -∗
    resources n Qacc -∗
    resources (renounce_node lr ls γR γP new_γP n) Qacc.
  Proof.
    intros Hnodup (i & j & Hle & Hi & Hj) HγPs HγRs HγP Hnew_γP HγR.
    iIntros "HP' HR #HγP #HγP_new #HγR Hres".
    assert (ls ≠ lr) as Hneq.
    { apply (NoDup_lookup_neq _ _ _ _ _ Hnodup Hi Hj). lia. }
    iInduction n as [|] "IH" using node_prev_ind
      forall (i j Qacc Hle Hi Hj); simpl in *.
    { rewrite !list_lookup_singleton_Some in Hi Hj. lia. }
    destruct i.
    - destruct j; first lia.
      injection Hi as ->.
      rewrite lookup_cons in Hj.
      rewrite bool_decide_eq_true_2; last auto; simpl.
      rewrite lookup_insert_ne in HγRs; last auto.
      rewrite lookup_insert in HγPs.
      injection HγPs as ->.
      iDestruct (recv_node_resources with "HγR Hres")
        as "Hres"; [apply HγRs | apply HγR |].
      iApply (resources_weaken with "Hres").
      iIntros "[HR' Hres] HPs".
      iApply "Hres".
      assert (({[new_γP]} ∪ γPs) ∖ {[γP]} = {[new_γP]} ∪ (γPs ∖ {[γP]}))
        as -> by set_solver.
      iDestruct (big_sepS_insert with "HPs") as "[HnewP HPs]"; first set_solver.
      iDestruct "HnewP" as (S) "[#HγP_new' HS]".
      iApply big_sepS_delete; first apply HγP.
      iFrame "HPs". iExists P'. iFrame "HγP".
      iDestruct (saved_prop_agree with "HγP_new HγP_new'") as "#Heq".
      iNext. iApply "HP'". iRewrite -"Heq" in "HS".
      iApply "HS". by iApply "HR".
    - destruct j; first lia.
      assert (l' ≠ ls) as Hneqs.
      { by apply (NoDup_lookup_neq _ 0 (S i) _ _ Hnodup (lookup_cons _ _ 0) Hi). }
      assert (l' ≠ lr) as Hneqr.
      { by apply (NoDup_lookup_neq _ 0 (S j) _ _ Hnodup (lookup_cons _ _ 0) Hj). }
      rewrite bool_decide_eq_false_2; last auto; simpl.
      rewrite lookup_insert_ne in HγPs; last auto.
      rewrite lookup_insert_ne in HγRs; last auto.
      apply NoDup_cons_1_2 in Hnodup.
      iApply ("IH" $! Hnodup HγPs HγRs i j
                      with "[%] [//] [//] HP' HR Hres").
      lia.
  Qed.

  Lemma renounce_node_γRs_disjoint (ls lr : loc) (γR γP γP_new : gname) (n : node) :
    γRs_disjoint n →
    γRs_disjoint (renounce_node lr ls γR γP γP_new n).
  Proof.
    cut (used_γRs (renounce_node lr ls γR γP γP_new n) ⊆ used_γRs n
          ∧ (γRs_disjoint n → γRs_disjoint (renounce_node lr ls γR γP γP_new n)));
      first by intros [_ ?].
    induction n using node_prev_ind; simpl.
    - destruct bool_decide; auto with set_solver.
    - destruct IHn as [? ?]. destruct bool_decide; simpl.
      + pose proof (recv_node_used_γRs lr γR n).
        split; first set_solver.
        intros [? ?]. split; first set_solver.
        by apply recv_node_γRs_disjoint.
      + split; first set_solver.
        intros [? ?]. split; set_solver.
  Qed.

  Lemma renunciation (b1 b2 : val) (R P : iProp Σ) (E : coPset) :
    ↑barrierN ⊆ E →
    recv b1 R -∗ send b2 P -∗ b1 ≺ b2
      ={E}=∗ send b2 (R -∗ P).
  Proof.
    iIntros (?).
    iDestruct 1 as (γ lr γR R' ->) "(H◯r & HR & #HγR & #Hlr_meta & #Hinv)".
    iDestruct 1 as (γ' ls γP P' ->) "(H◯s & HP' & #HγP & #Hls_meta & _)".
    iDestruct 1 as (γ'' ? ? [= <-] [= <-]) "(#Hlr_meta' & #Hls_meta' & #H≺)".
    iDestruct (meta_agree with "Hlr_meta Hlr_meta'") as %<-.
    iDestruct (meta_agree with "Hls_meta Hls_meta'") as %->.
    iInv barrierN as (n)
      "(>%Hdisjoint & >H●s & >H●r & >H●≺ & >Hchain & Hres)" "Hclose".
    iDestruct (to_γRs_lookup with "H●r H◯r")
      as %(γRs & HγR%singleton_subseteq_l & HγRs).
    iDestruct (to_γPs_lookup with "H●s H◯s")
      as %(γPs & HγP%singleton_subseteq_l & HγPs).

    iMod (saved_prop_alloc_cofinite γPs (R -∗ P) DfracDiscarded)
      as (γP_new HγP_new) "#HγP_new"; first done.
    iMod (update_recv_node with "H●r H◯r") as "H●r"; first apply HγRs.
    iMod (own_update_2 _ _ _
            (● _ ⋅ ◯ {[ls := GSet {[γP_new]} ⋅ GSet {[γP]}]})
            with "H●s H◯s") as "[H●s [H◯s_new H◯s]]".
    { apply auth_update. eapply singleton_local_update.
      - apply HγPs.
      - apply gset_disj_alloc_op_local_update. set_solver. }
    rewrite gset_disj_union; last set_solver.
    iMod (own_update_2 with "H●s H◯s") as "[H●s _]".
    { apply auth_update. eapply insert_local_update.
      - apply lookup_insert.
      - by rewrite lookup_singleton.
      - apply gset_disj_dealloc_local_update. }
    rewrite insert_insert.

    iMod ("Hclose" with "[H●s H●r H●≺ Hchain HP' HR Hres]") as "_".
    { iExists (renounce_node lr ls γR γP γP_new n).
      rewrite to_locations_renounce_node.
      rewrite (node_chain_renounce_node _ _ _ _ _ _ _ HγPs HγP HγP_new).
      iDestruct (order_own_auth_no_dup with "H●≺") as %Hnodup.
      iDestruct (order_own_ordered_indices with "H●≺ H≺") as %Horder.
      rewrite (to_γRs_renounce_node lr ls γR γP γP_new γRs n Hnodup Horder HγRs).
      rewrite (to_γRs_recv_node _ _ _ _ HγRs).
      rewrite (to_γPs_renounce_node _ _ _ _ _ _ _ HγPs).
      iFrame. iSplit.
      { iPureIntro. by apply renounce_node_γRs_disjoint. }
      by iApply (renounce_node_resources with "HP' HR HγP HγP_new HγR Hres"). }
    iExists γ, ls, γP_new, (R -∗ P)%I. iModIntro.
    iFrame "H◯s_new HγP_new Hls_meta Hinv".
    iSplit; first auto. iIntros "$".
  Qed.
End renunciation.

Section recv_weaken.
  Context `{!heapGS Σ, !barrierG Σ}.

  Lemma recv_weaken (b : val) (R1 R2 : iProp Σ) :
    (R1 -∗ R2) -∗ recv b R1 -∗ recv b R2.
  Proof.
    iIntros "HR2".
    iDestruct 1 as (γ l γR R ->) "(H◯ & HR1 & #HγR & #Hmeta & #Hinv)".
    iExists γ, l, γR, R. iFrame "H◯ HγR Hmeta Hinv".
    iSplit; first auto. iIntros "HR".
    iApply "HR2". by iApply "HR1".
  Qed.
End recv_weaken.

Section clone_spec.
  Context `{!heapGS Σ, !barrierG Σ}.

  Fixpoint send_split_node (l : loc) (γP γP1 γP2 : gname) (n : node) : node :=
    if bool_decide (l = n.(location))
      then mk_node n.(location) n.(γRs)
            ({[γP1; γP2]} ∪ (n.(γPs) ∖ {[γP]})) n.(prev)
      else mk_node n.(location) n.(γRs) n.(γPs) (send_split_node l γP γP1 γP2 <$> n.(prev)).

  Lemma send_split_node_location (l : loc) (γP γP1 γP2 : gname) (n : node) :
    (send_split_node l γP γP1 γP2 n).(location) = n.(location).
  Proof.
    destruct n. simpl. destruct (bool_decide (l = location0)); reflexivity.
  Qed.

  Lemma to_γRs_send_split_node (l : loc) (γP γP1 γP2 : gname) (n : node) :
    to_γRs (send_split_node l γP γP1 γP2 n) = to_γRs n.
  Proof.
    induction n using node_prev_ind; simpl.
    - destruct (bool_decide (l = l')); reflexivity.
    - destruct (bool_decide (l = l')); simpl;
        [|rewrite IHn]; reflexivity.
  Qed.

  Lemma to_γPs_send_split_node (l : loc) (γP γP1 γP2 : gname) (γPs : gset gname) (n : node) :
    to_γPs n !! l = Some (GSet γPs) →
    to_γPs (send_split_node l γP γP1 γP2 n)
      = <[l := GSet ({[γP1; γP2]} ∪ (γPs ∖ {[γP]}))]> (to_γPs n).
  Proof.
    intros Hlookup. induction n using node_prev_ind; simpl in *.
    - rewrite insert_empty in Hlookup.
      rewrite lookup_singleton_Some in Hlookup.
      destruct Hlookup as [<- [= <-]].
      rewrite bool_decide_eq_true_2; last done.
      simpl. by rewrite insert_insert insert_empty.
    - destruct (decide (l = l')) as [<- | Hneq].
      + rewrite bool_decide_eq_true_2; last done. simpl.
        rewrite lookup_insert in Hlookup.
        injection Hlookup as ->.
        by rewrite insert_insert.
      + rewrite bool_decide_eq_false_2; last done. simpl.
        rewrite lookup_insert_ne in Hlookup; last done.
        rewrite insert_commute; last done.
        rewrite IHn; auto with set_solver.
  Qed.

  Lemma to_locations_send_split_node (l : loc) (γP γP1 γP2 : gname) (n : node) :
    to_locations (send_split_node l γP γP1 γP2 n) = to_locations n.
  Proof.
    induction n using node_prev_ind; simpl.
    - destruct (bool_decide (l = l')); reflexivity.
    - destruct (bool_decide (l = l')); simpl;
        [|rewrite IHn]; reflexivity.
  Qed.

  Lemma to_counter_prevs_send_split_node (l : loc) (γP γP1 γP2 : gname)
      (c : nat) (prev : option loc) (γPs : gset gname) (n : node) :
    to_γPs n !! l = Some (GSet γPs) →
    γP ∈ γPs →
    γP1 ∉ γPs →
    γP2 ∉ γPs →
    γP1 ≠ γP2 →
    to_counter_prevs n !! l = Some (c, prev) →
    to_counter_prevs (send_split_node l γP γP1 γP2 n)
      = <[l := (S c, prev)]> (to_counter_prevs n).
  Proof.
    intros HγPs HγP HγP1 HγP2 Hneq Hlookup.
    induction n using node_prev_ind; simpl in *.
    - rewrite insert_empty lookup_singleton_Some in Hlookup.
      destruct Hlookup as [<- [= <- <-]].
      rewrite lookup_insert in HγPs.
      injection HγPs as <-.
      rewrite bool_decide_eq_true_2; last auto; simpl.
      rewrite insert_insert.
      replace ({[γP1; γP2]} ∪ (γPs' ∖ {[γP]})) with (({[γP1; γP2]} ∪ γPs') ∖ {[γP]})
        by set_solver.
      rewrite size_difference; last set_solver.
      rewrite size_union; last set_solver.
      rewrite size_union; last set_solver.
      rewrite !size_singleton.
      by replace (1 + 1 + size γPs' - 1) with (S (size γPs')) by lia.
    - rewrite lookup_insert_Some in Hlookup.
      destruct Hlookup as [[<- [= <- <-]] | [? Hlookup]].
      + rewrite lookup_insert in HγPs.
        injection HγPs as <-.
        rewrite bool_decide_eq_true_2; last auto; simpl.
        rewrite insert_insert.
        replace ({[γP1; γP2]} ∪ (γPs' ∖ {[γP]})) with (({[γP1; γP2]} ∪ γPs') ∖ {[γP]})
          by set_solver.
        rewrite size_difference; last set_solver.
        rewrite size_union; last set_solver.
        rewrite size_union; last set_solver.
        rewrite !size_singleton.
        by replace (1 + 1 + size γPs' - 1) with (S (size γPs')) by lia.
      + rewrite lookup_insert_ne in HγPs; last auto.
        rewrite bool_decide_eq_false_2; last auto; simpl.
        rewrite send_split_node_location.
        rewrite (IHn HγPs Hlookup).
        by rewrite insert_commute.
  Qed.

  Lemma send_split_node_γRs_disjoint (l : loc) (γP γP1 γP2 : gname) (n : node) :
    γRs_disjoint n → γRs_disjoint (send_split_node l γP γP1 γP2 n).
  Proof.
    cut (used_γRs (send_split_node l γP γP1 γP2 n) = used_γRs n
          ∧ (γRs_disjoint n → γRs_disjoint (send_split_node l γP γP1 γP2 n)));
      first by intros [_ ?].
    induction n using node_prev_ind; simpl;
      destruct bool_decide; simpl; split; set_solver.
  Qed.

  Lemma node_chain_send_split_node_acc (l : loc) (γP γP1 γP2 : gname)
      (c : nat) (γPs : gset gname) (prev : option loc) (n : node) :
    to_γPs n !! l = Some (GSet γPs) →
    γP ∈ γPs →
    γP1 ∉ γPs →
    γP2 ∉ γPs →
    γP1 ≠ γP2 →
    to_counter_prevs n !! l = Some (S c, prev) →
    node_chain n -∗
      l ↦ #(S c) ∗
      (l ↦ #(S (S c)) -∗ node_chain (send_split_node l γP γP1 γP2 n)).
  Proof.
    iIntros (HγPs HγP HγP1 HγP2 Hneq Hlookup) "Hchain".
    iDestruct (big_sepM_insert_acc with "Hchain")
      as "[[Hl_counter Hl_prev] Hchain]"; first apply Hlookup; simpl.
    iFrame "Hl_counter". iIntros "Hl_counter".
    iSpecialize ("Hchain" $! (S (S c), prev) with "[$Hl_counter $Hl_prev]").
    rewrite /node_chain.
    by rewrite (to_counter_prevs_send_split_node _ _ _ _ _ _ _ _
                  HγPs HγP HγP1 HγP2 Hneq Hlookup).
  Qed.

  Lemma send_split_node_resources (l : loc) (γP γP1 γP2 : gname)
      (γPs : gset gname) (P P1 P2 Qacc : iProp Σ) (n : node) :
    to_γPs n !! l = Some (GSet γPs) →
    γP ∈ γPs →
    γP1 ∉ γPs →
    γP2 ∉ γPs →
    γP1 ≠ γP2 →
    (P1 ∗ P2 -∗ P) -∗
    saved_prop_own γP DfracDiscarded P -∗
    saved_prop_own γP1 DfracDiscarded P1 -∗
    saved_prop_own γP2 DfracDiscarded P2 -∗
    resources n Qacc -∗
    resources (send_split_node l γP γP1 γP2 n) Qacc.
  Proof.
    iIntros (Hlookup HγP HγP1 HγP2 Hneq) "HP #HγP #HγP1 #HγP2 Hres".
    iInduction n as [|] "IH" using node_prev_ind forall (Qacc); simpl in *.
    - rewrite insert_empty lookup_singleton_Some in Hlookup.
      destruct Hlookup as [<- [= <-]] .
      rewrite bool_decide_eq_true_2; last auto; simpl.
      rewrite big_sepS_union; last set_solver.
      rewrite big_sepS_union; last set_solver.
      rewrite !big_sepS_singleton.
      iIntros "[[HP1 HP2] HPs]".
      iDestruct "HP1" as (P1') "[#HγP1' HP1]".
      iDestruct (saved_prop_agree with "HγP1 HγP1'") as "#Heq1".
      iDestruct "HP2" as (P2') "[#HγP2' HP2]".
      iDestruct (saved_prop_agree with "HγP2 HγP2'") as "#Heq2".
      iApply "Hres". iApply (big_sepS_delete with "[- $HPs]"); first auto.
      iExists P. iFrame "HγP". iNext. iApply "HP".
      iRewrite "Heq1". iRewrite "Heq2". iFrame.
    - rewrite lookup_insert_Some in Hlookup.
      destruct Hlookup as [[<- [= <-]] | [Hneq' Hlookup]].
      + rewrite bool_decide_eq_true_2; last auto; simpl.
        iApply (resources_weaken with "Hres").
        iIntros "Hres".
        rewrite big_sepS_union; last set_solver.
        rewrite big_sepS_union; last set_solver.
        rewrite !big_sepS_singleton.
        iIntros "[[HP1 HP2] HPs]".
        iDestruct "HP1" as (P1') "[#HγP1' HP1]".
        iDestruct (saved_prop_agree with "HγP1 HγP1'") as "#Heq1".
        iDestruct "HP2" as (P2') "[#HγP2' HP2]".
        iDestruct (saved_prop_agree with "HγP2 HγP2'") as "#Heq2".
        iApply "Hres". iApply (big_sepS_delete with "[- $HPs]"); first auto.
        iExists P. iFrame "HγP". iNext. iApply "HP".
        iRewrite "Heq1". iRewrite "Heq2". iFrame.
      + rewrite bool_decide_eq_false_2; last auto; simpl.
        iApply ("IH" $! Hlookup with "HP Hres").
  Qed.

  Lemma clone_spec (b : val) (P1 P2 : iProp Σ) :
    {{{ send b (P1 ∗ P2) }}} clone b {{{ RET #(); send b P1 ∗ send b P2 }}}.
  Proof.
    iIntros (Φ) "Hs HΦ".
    iDestruct "Hs" as (γ l γP P ->) "(H◯s & HP & #HγP & #Hmeta & #Hinv)".
    wp_lam. wp_bind (FAA _ _).

    iInv barrierN as (n)
      "(>%Hdisjoint & >H●s & >H●r & >H●≺ & >Hchain & Hres)" "Hclose".
    iDestruct (to_γPs_lookup with "H●s H◯s") as %(γPs & HγP%singleton_subseteq_l & HγPs).
    iDestruct (to_counter_prevs_lookup_sender with "H●s H◯s")
      as %(c & prev & Hlookup).

    (* Allocate the new saved props and replace γP with them *)
    iMod (saved_prop_alloc_cofinite γPs P1 DfracDiscarded)
      as (γP1 Hfresh1) "#HγP1"; first done.
    iMod (saved_prop_alloc_cofinite ({[γP1]} ∪ γPs) P2 DfracDiscarded)
      as (γP2 Hfresh2) "#HγP2"; first done.
    iMod (own_update_2 with "H●s H◯s") as "[H●s H◯s]".
    { apply auth_update. eapply singleton_local_update.
      - apply HγPs.
      - apply gset_disj_dealloc_local_update. }
      iMod (own_update_2 _ _ _
            (● _ ⋅ (◯ {[l := GSet {[γP1]} ⋅ GSet {[γP2]}]}))
      with "H●s H◯s") as "(H●s & H◯s1 & H◯s2)".
    { apply auth_update. eapply singleton_local_update.
      - apply lookup_insert.
      - rewrite gset_disj_union; last set_solver.
        apply gset_disj_alloc_empty_local_update. set_solver. }
    iEval (rewrite insert_insert) in "H●s".

    iDestruct (node_chain_send_split_node_acc l γP γP1 γP2 with "Hchain")
      as "[Hl_counter Hchain]"; [apply HγPs | set_solver.. | apply Hlookup |].
    wp_faa. replace (S c + 1)%Z with (S (S c) : Z) by lia.
    iSpecialize ("Hchain" with "Hl_counter").

    (* Close the invariant again *)
    iMod ("Hclose" with "[H●s H●r H●≺ Hchain HP Hres]") as "_".
    { iExists (send_split_node l γP γP1 γP2 n).
      rewrite to_γRs_send_split_node to_locations_send_split_node.
      rewrite (to_γPs_send_split_node _ _ _ _ _ _ HγPs).
      iFrame. iSplit. { iPureIntro. by apply send_split_node_γRs_disjoint. }
      iApply (send_split_node_resources with "HP HγP HγP1 HγP2 Hres");
        [apply HγPs | set_solver..]. }

    iModIntro. wp_seq. iApply "HΦ". iSplitL "H◯s1".
    - iExists γ, l, γP1, P1. auto 10 with iFrame.
    - iExists γ, l, γP2, P2. auto 10 with iFrame.
  Qed.
End clone_spec.

Section send_strengthen.
  Context `{!heapGS Σ, !barrierG Σ}.

  Lemma send_strengthen (b : val) (P1 P2 : iProp Σ) :
    (P2 -∗ P1) -∗ send b P1 -∗ send b P2.
  Proof.
    iIntros "HP1".
    iDestruct 1 as (γ l γP P ->) "(H◯ & HP & #HγP & #Hmeta & #Hinv)".
    iExists γ, l, γP, P. iFrame "H◯ HγP Hmeta Hinv".
    iSplit; first auto. iIntros "HP2".
    iApply "HP". by iApply "HP1".
  Qed.
End send_strengthen.

Definition chainable_barrier `{!heapGS Σ, !barrierG Σ} : chainable_barrier_spec Σ barrierN :=
  {| specification.new_barrier := new_barrier;
     specification.signal := signal;
     specification.wait := wait;
     specification.extend := extend;
     specification.clone := clone;
     specification.send := send;
     specification.recv := recv;
     specification.earlier := earlier;
     specification.earlier_persistent := earlier_persistent;
     specification.earlier_timeless := earlier_timeless;
     specification.earlier_trans := earlier_trans;
     specification.renunciation := renunciation;
     specification.recv_split := recv_split;
     specification.recv_weaken := recv_weaken;
     specification.send_strengthen := send_strengthen;
     specification.new_barrier_spec := new_barrier_spec;
     specification.signal_spec := signal_spec;
     specification.wait_spec := wait_spec;
     specification.extend_spec := extend_spec;
     specification.clone_spec := clone_spec;
  |}.

Global Opaque new_barrier signal wait extend clone.
Global Typeclasses Opaque barrier_inv.
Global Opaque barrier_inv.
Global Typeclasses Opaque send.
Global Opaque send.
Global Typeclasses Opaque recv.
Global Opaque recv.
Global Typeclasses Opaque earlier.
Global Opaque earlier.
