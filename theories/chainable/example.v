From iris.heap_lang Require Export notation lang lib.par adequacy.
From iris.proofmode Require Export proofmode.
From iris.heap_lang Require Import proofmode.
From iris.prelude Require Import options.

From barriers.chainable Require Import verification.

Section example.
  Context `{!heapGS Σ, !spawnG Σ, !barrierG Σ}.

  Definition example : val :=
    λ: "x" "y" "z",
      let: "l" := ref #0 in
      let: "b1" := new_barrier #() in
      let: "b2" := extend "b1" in
      ("l" <- ! "l" + "x";; signal "b2")
        ||| ((if: "y" = #0 then #() else wait "b2";; "l" <- ! "l" + "y");; signal "b1")
        ||| (wait "b1";; "l" <- ! "l" + "z");;
      ! "l".

  Lemma example_spec (x y z : nat) :
    {{{ True }}} example #x #y #z {{{ RET #(x + y + z); True }}}.
  Proof using Type*.
    iIntros (Φ) "_ HΦ".
    wp_lam. wp_let. wp_alloc l as "Hl". wp_let.
    wp_apply (new_barrier_spec (l ↦ #(x + y)) with "[//]").
    iIntros (b1) "[Hs1 Hr1]". wp_let.
    wp_apply (extend_spec (l ↦ #x) ∅ with "[$Hs1]"); first auto.
    iIntros (b2) "(Hs2 & Hr2 & Hs1 & #H≺ & _)".
    wp_let.
    wp_smart_apply (wp_par (λ _, ⌜True⌝)%I (λ _, l ↦ #(x + y + z))%I
      with "[Hl Hs2 Hr2 Hs1] [Hr1]");
    first wp_smart_apply (wp_par (λ _, ⌜True⌝)%I (λ _, ⌜True⌝)%I
        with "[Hl Hs2] [Hr2 Hs1]"); [| |auto| |].
    { (* Thread 1 *)
      wp_load. wp_store. rewrite Z.add_0_l.
      by wp_apply (signal_spec with "[$Hs2 $Hl]"). }
    { (* Thread 2 *)
      wp_op. destruct (decide (y = 0)) as [-> | Hneq].
      - wp_if_true. wp_seq.
        iMod (renunciation with "Hr2 Hs1 H≺") as "Hs1"; first done.
        wp_apply (signal_spec with "[$Hs1]"); last done.
        rewrite Z.add_0_r. auto.
      - rewrite bool_decide_eq_false_2; last first.
        { intros [=]. lia. }
        wp_if_false.
        wp_apply (wait_spec with "Hr2").
        iIntros "Hl". wp_seq. wp_load. wp_store.
        by iApply (signal_spec with "[$Hs1 $Hl]"). }
    { (* Thread 3 *)
      wp_apply (wait_spec with "Hr1").
      iIntros "Hl". wp_seq. wp_load. wp_op. by wp_store. }
    iIntros (v1 v2) "[_ Hl] !>".
    wp_seq. wp_load. by iApply "HΦ".
  Qed.
End example.

Section ClosedProofs.

Let Σ : gFunctors := #[ heapΣ ; barrierΣ ; spawnΣ ].

Lemma example_adequate (x y z : nat) σ : adequate NotStuck (example #x #y #z) σ (λ v _, v = #(x + y + z)).
Proof. apply (heap_adequacy Σ)=>?. iIntros "_". by iApply example_spec. Qed.

End ClosedProofs.

Print Assumptions example_adequate.
