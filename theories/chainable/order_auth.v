From iris.algebra Require Import auth gset.
From iris.base_logic.lib Require Import own.
From iris.proofmode Require Import proofmode.
From iris.bi Require Import relations.
From iris.prelude Require Import options.

Definition order_authR (A : Type) `{Countable A} :=
  authR (gsetUR (prodO (leibnizO A) (leibnizO A))).

Class order_authG Σ A `{Countable A} :=
  OrderAuthG { order_authG_inG : inG Σ (order_authR A); }.
Local Existing Instance order_authG_inG.
Global Hint Mode order_authG - ! - - : typeclass_instances.

Definition order_authΣ A `{Countable A}: gFunctors :=
  #[ GFunctor (order_authR A) ].
Global Instance subG_order_authΣ `{Countable A} Σ :
  subG (order_authΣ A) Σ → order_authG Σ A.
Proof. solve_inG. Qed.

Definition order_own_auth `{order_authG Σ A}
    (γ : gname) (xs : list A) : iProp Σ :=
  ∃ (X : gset (A * A)),
    ⌜NoDup xs⌝
    ∗ ⌜∀ (x y : A), (x, y) ∈ X ↔ ∃ (i j : nat), i < j ∧ xs !! i = Some x ∧ xs !! j = Some y⌝
    ∗ own γ (● X) ∗ own γ (◯ X).

Local Definition order_own_ordered_pre `{order_authG Σ A}
    (γ : gname) (x y : leibnizO A) : iProp Σ :=
  own γ (◯ {[ (x, y) ]}).

Definition order_own_ordered `{order_authG Σ A}
    (γ : gname) (x y : leibnizO A) : iProp Σ :=
  bi_tc (order_own_ordered_pre γ) x y.

Notation "●≺[ γ ] xs" := (order_own_auth γ xs)
  (at level 20, γ at level 50, format "●≺[ γ ]  xs") : bi_scope.
Notation "x ◯≺[ γ ] y" := (order_own_ordered γ x y)
  (at level 20, γ at level 50, format "x  ◯≺[ γ ]  y") : bi_scope.

Section order_auth.
  Context `{order_authG Σ A}.

  Global Instance order_own_auth_timeless (xs : list A) (γ : gname) :
    Timeless (●≺[γ] xs).
  Proof. apply _. Qed.

  Lemma order_own_auth_exclusive (xs ys : list A) (γ : gname) :
    ●≺[γ] xs -∗ ●≺[γ] ys -∗ False.
  Proof.
    iDestruct 1 as (X _ _) "[H● _]".
    iDestruct 1 as (Y _ _) "[H●' _]".
    iDestruct (own_valid_2 with "H● H●'") as %[]%auth_auth_op_valid.
  Qed.

  Global Instance order_own_ordered_timeless (x y : A) (γ : gname) :
    Timeless (x ◯≺[γ] y).
  Proof. apply _. Qed.

  Global Instance order_own_ordered_persistent (x y : A) (γ : gname) :
    Persistent (x ◯≺[γ] y).
  Proof. apply bi_tc_persistent; apply _. Qed.

  Lemma order_own_ordered_trans (x y z : A) (γ : gname) :
    x ◯≺[γ] y -∗
    y ◯≺[γ] z -∗
    x ◯≺[γ] z.
  Proof. apply bi_tc_trans. apply _. Qed.

  Lemma order_own_ordered_get (x y : leibnizO A) (xs : list A) (γ : gname) :
    (∃ (i j : nat), i < j ∧ xs !! i = Some x ∧ xs !! j = Some y) →
    ●≺[γ] xs -∗
    x ◯≺[γ] y.
  Proof.
    iIntros (Hij).
    iDestruct 1 as (X Hnodup Helem) "[H● H◯]".
    iApply bi_tc_once.
    iCombine "H● H◯" as "H".
    iApply (own_mono with "H").
    apply auth_frag_included.
    apply gset_included, singleton_subseteq_l.
    apply Helem. apply Hij.
  Qed.

  Lemma order_own_ordered_indices (x y : leibnizO A) (xs : list A) (γ : gname) :
    ●≺[γ] xs -∗
    x ◯≺[γ] y -∗
    ⌜(∃ (i j : nat), i < j ∧ xs !! i = Some x ∧ xs !! j = Some y)⌝.
  Proof.
    iDestruct 1 as (X Hnodup Helem) "[H● _]".
    iIntros "H". iRevert (x) "H H●". iApply bi_tc_ind_l.
    iIntros "!>" (x1). iIntros "[H | IH] H●".
    { iDestruct (own_valid_2 with "H● H") as
        %[Hin%gset_included%singleton_subseteq_l _]%auth_both_valid_discrete.
      iPureIntro. by apply Helem. }
    iDestruct "IH" as (x') "[H IH]".
    iDestruct (own_valid_2 with "H● H") as
        %[Hin%gset_included%singleton_subseteq_l _]%auth_both_valid_discrete.
    apply Helem in Hin as (i & j & Hle & Hi & Hj).
    iDestruct ("IH" with "H●") as %(j' & k & Hle' & Hj' & Hk).
    iPureIntro. exists i, k.
    rewrite (NoDup_lookup _ _ _ _ Hnodup Hj Hj') in Hle.
    repeat split; auto with lia.
  Qed.

  Lemma order_own_auth_no_dup (xs : list A) (γ : gname) :
    ●≺[γ] xs -∗ ⌜NoDup xs⌝.
  Proof.
    by iDestruct 1 as (? goal) "_".
  Qed.

  Lemma order_own_auth_alloc (xs : list A) :
    NoDup xs →
    ⊢ |==> ∃ (γ : gname), ●≺[γ] xs.
  Proof.
    iIntros (Hnodup).
    iInduction Hnodup as [|x xs] "IH".
    - iMod (own_alloc (● ∅ ⋅ ◯ ∅ : order_authR A)) as (γ) "[H● H◯]".
      { by apply auth_both_valid_discrete. }
      iModIntro. iExists γ, ∅. iFrame.
      iPureIntro. split; first apply NoDup_nil_2.
      intros x y. rewrite elem_of_empty.
      split; first (intros []).
      intros (i & j & ? & C & _).
      by rewrite lookup_nil in C.
    - iMod "IH" as (γ X _ Helem) "[H● _]".
      set (X' := X ∪ ⋃ ((λ y, {[(x, y)]}) <$> xs)).
      iMod (own_update _ _ (● X' ⋅ ◯ X') with "H●") as "[H● H◯]".
      { apply auth_update_alloc. apply gset_local_update. set_solver. }
      iModIntro. iExists γ, X'. iFrame "H● H◯".
      iPureIntro. split; first (by apply NoDup_cons).
      intros y z. split.
      + rewrite elem_of_union. intros [Hin | Hin].
        * apply Helem in Hin as (i & j & Hle & Hi & Hj).
          exists (S i), (S j). rewrite !lookup_cons.
          auto with lia.
        * apply elem_of_union_list in Hin
            as (? & (? & -> & Hin)%elem_of_list_fmap_2 & [= -> <-]%elem_of_singleton).
          apply elem_of_list_lookup in Hin as (j & Hj).
          exists 0, (S j). rewrite !lookup_cons.
          auto with lia.
      + intros (i & j & Hle & Hi & Hj). destruct i.
        * injection Hi as <-. subst X'.
          apply elem_of_union_r. apply elem_of_union_list.
          exists {[(x, z)]}. split; last set_solver.
          eapply elem_of_list_fmap_1_alt; last set_solver.
          destruct j; first lia.
          rewrite lookup_cons in Hj.
          apply elem_of_list_lookup. by exists j.
        * destruct j; first lia. rewrite !lookup_cons in Hi, Hj.
          subst X'. apply elem_of_union_l. apply Helem.
          exists i, j. auto with lia.
  Qed.

  Lemma order_own_auth_update (z : A) (xs ys : list A) (γ : gname) :
    z ∉ xs ++ ys →
    ●≺[γ] (xs ++ ys) ==∗ ●≺[γ] (xs ++ z :: ys).
  Proof.
    iIntros (Hfresh). iDestruct 1 as (X Hnodup Helem) "[H● _]".
    set (X' := X ∪ ⋃ ((λ x, {[(x, z)]}) <$> xs)
                 ∪ ⋃ ((λ y, {[(z, y)]}) <$> ys)).
    iMod (own_update _ _ (● X' ⋅ ◯ X') with "H●") as "[H● H◯]".
    { apply auth_update_alloc. apply gset_local_update. set_solver. }
    iModIntro. iExists X'. iFrame "H● H◯". iPureIntro. split.
    - rewrite -Permutation_middle.
      apply NoDup_cons. auto.
    - intros x y. split.
      + rewrite !elem_of_union. intros [[Hin | Hin] | Hin].
        * apply Helem in Hin as (i & j & Hle & Hi & Hj).
          apply lookup_app_Some in Hi as [Hi | [? Hi]].
          { apply lookup_app_Some in Hj as [? | [? ?]].
            - exists i, j. split; first lia.
              split; by apply lookup_app_l_Some.
            - exists i, (S j). repeat split; first lia.
              + by apply lookup_app_l_Some.
              + rewrite lookup_app_r; last lia.
                replace (S j - length xs) with (S (j - length xs)) by lia.
                by rewrite lookup_cons. }
          rewrite lookup_app_r in Hj; last lia.
          exists (S i), (S j).
          repeat split; first lia.
          { rewrite lookup_app_r; last lia.
            rewrite lookup_cons_ne_0; last lia.
            rewrite <- Hi. f_equal. lia. }
          { rewrite lookup_app_r; last lia.
            rewrite lookup_cons_ne_0; last lia.
            rewrite <- Hj. f_equal. lia. }
        * apply elem_of_union_list in Hin
            as (? & (? & -> & Hin)%elem_of_list_fmap_2 & [= <- ->]%elem_of_singleton).
          apply elem_of_list_lookup in Hin as (i & Hi).
          exists i, (length xs). repeat split.
          { by apply lookup_lt_Some with x. }
          { by apply lookup_app_l_Some. }
          { by apply list_lookup_middle. }
        * apply elem_of_union_list in Hin
            as (? & (? & -> & Hin)%elem_of_list_fmap_2 & [= -> <-]%elem_of_singleton).
          apply elem_of_list_lookup in Hin as (j & Hj).
          exists (length xs), (S j + length xs). repeat split; first lia.
          { by apply list_lookup_middle. }
          { rewrite lookup_app_r; last lia.
            rewrite lookup_cons_ne_0; last lia.
            rewrite <- Hj. f_equal. lia. }
      + intros (i & j & Hle & Hi & Hj).
        destruct (decide (i = length xs)) as [-> | [? | ?]%not_eq].
        * rewrite list_lookup_middle in Hi; last done.
          injection Hi as ->. subst X'.
          apply elem_of_union_r. apply elem_of_union_list.
          exists {[(x, y)]}. split; last set_solver.
          eapply elem_of_list_fmap_1_alt; last set_solver.
          rewrite lookup_app_r in Hj; last lia.
          rewrite lookup_cons_ne_0 in Hj; last lia.
          eapply elem_of_list_lookup_2. apply Hj.
        * destruct (decide (j = length xs)) as [-> | [? | ?]%not_eq].
          { rewrite list_lookup_middle in Hj; last done.
            injection Hj as ->. subst X'.
            apply elem_of_union_l. apply elem_of_union_r.
            apply elem_of_union_list.
            exists {[(x, y)]}. split; last set_solver.
            eapply elem_of_list_fmap_1_alt; last set_solver.
            rewrite lookup_app_l in Hi; last lia.
            apply elem_of_list_lookup. by exists i. }
          { rewrite lookup_app_l in Hi; last lia.
            rewrite lookup_app_l in Hj; last lia.
            subst X'. apply elem_of_union_l. apply elem_of_union_l.
            apply Helem. exists i, j. split; first lia.
            split; by apply lookup_app_l_Some. }
          { rewrite lookup_app_l in Hi; last lia.
            rewrite lookup_app_r in Hj; last lia.
            rewrite lookup_cons_ne_0 in Hj; last lia.
            subst X'. apply elem_of_union_l. apply elem_of_union_l.
            apply Helem. exists i, (j - 1). repeat split; first lia.
            - by apply lookup_app_l_Some.
            - rewrite lookup_app_r; last lia.
              rewrite <- Hj. f_equal. lia. }
        * rewrite lookup_app_r in Hi; last lia.
          rewrite lookup_cons_ne_0 in Hi; last lia.
          rewrite lookup_app_r in Hj; last lia.
          rewrite lookup_cons_ne_0 in Hj; last lia.
          subst X'. apply elem_of_union_l. apply elem_of_union_l.
          apply Helem. exists (i - 1), (j - 1). repeat split; first lia.
          { rewrite lookup_app_r; last lia.
            rewrite <- Hi. f_equal. lia. }
          { rewrite lookup_app_r; last lia.
            rewrite <- Hj. f_equal. lia. }
  Qed.
End order_auth.

Global Typeclasses Opaque order_own_auth.
Global Opaque order_own_auth.
Global Typeclasses Opaque order_own_ordered.
Global Opaque order_own_ordered.

Section properties.
  Context `{order_authG Σ A}.

  Lemma order_own_auth_update_insert_after (y x : A) (xs : list A) (γ : gname) :
    y ∉ xs →
    x ∈ xs →
    ●≺[γ] xs ==∗ ∃ (xs1 xs2 : list A),
      ⌜xs = xs1 ++ x :: xs2⌝
      ∗ ●≺[γ] (xs1 ++ x :: y :: xs2)
      ∗ x ◯≺[γ] y.
  Proof.
    iIntros (Hfresh Hin) "H●".
    iDestruct (order_own_auth_no_dup with "H●") as %Hnodup.
    apply elem_of_list_split in Hin as (xs1 & xs2 & Hxs).
    rewrite cons_middle app_assoc in Hxs.
    rewrite Hxs in Hnodup, Hfresh |- *.
    iMod (order_own_auth_update y with "H●") as "H●"; first apply Hfresh.
    iModIntro. iExists xs1, xs2.
    rewrite -!app_assoc.
    iSplit; first done.
    iDestruct (order_own_ordered_get x y with "H●") as "#Horder"; last by iFrame.
    exists (length xs1), (S (length xs1)).
    repeat split; first lia.
    - by apply list_lookup_middle.
    - rewrite app_assoc.
      apply list_lookup_middle.
      by rewrite app_length Nat.add_comm.
  Qed.

  Lemma order_own_auth_adjacent (x y z : A) (xs1 xs2 : list A) (γ : gname) :
    ●≺[γ] (xs1 ++ x :: y :: xs2) -∗
    x ◯≺[γ] z -∗
      ⌜z = y⌝ ∨ y ◯≺[γ] z.
  Proof.
    iIntros "H● Hxz".
    iDestruct (order_own_ordered_indices with "H● Hxz")
      as %(i & j & Hle & Hi & Hj).
    iDestruct (order_own_auth_no_dup with "H●") as %Hnodup.
    rewrite NoDup_alt in Hnodup. specialize Hnodup with i (length xs1) x.
    apply Hnodup in Hi as ->; last first.
    { rewrite lookup_app_r; last done. by rewrite Nat.sub_diag. }
    destruct (decide (j = S (length xs1))) as [-> |].
    - iLeft. iPureIntro.
      rewrite cons_middle app_assoc list_lookup_middle in Hj.
      { by injection Hj. }
      by rewrite app_length Nat.add_comm.
    - iRight. iApply (order_own_ordered_get with "H●").
      exists (S (length xs1)), j. repeat split; auto with lia.
      rewrite cons_middle app_assoc.
      apply list_lookup_middle.
      by rewrite app_length Nat.add_comm.
  Qed.

  Lemma order_own_ordered_elem_of (x y : A) (xs : list A) (γ : gname) :
    ●≺[γ] xs -∗
    x ◯≺[γ] y -∗
      ⌜x ∈ xs⌝ ∗ ⌜y ∈ xs⌝.
  Proof.
    iIntros "H● Hxy".
    iDestruct (order_own_ordered_indices with "H● Hxy") as
      %(i & j & Hle & Hi & Hj).
    iPureIntro. rewrite !elem_of_list_lookup. eauto.
  Qed.
End properties.
