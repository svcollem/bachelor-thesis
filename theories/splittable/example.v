From iris.heap_lang Require Export notation lang lib.par adequacy.
From iris.proofmode Require Export proofmode.
From iris.heap_lang Require Import proofmode.
From iris.prelude Require Import options.

From barriers.splittable Require Import verification.

Definition example : expr :=
  let: "x" := ref #37 in
  let: "y" := ref #42 in
  let: "b" := new_barrier #() in
  let: "r" :=
    ("x" <- #6;; "y" <- #8;; signal "b")
      ||| (wait "b";; #2 * !"x")
      ||| (wait "b";; #2 * !"y")
    in (Snd (Fst "r"), Snd "r").

Section example.
  Context `{!heapGS Σ, !barrierG Σ, !spawnG Σ}.

  Lemma example_spec :
    {{{ True }}} example {{{ RET (#12, #16); True }}}.
  Proof using Type*.
    iIntros (Φ) "_ HΦ". unfold example.
    wp_alloc l as "Hl". wp_let.
    wp_alloc l' as "Hl'". wp_let.
    wp_apply (new_barrier_spec (l ↦ #6 ∗ l' ↦ #8) with "[//]").
    iIntros (b) "(Hsend & Hrecv)".
    iMod (recv_split with "Hrecv") as "[Hrecv Hrecv']"; first set_solver.
    wp_let.
    wp_smart_apply (wp_par (λ v, ⌜v = (#(), #12)%V⌝)%I (λ v, ⌜v = #16⌝)%I
      with "[Hl Hl' Hsend Hrecv] [Hrecv']");
    first wp_smart_apply (wp_par (λ v, ⌜v = #()⌝)%I (λ v, ⌜v = #12⌝)%I
      with "[Hl Hl' Hsend] [Hrecv]").
    { (* Thread 1 *)
      wp_store. wp_store.
      by iApply (signal_spec with "[$Hsend $Hl $Hl']"). }
    { (* Thread 2 *)
      wp_apply (wait_spec with "Hrecv").
      iIntros "Hl". wp_seq. wp_load.
      wp_op. iPureIntro.
      by replace (2 * 6)%Z with 12%Z by lia. }
    { by iIntros (v1 v2) "[-> ->]". }
    { (* Thread 3 *)
      wp_apply (wait_spec with "Hrecv'").
      iIntros "Hl'". wp_seq. wp_load.
      wp_op. iPureIntro.
      by replace (2 * 8)%Z with 16%Z by lia. }
    iIntros (v1 v2) "[-> ->] !>".
    wp_let. repeat wp_proj. wp_pair.
    by iApply "HΦ".
  Qed.
End example.

Section ClosedProofs.

Let Σ : gFunctors := #[ heapΣ ; barrierΣ ; spawnΣ ].

Lemma example_adequate σ : adequate NotStuck example σ (λ v _, v = (#12, #16)%V).
Proof. apply (heap_adequacy Σ)=>?. iIntros "_". by iApply example_spec. Qed.

End ClosedProofs.

Print Assumptions example_adequate.
