From iris.heap_lang Require Export notation lang.
From iris.proofmode Require Export proofmode.
From iris.heap_lang Require Import proofmode.
From iris.base_logic Require Import invariants lib.saved_prop.
From iris.algebra Require Import auth gset.
From iris.prelude Require Import options.

From barriers.splittable Require Import specification.

Section code.
  Definition new_barrier : val :=
  λ: <>, ref #false.

  Definition signal : val :=
    λ: "b", "b" <- #true.

  Definition wait : val :=
    rec: "wait" "b" := if: !"b" then #() else "wait" "b".
End code.

Class barrierG Σ := BarrierG {
  barrier_inG :> inG Σ (authR (gset_disjUR gname));
  barrier_savedPropG :> savedPropG Σ;
}.

Definition barrierΣ : gFunctors :=
  #[ GFunctor (authRF (gset_disjUR gname)); savedPropΣ ].

Global Instance subG_barrierΣ {Σ} : subG barrierΣ Σ → barrierG Σ.
Proof. solve_inG. Qed.

Section proof.

  Context `{!heapGS Σ, !barrierG Σ}.

  Definition barrierN := nroot .@ "splittable_barrier".

  Definition barrier_inv (l : loc) (γ : gname) (P : iProp Σ) : iProp Σ :=
    ∃ (b : bool) (γsps : gset gname),
      l ↦{#1 / 2} #b ∗ own γ (● GSet γsps)
        ∗ ((if b then True else P)
              -∗ [∗ set] γsp ∈ γsps, ∃ R, saved_prop_own γsp DfracDiscarded R ∗ ▷ R).
  
  Definition send (b : val) (P : iProp Σ) : iProp Σ :=
    ∃ (l : loc) (γ : gname),
      ⌜b = #l⌝ ∗ l ↦{#1 / 2} #false ∗ inv barrierN (barrier_inv l γ P).
  Definition recv (b : val) (R : iProp Σ) : iProp Σ :=
    ∃ (l : loc) (γ γsp : gname) (R' P : iProp Σ),
      ⌜b = #l⌝
        ∗ own γ (◯ GSet {[γsp]})
        ∗ saved_prop_own γsp DfracDiscarded R'
        ∗ (R' -∗ R)
        ∗ inv barrierN (barrier_inv l γ P).

  Lemma new_barrier_spec P :
    {{{ True }}} new_barrier #() {{{ b, RET b; send b P ∗ recv b P }}}.
  Proof.
    iIntros (Φ) "_ HΦ".
    wp_lam. wp_alloc l as "[Hl Hl']".
    (* We allocate the set with all identifiers which
       save a yet to be received resource *)
    iMod (saved_prop_alloc P DfracDiscarded) as (γsp) "#Hsp"; first done.
    iMod (own_alloc (● GSet {[γsp]} ⋅ ◯ GSet {[γsp]})) as (γ) "[H● H◯]".
    { apply auth_both_valid_discrete. done. }
    iMod (inv_alloc barrierN _ (barrier_inv l γ P) with "[Hl H●]") as "#Hinv".
    { iExists false, {[γsp]}. iFrame.
      iIntros "!> HP". rewrite big_sepS_singleton.
      iExists P. by iFrame. }
    iApply "HΦ". iSplitL "Hl'"; iExists l, γ.
    - auto with iFrame.
    - iExists γsp, P, P. iFrame "Hsp Hinv H◯". auto.
  Qed.

  Lemma signal_spec b P :
    {{{ send b P ∗ P }}} signal b {{{ RET #(); True }}}.
  Proof.
    iIntros (Φ) "[(%l & %γ & -> & Hl & #Hinv) HP] HΦ".
    wp_lam.
    iInv barrierN as (b γsps) "(>Hl' & >H● & HRs)" "Hclose".
    iDestruct (mapsto_agree with "Hl' Hl") as %[=->].
    iCombine "Hl Hl'" as "Hl".
    wp_store. iApply "HΦ". iApply "Hclose".
    (* We signal so we have to give up P *)
    iExists true, γsps.
    iDestruct "Hl" as "[$ _]".
    iIntros "{$H●} _".
    by iApply "HRs".
  Qed.

  Lemma wait_spec b R :
    {{{ recv b R }}} wait b {{{ RET #(); R }}}.
  Proof.
    iIntros (Φ) "Hr HΦ".
    iDestruct "Hr" as (l γ γsp R' P ->) "(H◯ & #Hsp & HR & #Hinv)".
    iLöb as "IH". wp_lam. wp_bind (! _)%E.
    iInv barrierN as (b γsps) "(>Hl & >H● & HRs)" "Hclose".
    wp_load. destruct b.
    - (* l ↦ #true so we're done waiting.
         We remove γsp from γsps to get access to our resource *)
      iSpecialize ("HRs" with "[//]").
      iDestruct (own_valid_2 with "H● H◯")
        as %[Hin%gset_disj_included%singleton_subseteq_l _]%auth_both_valid_discrete.
      rewrite (big_sepS_delete _ _ _ Hin).
      iDestruct "HRs" as "[(%R'' & #Hsp' & HR'') HRs]".
      iDestruct (saved_prop_agree with "Hsp Hsp'") as "#Heq".
      iMod (own_update_2 _ _ _ (● GSet (γsps ∖ {[γsp]})) with "H● H◯") as "H●".
      { apply auth_update_dealloc. apply gset_disj_dealloc_local_update. }
      iMod ("Hclose" with "[Hl H● HRs]") as "_".
      { iExists true, (γsps ∖ {[γsp]}). by iFrame. }
      iModIntro. wp_if_true. iApply "HΦ".
      iApply "HR". by iRewrite "Heq".
    - (* l ↦ #false, so we have to keep waiting *)
      iMod ("Hclose" with "[Hl H● HRs]") as "_".
      { iExists false, γsps. iFrame. }
      iModIntro. wp_if_false.
      iApply ("IH" with "H◯ HR HΦ").
  Qed.

  Lemma recv_split (b : val) (R1 R2 : iProp Σ) (E : coPset) :
    ↑barrierN ⊆ E → recv b (R1 ∗ R2) ={E}=∗ recv b R1 ∗ recv b R2.
  Proof.
    iIntros (?).
    iDestruct 1 as (l γ γsp R' P ->) "(H◯ & #Hsp & HR' & #Hinv)".
    iInv barrierN as (b γsps) "(>Hl & >H● & HRs)" "Hclose".
    (* We'll remove R' from the saved propositions and add P1, P2 to it *)
    iDestruct (own_valid_2 with "H● H◯")
        as %[Hin%gset_disj_included%singleton_subseteq_l _]%auth_both_valid_discrete.
    iMod (own_update_2 _ _ _ (● GSet (γsps ∖ {[γsp]})) with "H● H◯") as "H●".
    { apply auth_update_dealloc. apply gset_disj_dealloc_local_update. }
    iMod (saved_prop_alloc_cofinite γsps R1 DfracDiscarded)
      as (γsp1 Hγsp1) "#Hsp1"; first done.
    iMod (saved_prop_alloc_cofinite ({[γsp1]} ∪ γsps) R2 DfracDiscarded)
      as (γsp2 [Hneq%not_elem_of_singleton_1 Hγsp2]%not_elem_of_union) "#Hsp2";
      first done.
    iMod (own_update _ _ (● _ ⋅ (◯ GSet {[γsp1]} ⋅ ◯ (GSet {[γsp2]})))
      with "H●") as "(H● & H◯1 & H◯2)".
    { rewrite -auth_frag_op gset_disj_union; last set_solver.
      apply auth_update_alloc.
      apply gset_disj_alloc_empty_local_update. set_solver. }
    iMod ("Hclose" with "[Hl H● HRs HR']") as "_".
    { iExists b, ({[γsp1; γsp2]} ∪ γsps ∖ {[γsp]}).
      iFrame "Hl H●". iIntros "!> HP".
      iSpecialize ("HRs" with "HP").
      rewrite (big_sepS_delete _ _ γsp); last apply Hin.
      iDestruct "HRs" as "[(%R & #Hsp' & HR) HRs]".
      iDestruct (saved_prop_agree with "Hsp Hsp'") as "#Heq".
      iAssert (▷ (R1 ∗ R2))%I with "[HR HR']" as "[HR1 HR2]".
      { iApply "HR'". iNext. by iRewrite "Heq". }
      rewrite big_sepS_union; last set_solver.
      rewrite big_sepS_union; last set_solver.
      iFrame "HRs". rewrite !big_sepS_singleton. iSplitL "HR1".
      - iExists R1. by iFrame.
      - iExists R2. by iFrame. }
    iModIntro. iSplitL "H◯1".
    - iExists l, γ, γsp1, R1, P. iFrame "# H◯1". auto.
    - iExists l, γ, γsp2, R2, P. iFrame "# H◯2". auto.
  Qed.

  Lemma recv_weaken b R1 R2 :
    (R1 -∗ R2) -∗ recv b R1 -∗ recv b R2.
  Proof.
    iIntros "HR2".
    iDestruct 1 as (l γ γsp R' P ->) "(H◯ & #Hsp & HR1 & #Hinv)".
    iExists l, γ, γsp, R', P. iFrame "H◯ Hsp Hinv".
    iSplit; first auto. iIntros "HR'".
    iApply "HR2". by iApply "HR1".
  Qed.

  Global Opaque new_barrier signal wait.
End proof.

Program Definition splittable_barrier `{!heapGS Σ, !barrierG Σ} : splittable_barrier_spec Σ barrierN :=
  {| specification.new_barrier := new_barrier;
     specification.signal := signal;
     specification.wait := wait;
     specification.send := send;
     specification.recv := recv;
     specification.recv_split := recv_split;
     specification.recv_weaken := recv_weaken;
     specification.new_barrier_spec := new_barrier_spec;
     specification.signal_spec := signal_spec;
     specification.wait_spec := wait_spec;
  |}.

Global Typeclasses Opaque barrier_inv.
Global Opaque barrier_inv.
Global Typeclasses Opaque send.
Global Opaque send.
Global Typeclasses Opaque recv.
Global Opaque recv.
