From iris.heap_lang Require Export notation lang.
From iris.proofmode Require Export proofmode.
From iris.heap_lang Require Import proofmode.
From iris.prelude Require Import options.

Section spec.

  Structure splittable_barrier_spec Σ `{!heapGS Σ} (N : namespace) := splittable_barrier {
    (* Operations *)
    new_barrier : val;
    signal : val;
    wait : val;
    (* Predicates *)
    send (b : val) (P : iProp Σ) : iProp Σ;
    recv (b : val) (P : iProp Σ) : iProp Σ;
    (* General properties of the predicates *)
    recv_split (b : val) (P1 P2 : iProp Σ) (E : coPset) :
      ↑N ⊆ E → recv b (P1 ∗ P2) ={E}=∗ recv b P1 ∗ recv b P2;
    recv_weaken (b : val) (P Q : iProp Σ) :
      (P -∗ Q) -∗ recv b P -∗ recv b Q;
    (* Program specs *)
    new_barrier_spec (P : iProp Σ) :
      {{{ True }}}
        new_barrier #()
      {{{ b, RET b; send b P ∗ recv b P }}};
    signal_spec (b : val) (P : iProp Σ) :
      {{{ send b P ∗ P }}}
        signal b
      {{{ RET #(); True }}};
    wait_spec (b : val) (P : iProp Σ) :
      {{{ recv b P }}}
        wait b
      {{{ RET #(); P }}};
  }.

End spec.
