From iris.heap_lang Require Export notation lang.
From iris.proofmode Require Export proofmode.
From iris.heap_lang Require Import proofmode.
From iris.base_logic Require Import invariants.
From iris.algebra Require Import excl.
From iris.prelude Require Import options.

From barriers.simple Require Import specification.

Section code.
  Definition new_barrier : val :=
    λ: <>, ref #false.

  Definition signal : val :=
    λ: "b", "b" <- #true.

  Definition wait : val :=
    rec: "wait" "b" := if: !"b" then #() else "wait" "b".
End code.

Class barrierG Σ := barrier_G :> inG Σ (exclR unitO).

Definition barrierΣ : gFunctors :=
  #[ GFunctor (exclR unitO) ].

Global Instance subG_barrierΣ {Σ} : subG barrierΣ Σ → barrierG Σ.
Proof. solve_inG. Qed.

Section proof.

  Context `{!heapGS Σ, !barrierG Σ}.

  Let N := nroot .@ "simple_barrier".
  Definition barrier_inv (l : loc) (γ : gname) (P : iProp Σ) : iProp Σ :=
    ∃ (b : bool), l ↦ #b ∗ if b then own γ (Excl ()) ∨ P else True.
  
  Definition send (b : val) (P : iProp Σ) : iProp Σ :=
    ∃ (l : loc) (γ : gname), ⌜b = #l⌝ ∗ inv N (barrier_inv l γ P).
  Definition recv (b : val) (P : iProp Σ) : iProp Σ :=
    ∃ (l : loc) (γ : gname) (R : iProp Σ),
      ⌜b = #l⌝ ∗ own γ (Excl ()) ∗ (R -∗ P) ∗ inv N (barrier_inv l γ R).

  Lemma new_barrier_spec P :
    {{{ True }}} new_barrier #() {{{ b, RET b; send b P ∗ recv b P }}}.
  Proof.
    iIntros (Φ) "_ HΦ".
    wp_lam. wp_alloc l as "Hl".
    iMod (own_alloc (Excl ())) as (γ) "Hγ"; first done.
    iMod (inv_alloc N _ (barrier_inv l γ P) with "[Hl]") as "#Hinv".
    { iExists false. by iFrame. }
    iApply "HΦ". iSplitR; iExists l, γ.
    - auto with iFrame.
    - iExists P. iSplitR; first auto. auto with iFrame.
  Qed.

  Lemma signal_spec b P :
    {{{ send b P ∗ P }}} signal b {{{ RET #(); True }}}.
  Proof.
    iIntros (Φ) "[(%l & %γ & -> & #Hinv) HP] HΦ".
    wp_lam.
    iInv N as (b) "(>Hl & H)" "Hclose".
    wp_store. iApply "HΦ". iApply "Hclose".
    (* We signal so we have to give up P *)
    iExists true. iFrame "Hl". by iRight.
  Qed.

  Lemma wait_spec b R :
    {{{ recv b R }}} wait b {{{ RET #(); R }}}.
  Proof.
    iIntros (Φ) "Hr HΦ".
    iDestruct "Hr" as (l γ P ->) "(Hγ & HR & #Hinv)".
    iLöb as "IH". wp_lam. wp_bind (! _)%E.
    iInv N as (b) "[>Hl H]" "Hclose".
    wp_load. destruct b.
    - (* l ↦ #true so we're done waiting *)
      iDestruct "H" as "[Hγ' | HP]".
      { (* The invariant says that we already acknowledged
           that we got signalled, but we still own Hγ so
           this is clearly a contradiction. *)
        iExFalso. iDestruct (own_valid_2 with "Hγ' Hγ") as %[]. }
      iMod ("Hclose" with "[Hl Hγ]") as "_".
      { (* We acknowledge that we got signalled by giving up Hγ *)
        iExists true. iFrame "Hl". by iLeft. }
      iModIntro. wp_if_true. iApply "HΦ". by iApply "HR".
    - (* l ↦ #false, so we have to keep waiting *)
      iMod ("Hclose" with "[Hl]") as "_".
      { iExists false. iFrame. }
      iModIntro. wp_if_false.
      iApply ("IH" with "Hγ HR HΦ").
  Qed.

  Lemma recv_weaken b R1 R2 :
    (R1 -∗ R2) -∗ recv b R1 -∗ recv b R2.
  Proof.
    iIntros "HR2".
    iDestruct 1 as (l γ P ->) "(Hγ & HR1 & #Hinv)".
    iExists l, γ, P. iSplit; first auto.
    iFrame "Hγ Hinv". iIntros "HP".
    iApply "HR2". by iApply "HR1".
  Qed.

  Global Opaque new_barrier signal wait.
End proof.

Program Definition simple_barrier `{!heapGS Σ, !barrierG Σ} : simple_barrier_spec Σ :=
  {| specification.new_barrier := new_barrier;
     specification.signal := signal;
     specification.wait := wait;
     specification.send := send;
     specification.recv := recv;
     specification.recv_weaken := recv_weaken;
     specification.new_barrier_spec := new_barrier_spec;
     specification.signal_spec := signal_spec;
     specification.wait_spec := wait_spec;
  |}.

Global Typeclasses Opaque barrier_inv.
Global Opaque barrier_inv.
Global Typeclasses Opaque send.
Global Opaque send.
Global Typeclasses Opaque recv.
Global Opaque recv.
