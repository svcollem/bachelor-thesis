From iris.heap_lang Require Export notation lang lib.par adequacy.
From iris.proofmode Require Export proofmode.
From iris.heap_lang Require Import proofmode.
From iris.prelude Require Import options.

From barriers.simple Require Import verification.

Section example.
  Context `{!heapGS Σ, !barrierG Σ, !spawnG Σ}.

  Parameter expensive_bool : val.
  Parameter expensive_int  : val.

  Axiom expensive_bool_spec :
    {{{ True }}} expensive_bool #() {{{ RET #true; True }}}.
  Axiom expensive_int_spec :
    {{{ True }}} expensive_int #() {{{ RET #37; True }}}.

  Definition example : val :=
    λ: "r",
      let: "x" := ref #false in
      let: "b" := new_barrier #() in
        ("x" <- expensive_bool #();; signal "b")
        |||
        (let: "n" := expensive_int #() in
        wait "b";;
        if: ! "x"
          then "r" <- #2 * "n"
          else "r" <- "n").

  Lemma example_spec (r : loc) (v : val) :
    {{{ r ↦ v }}} example #r {{{ w, RET w; r ↦ #74 }}}.
  Proof using Type*.
    iIntros (Φ) "Hr HΦ". wp_lam.
    wp_alloc x as "Hx". wp_let.
    wp_apply (new_barrier_spec (x ↦ #true) with "[//]").
    iIntros (b) "(Hsend & Hrecv)". wp_let.
    wp_smart_apply (wp_par (λ _, True)%I (λ _, r ↦ #74)%I
      with "[Hx Hsend] [Hr Hrecv]").
    { (* Thread 1 *)
      wp_apply (expensive_bool_spec with "[//]").
      iIntros "_". wp_store.
      by iApply (signal_spec with "[$Hsend $Hx]"). }
    { (* Thread 2 *)
      wp_apply (expensive_int_spec with "[//]").
      iIntros "_". wp_let.
      wp_apply (wait_spec with "Hrecv").
      iIntros "Hx". wp_seq. wp_load.
      wp_if_true. wp_op. wp_store.
      iApply "Hr". }
    iIntros (v1 v2) "[_ Hr]".
    by iApply "HΦ".
  Qed.
End example.

Section par.
  Context `{!heapGS Σ, !barrierG Σ, !spawnG Σ}.

  Definition par : val :=
    λ: "e1" "e2",
      let: "b" := new_barrier #() in
      Fork ("e2" #();; signal "b");;
      "e1" #();;
      wait "b".

  Lemma par_spec (e1 e2 : val) (P1 P2 Q1 Q2 : iProp Σ) :
    {{{ P1 }}} e1 #() {{{ v, RET v; Q1 }}} →
    {{{ P2 }}} e2 #() {{{ v, RET v; Q2 }}} →
    {{{ P1 ∗ P2 }}} par e1 e2 {{{ v, RET v; Q1 ∗ Q2 }}}.
  Proof using Type*.
    iIntros (He1 He2 Φ) "[HP1 HP2] HΦ".
    wp_lam. wp_let.
    wp_apply (new_barrier_spec Q2 with "[//]").
    iIntros (b) "[Hsend Hrecv]". wp_let.
    wp_apply (wp_fork with "[HP2 Hsend]").
    { iNext. wp_apply (He2 with "HP2").
      iIntros (?) "HQ2". wp_seq.
      by wp_apply (signal_spec with "[$Hsend $HQ2]"). }
    wp_seq. wp_apply (He1 with "HP1").
    iIntros (?) "HQ1". wp_seq.
    wp_apply (wait_spec with "Hrecv").
    iIntros "HQ2".
    iApply "HΦ". iFrame.
  Qed.

End par.
