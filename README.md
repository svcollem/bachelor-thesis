## Installation

You first need to make sure [`opam` is installed on your system](https://opam.ocaml.org/doc/Install.html).
After that you can install the dependencies as follows:
```
opam switch create iris-barriers ocaml-base-compiler.4.14.0
eval $(opam env)
opam switch link iris-barriers .
opam repo add coq-released https://coq.inria.fr/opam/released
opam repo add iris-dev https://gitlab.mpi-sws.org/iris/opam.git
make builddep
```
To build the project files, run `make -jN` with `N` the number of used CPU cores.


## Directory structure

* The [theories/simple](theories/simple) directory contains the simple barrier from Chapter 3:
  + The specification from Section 3.2 is given in [specification.v](theories/simple/specification.v).
  + The implementation from Section 3.1 is given in [verification.v](theories/simple/verification.v).
  + The verification from Section 3.3 is given in [verification.v](theories/simple/verification.v).
  + The verification of Figure 3.1 is given in [example.v](theories/simple/example.v).
* The [theories/splittable](theories/splittable) directory contains verification of Section 5 of Dodds et al.
  It is almost identical to the verification in the [Iris examples repository](https://gitlab.mpi-sws.org/iris/examples/-/blob/26b7ab260b79ffeff8a16a866aeb00ee37a8fb67/theories/barrier/proof.v).
  The only difference is in the definition of `send`, which allows to remove a trivial case from the proof on [line 71](https://gitlab.mpi-sws.org/iris/examples/-/blob/26b7ab260b79ffeff8a16a866aeb00ee37a8fb67/theories/barrier/proof.v#L71).
  This verification has not become part of the thesis.
* The [theories/chainable](theories/chainable) directory contains the simple barrier from Chapters 4, 5:
  + The specification from Section 4.3 is given in [specification.v](theories/chainable/specification.v).
  + The implementation from Section 4.2 is given in [verification.v](theories/chainable/verification.v).
  + The _authoritative ordering_ construction from Section 5.1 is given in [order_auth.v](theories/chainable/order_auth.v).
  + The verification from Sections 4.4, 5.2 is given in [verification.v](theories/chainable/verification.v).
  + The verification of Figure 4.3 is given in [example.v](theories/chainable/example.v).