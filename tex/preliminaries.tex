\chapter{Preliminaries}\label{preliminaries}

In this chapter we give a brief introduction to separation logic and HeapLang.
We start by presenting Iris's HeapLang~(\S\ref{sec:prelims-heaplang}), a programming language used in the rest of this thesis.
We then proceed by giving a short introduction to separation logic~(\S\ref{sec:prelims-sep-logic}),
and conclude by showing how we can prove program specifications~(\S\ref{sec:prelims-proving-specs}).
We do not discuss Iris specific features in this chapter,
so readers familiar with separation logic may safely skim this chapter.


\section{HeapLang}\label{sec:prelims-heaplang}
We start by introducing HeapLang. HeapLang is an untyped ML style language
which supports higher-order heap (e.g. storing functions on the heap), concurrency and common atomic operations.
It is a language which is primarily used to provide examples and show what Iris is capable of.
There do, until now, not exist ways to run HeapLang outside of Coq.

We partially present the syntax of HeapLang.
In some places we cut some corners to simplify the story.
One can refer to~\parencite[\S 12]{iris:technical_reference} for a more thorough formalization of HeapLang.
\begin{align*}
    \val \bnfdef{}&
      \TT \mid
      z \mid
      \loc \mid
      \True \mid
      \False \mid
      \Rec\lvarF(\lvar)= \expr \mid
      \dots \quad\quad (z \in \integer, \loc \in \Loc) {}\\ &
      \Inl(\val) \mid
      \Inr(\val) \mid
      \dots {}\\
    \expr \bnfdef{}&
      \val \mid
      \lvar \mid
      \expr_1 \expr_2 \mid
      \If \expr then \expr_1 \Else \expr_2 \mid
      {}\\ &
      -e \mid
      \expr_1 + \expr_2 \mid
      \dots
      {}\\ &
      \Inl(\expr) \mid
      \Inr(\expr) \mid
      (\Match \expr with \Inl(x) => \expr_1 | \Inr(y) => \expr_2 end) \mid
      {}\\ &
      \Alloc(\expr) \mid
      \Free(\expr) \mid
      \deref \expr \mid
      \expr_1 \gets \expr_2 \mid
      \CAS(\expr_1, \expr_2, \expr_3) \mid
      \FAA(\expr_1, \expr_2) \mid
      {}\\ &
      \expr_1 \mathbin{||} \expr_2 \mid
      \dots
\end{align*}
HeapLang is an untyped lambda calculus.
It furthermore has recursive functions which we use to define non-recursive functions, let expressions and the sequence of two expressions:
\begin{align*}
    \Lam x. \expr \eqdef{}& \Rec \any (x)= \expr \\
    \Let x = \expr_1 in \expr_2 \eqdef{}& (\Lam x. \expr_2) \expr_1 \\
    \expr_1; \expr_2 \eqdef{}& \Let \any = \expr_1 in \expr_2
\end{align*}
We also frequently use optionals, which we can encode in the following way:%
\footnote{The definition of $\None$ looks a bit weird but we just pass $\TT$, the unit value, to the $\Inl$ constructor.}
\begin{align*}
    \None \eqdef{}& \Inl(\TT) \\
    \Some(e) \eqdef{}& \Inr(e)
\end{align*}
We furthermore use indentation for if-statements to indicate their scopes.
For example, the code on the left should be parsed as the code on the right.
\begin{mathpar}
    \begin{array}{@{}l@{}}
        \If e_1 \\
        \quad then
        \begin{array}[t]{@{}l@{}}
            e_2; \\
            e_3
        \end{array} \\
        \quad \Else
        \begin{array}[t]{@{}l@{}}
            e_4; \\
            e_5
        \end{array} \\
        e_6
    \end{array}
    \and
    (\If e_1 then (e_2; e_3) \Else (e_4; e_5)); e_6
\end{mathpar}

HeapLang is also able to interact with the heap.
We can use $\Alloc(e)$ to store a value on the heap. It returns a location $\loc$ which can be thought of as a pointer.
Note that there is no ``null pointer'', but we can represent it as $\None$, and all other pointers as $\Some(\loc)$.
We use $\Free(e)$ to free a location, $\deref e$ to read a value stored at a location, and $e_1 \gets e_2$ to update the value stored at $e_1$ to $e_2$.
These four operations are atomic, meaning that they do this in a single program step.%
\footnote{Do note that $\loc \gets f(n)$ first reduces $f(n)$ to a value, before storing it in $\loc$.
    That means that $\loc \gets f(n)$ is not atomic, while $\loc \gets 1$ is.}
Apart from these heap operations, HeapLang also has two other atomic operations.
$\CAS(e_1, e_2, e_3)$ (compare and swap) compares the value stored at $e_1$ with $e_2$.
When they are equal, $e_1$ gets updated to $e_3$ and $\True$ is returned, otherwise it returns $\False$.
$\FAA(e_1, e_2)$ (fetch and add) loads the value stored in $e_1$, adds $e_2$ to it and returns the original value.
Finally, we can use $e_1 \mathbin{||} e_2$ to execute $e_1$ and $e_2$ in parallel and join them together when they are both finished.%
\footnote{
    The par operation actually is not a primitive of HeapLang.
    Rather, it has $\Fork{e}$, which forks a new thread to execute $e$.
    We can implement par using $\langkw{fork}$.
    However, for our examples, it is easier to think of par as a primitive of the language.}
It is called the \emph{par} operation.

As HeapLang has support for multithreading, we need to talk about the semantics of how the threads interleave.
Thread scheduling is completely nondeterministic meaning a programmer can make no assumptions about the order threads execute in.

We now present a few example programs, which we later give and prove specifications for.
\newcommand{\parExecName}{\textlang{par\_exec}}
\newcommand{\parExec}[1]{\parExecName(#1)}
\begin{example}[Parallel execution]
\label{example:par-exec-code}
    We define a function $\parExecName$, which takes a location $\loc$.
    It then allocates a new location $\loc'$ where we store $37$.
    It doubles the value stored in $\loc$ and adds 3 to the value stored in $\loc'$.
    These operations are done in parallel.
    Finally it returns the newly allocated location $\loc'$.
    \begin{gather*}
        \parExecName \eqdef{} \Lam \loc.
        \begin{array}[t]{@{}l@{}}
            \Let \loc' = \Alloc(37) in \\
            \left(
            \begin{array}{l||l}
                \begin{array}{@{}l@{}}
                    \Let n = \deref \loc in \\
                    \loc \gets 2 \cdot n
                \end{array} &
                \begin{array}{@{}l@{}}
                    \Let m = \deref \loc' in \\
                    \loc' \gets m + 3
                \end{array}
            \end{array}
            \right) \\
            \loc'
        \end{array}
    \end{gather*}
\end{example}

\newcommand{\appendName}{\textlang{append}}
\newcommand{\append}[2]{\appendName(#1, #2)}
\newcommand{\cons}[2]{#1 \mathbin{::} #2}
\begin{example}[List append]
\label{example:list-append-code}
    Before we can write a function to append two lists, we first need a way to encode lists in HeapLang.
    There are many possible ways to do this, but we use a representation which is sort of analogous to linked lists in C-style languages.
    An empty list is represented by $\None$,
    and a list of values $\cons{a}{\vec{x}}$ is represented by $\Some(\loc)$,
    such that the value stored at $\loc$ is a pair containing $x$ and the representation of $\vec{x}$.
    Using this representation, we represent the list $[1,2]$ by $\Some(\loc_1)$,
    where the value stored at $\loc_1$ is $(1, \Some(\loc_2))$
    and the value stored at $\loc_2$ is $(2, \None)$.
    Now that we have a way to represent lists in HeapLang, we can define $\appendName$ as follows:
    \begin{gather*}
        \appendName \eqdef{} \Rec \mathit{append} (v_1, v_2)=
            \MatchML v_1 with
              \None => v_2
            | \Some(\mathit{hd}) =>
            \begin{array}[t]{@{}l@{}}
                \Let x = \Fst (\deref \mathit{hd}) in \\
                \Let \mathit{xs} = \Snd (\deref \mathit{hd}) in \\
                \Let v_1' = \mathit{append}(\mathit{xs}, v_2) in \\
                \mathit{hd} \gets (x, v_1'); \\
                \Some(\mathit{hd})
            \end{array}
            end{}
    \end{gather*}
    If $v_1$ represents the empty list, then we can just return $v_2$.
    In the other case, we first read the values stored in the $\mathit{hd}$ location.
    We then do a recursive call to append $v_2$ to the tail of our current list.
    Finally, we store the original value $x$ together with the new tail, $v_1'$, in $\mathit{hd}$,
    and return $\Some(\mathit{hd})$, the representation of our appended list.
\end{example}

Finally, it is good to stress that this language is not safe at all.
We can write all kind of programs like $\True \gets \False$, $\TT \TT$, which have undefined behavior.
It is furthermore possible to write programs with use-after-free bugs:
\begin{align*}
    &\Let \loc = \Alloc(\True) in \\
    &\Free(\loc); \\
    &\loc \gets \False
\end{align*}
This is of course undesirable. We thus want a version of Hoare logic which not only allows us to prove specifications for programs,
but furthermore also allows us to argue that a program is safe.


\section{Hoare and separation logic}\label{sec:prelims-sep-logic}
We can use separation logic to reason about HeapLang programs.
Separation logic is an extension of Hoare logic.
We write Hoare triples $\hoare{P}{e}{\Phi}$ as specifications for programs.
Intuitively, they represent the fact that
(i) $e$ is a safe program,
and (ii) if precondition $P$ holds and program $e$ terminates with value $v$, then postcondition $\Phi(v)$ holds.
We often write $\hoare{P}{e}{\Ret v. Q}$ as a shorthand for $\hoare{P}{e}{\Lam v. Q}$,
and $\hoare{P}{e}{Q}$ as a shorthand for $\hoare{P}{e}{\Lam \any. Q}$ whenever we are not interested in the return value.
Furthermore, it is good to stress that these Hoare triples only specify partial correctness of a program.
Take for example the following recursive function which loops forever:
\newcommand{\divergeName}{\textlang{diverge}}
\newcommand{\diverge}[1]{\divergeName\spac#1}
\begin{gather*}
    \divergeName \eqdef \Rec \mathit{diverge} (\any)= \mathit{diverge}\spac \TT
\end{gather*}
We are able to prove the following specification for it:%
\footnote{The proof uses L\"ob induction which we do not present until~\S\ref{sec:simple-barrier-verification}.}
\begin{gather*}
    \hoare{\TRUE}{\diverge{\TT}}{\FALSE}
\end{gather*}
This makes sense as $\diverge{\TT}$ does not terminate, allowing us to state any postcondition we would like.

We now present the Iris propositions, which can be used for pre and postconditions.%
\footnote{We again cut a corner here by taking Hoare triples as a primitive of Iris,
    while they are actually defined inside the logic itself~\parencite[\S 6]{iris_from_the_ground_up}.}
\begin{align*}
    P \bnfdef{}&
      \varphi \mid
      P \land Q \mid
      P \lor Q \mid
      P \rightarrow Q \mid
      \All x. P \mid
      \Exists x. P \mid
      \quad&&\text{Higher order logic}
      {}\\ &
      \loc \mapsto v \mid
      P \ast Q \mid
      P \wand Q \mid
      \hoare{P}{e}{\Phi} \mid
      &&\text{Separation logic}
      {}\\ &
      \later P \mid
      \always P \mid
      \pvs P \mid
      \knowInv{\namesp}{P} \mid
      \ownGhost{\gamma}{a} \mid
      \dots
      && \text{Iris}
\end{align*}
The first line corresponds with standard higher order logic.
We let $\varphi$ range over all propositions of the meta logic, Coq in this case.
This allows us to, for example, reason about lists in our logic.
The second line has the separation logic primitives.
The intuitive idea behind separation logic propositions is that they describe some resource.
Proposition $P$ holding corresponds to owning said resource.
Because of this correspondence, we often refer to propositions as resources.
Programs can only use and update resources they own.
We shortly see an example of this when we discuss $\loc \mapsto v$.
In the original separation logic papers~\parencite{OHearn_et_al_2001,Reynolds_2002},
resources were heap fragments, describing a finite part of the heap.
In Iris it is also possible to own other kinds of resources,%
\footnote{One can refer to~\parencite[\S 3]{iris_from_the_ground_up} for a more thorough discussion on resource ownership in Iris.}
some of which are shown on the third line.
For simplicity sake, we only consider heap fragments as our type of resources for the rest of this chapter.

The \emph{separating conjunction} $\ast$ has the following meaning:
$P \ast Q$ holds whenever $P$ and $Q$ both hold and describe disjoint resources.
In our case this corresponds to $P$ and $Q$ describing disjoint parts of the heap.
We typically say that a program owns $P$ and $Q$ when it owns $P \ast Q$.

The proposition $\loc \mapsto v$ should be read as $\loc$ \emph{points to} $v$
and is often referred to as \emph{the points-to predicate of} $\loc$.
It states that value $v$ is stored at location $\loc$ in the heap.
Only when a program owns $\loc \mapsto v$, it is allowed to read or update the value stored at $\loc$.
This is what makes sure that we cannot have use-after-free-bugs,
as a program loses the points-to predicate of $\loc$ after freeing $\loc$.
In a similar way, this also ensures that a program cannot write to unallocated memory.
Also note that if a program owns $\loc \mapsto v \ast \loc' \mapsto w$,
then we know that $\loc \neq \loc'$ as both resources cannot describe the same part of the heap.

The \emph{magic wand} $\wand$ can be thought of as an implication for separation logic:
whenever a program owns $P \wand Q$ and combines it, using $\ast$, with a disjoint resource $P$, the program owns $Q$.

Hoare triples $\hoare{P}{e}{\Phi}$ are, as already discussed, used to state specifications for programs.
Furthermore, $P$ also describes a \emph{footprint} of $e$.
As $e$ can only use and update resources it owns itself,
we can be sure that it does not use any resources disjoint with $P$.

Finally the last line consists of propositions which are not present in the original separation logics~\parencite{OHearn_et_al_2001,Reynolds_2002}.
We discuss them on demand whenever we come across them.
The $\later$ modality and invariants $\knowInv{\namesp}{P}$ are discussed in~\S\ref{sec:simple-barrier-verification},
and the $\always$ and $\pvs$ modalities are discussed in~\S\ref{sec:chainable-specification}.
Ghost state ownership $\ownGhost{\gamma}{a}$ is used in Chapter~\ref{chapter:formalization}.
One can refer to \cite[\S3]{iris_from_the_ground_up} for a discussion.

Now that we can describe resources, we can use them to reason about concurrent programs.
Instead of saying that a program owns a resource, we assign ownership of resources to individual threads.
We furthermore make sure that the resources owned by threads are all disjoint with each other.
This allows us to reason locally about each thread.
When a thread owns $\loc \mapsto v$, it can be sure that no other thread also owns a points-to predicate for the same location.
Otherwise the two threads own resources describing intersecting heap fragments which cannot happen.
Since a thread knows that it exclusively owns $\loc \mapsto v$,
it can be sure that no other thread changes the value stored at $\loc$, thus never invalidating $\loc \mapsto v$.

We conclude this section by discussing plausible specifications for the example programs from the previous section.
We prove these specifications in the next section.
\begin{example}[Parallel execution specification]
\label{example:par-exec-spec}
    The $\parExecName$ function, which we defined in Example~\ref{example:par-exec-code},
    has the following specification:
    \begin{mathpar}
        \axiomH{ParExec-Spec}
            {\hoare{\loc \mapsto k}{\parExec{\loc}}{\Ret \loc'. \loc \mapsto (2 \cdot k) \ast \loc' \mapsto 40}}
    \end{mathpar}
    In this case $\loc$ is a location and $k$ an integer.
    We often leave these variables unbound, especially when it is clear from the context what their type should be.

    This specification says that, if we own $\loc \mapsto k$ and execute $\parExec{\loc}$,
    the function returns a location $\loc'$ and ownership of the resources $\loc \mapsto (2 \cdot k)$, $\loc' \mapsto 40$.
    As we own both resources, they should be disjunct, meaning that $\loc \neq \loc'$.
    This essentially tells us that the location $\loc'$ is fresh, i.e. a new location.
\end{example}

\newcommand{\isListName}{\operatorname{isList}}
\newcommand{\isList}[2]{\isListName(#1, #2)}
\newcommand{\app}{\mathbin{++}}
\begin{example}[List append specification]
\label{example:list-append-spec}
    When we make a call $\append{v_1}{v_2}$, we should make sure that both $v_1, v_2$ are actual representations of lists of values.
    Otherwise the execution gets stuck at some point.
    We do this using an abstract predicate $\isListName \colon \Val \to \textdom{List}(\Val) \to \iProp$.
    This predicate takes a value $v$ and a list of values $\vec{x}$,
    and asserts whether the value $v$ represents the list $\vec{x}$, according to the representation we discussed in Example~\ref{example:list-append-code}.
    Before we discuss the definition of $\isListName$, let us look at how we can use it in the specification for $\appendName$:
    \begin{mathpar}
        \axiomH{Append-Spec}
            {\hoare{\isList{v_1}{\vec{x_1}} \ast \isList{v_2}{\vec{x_2}}}{\append{v_1}{v_2}}{\Ret v. \isList{v}{\vec{x_1} \app \vec{x_2}}}}
    \end{mathpar}
    This specification states that, if we have two lists of values $\vec{x_1},\vec{x_2}$,
    and two values $v_1,v_2$ representing these lists respectively,
    and we execute $\append{v_1}{v_2}$, the function returns a value $v$,
    which represents the list $\vec{x_1} \app \vec{x_2}$.

    We define $\isListName$ by recursion on the list, as follows:
    \begin{align*}
        \isList{v}{[\,]} \eqdef{}& v = \None \\
        \isList{v}{\cons{a}{\vec{x}}} \eqdef{}& \Exists \loc, v'. v = \Some(\loc) \ast \loc \mapsto (a, v') \ast \isList{v'}{\vec{x}}
    \end{align*}
    This corresponds with the intuitive representation we discussed in Example~\ref{example:list-append-code}.
    The empty list is represented by $\None$ and a non empty list is represented by $\Some(\loc)$,
    where the value stored at $\loc$ is a pair containing the head of the list and the representation of the tail of the list.
\end{example}


\section{Proving specifications}\label{sec:prelims-proving-specs}
Until now we have seen how we can create programs and what kind of specifications we can write down.
The final piece of the puzzle is to be able to prove specifications.
We start by giving some general proof rules,
after which we prove the specifications we discussed in Examples~\ref{example:par-exec-spec},~\ref{example:list-append-spec}.

Before we can give proof rules we need to present the \emph{entailment} relation $\proves$.
Whenever we write $P \proves Q$ we mean that if $P$ holds, then $Q$ also holds.
Furthermore, we write $\proves P$ when $P$ holds without any assumptions,
and $P \provesIff Q$ when both $P \proves Q$ and $Q \proves P$.
Although we present Hoare triples are first-class citizens of Iris,
we write them without the entailment in from of them, for simplicity sake.

We start by introducing the rules for separating conjunction and the magic wand.
\begin{mathpar}
    \axiomH{Sep-Unit}
        {\TRUE \ast P \provesIff P}
    \and
    \axiomH{Sep-Weaken}
        {P \ast Q \proves P}
    \and
    \axiomH{Sep-Comm}
        {P \ast Q \provesIff Q \ast P}
    \and
    \axiomH{Sep-Assoc}
        {P \ast (Q \ast R) \provesIff (P \ast Q) \ast R}
    \and
    \inferH{Sep-Mono}
        {P_1 \proves Q_1 \and P_2 \proves Q_2}
        {P_1 \ast P_2 \proves Q_1 \ast Q_2}
    \and
    \inferH{Wand-Intro}
        {P \ast Q \proves R}
        {P \proves Q \wand R}
    \and
    \inferH{Wand-Elim}
        {P \proves Q \wand R}
        {P \ast Q \proves R}
\end{mathpar}
The unit resource of $\ast$ is $\TRUE$.
We can use \ruleref{Sep-Weaken} to throw away resources.%
\footnote{One could notice that by having this rule, it is impossible to reason about memory leaks.
    Some separation logics therefore exclude this rule.
    A discussion on why this rule is present in Iris can be found in~\parencite[\S9]{iris_from_the_ground_up}.
    We discuss how this rule restricts a possible extension of the specification in Chapter~\ref{conclusions}.}
We furthermore have that the separating conjunction is both commutative and associative.
If we want to prove a separating conjunction,
we can use \ruleref{Sep-Mono} to prove both parts separately by also splitting our assumptions.
Finally, \ruleref{Wand-Intro} and \ruleref{Wand-Elim} describe that the magic wand can be seen as an implication for separation logic.
These rules are regularly used when proving specifications.
We typically use them implicitly, just like we normally do when using proof rules of the logical connectives in higher order logic.
Iris of course also has the regular proof rules for higher order logic which we do not state here.

We now discuss the following proof rules for Hoare triples:
\begin{mathpar}
    \inferH{Hoare-Frame}
        {\hoare{P}{e}{\Ret v. Q}}
        {\hoare{P \ast R}{e}{\Ret v. Q \ast R}}
    \and
    \inferH{Hoare-Conseq}
        {P \proves P' \and \hoare{P'}{e}{\Ret v. Q'} \and \All u. (Q'[u/v] \proves Q[u/v])}
        {\hoare{P}{e}{\Ret v. Q}}
    \and
    \inferH{Hoare-Pure}
        {\hoare{P}{e_2}{\Ret v. Q} \and e_1 \rightarrow_{\text{pure}} e_2}
        {\hoare{P}{e_1}{\Ret v. Q}}
    \and
    \axiomH{Hoare-Val}
        {\hoare{\TRUE}{v}{\Ret w. w = v}}
\end{mathpar}
The most important rule of separation logic is \ruleref{Hoare-Frame}.
Like we discussed earlier, $P$ can be seen as a footprint of $e$.
As $e$ only uses those resources in $P$, we can be sure that a disjoint \emph{frame} $R$,
still holds after $e$ has finished executing.
This rule is what allows us to write specifications which only describe a small part of the heap,
but instantiate them for any heap satisfying that precondition.
We could for example use it to state another specification for $\parExecName$:
\begin{gather*}
    \hoare{\loc \mapsto k \ast \loc'' \mapsto v}{\parExec{\loc}}{\Ret \loc'. (\loc \mapsto (2 \cdot k) \ast \loc' \mapsto 40) \ast \loc'' \mapsto v}
\end{gather*}
As location $\loc''$ is not used in $\parExecName$, we can be sure that $\loc'' \mapsto v$ still holds in the postcondition.
We often use this rule implicitly in proofs.

\ruleref{Hoare-Conseq} is similar to the consequence rule from Hoare logic.
It allows us to weaken the precondition and strengthen the postcondition.
This rules is also used implicitly in proofs most of the times.

If we can do a pure program step from $e_1$ to $e_2$, i.e. one which does not involve using the heap,
we can use \ruleref{Hoare-Pure} to prove a Hoare triple for $e_2$.
These pure program steps are defined like one would expect for a lambda calculus.
The complete definition of $\rightarrow_{\text{pure}}$ is given in~\parencite[\S 12]{iris:technical_reference}.%
\footnote{Note that they use a different notation for pure reductions, namely $\hstep[\epsilon]$.}

Finally, \ruleref{Hoare-Val} states that a program consisting of a single value returns that value.

We now give proofs of the specifications for $\parExecName,\appendName$ we gave in Examples~\ref{example:par-exec-spec},~\ref{example:list-append-spec}.
We give our proofs as decorated programs, accompanied with extra explanation where needed.
In these decorated programs we write resources in \textcolor{rescolor}{green}, code in \textcolor{codecolor}{blue}
and comments explaining intermediate steps in ML comment style: \COMMENT{Comment}.

\begin{figure}[t]
    \begin{proofoutline*}
        \RES{\loc \mapsto k} \\
        \CODE{\Let \loc' = \Alloc(37) in} \\
        \COMMENT{Use \ruleref{Hoare-Alloc}} \\
        \RES{\loc \mapsto k \ast \loc' \mapsto 37} \\
        \text{(* Use \ruleref{Hoare-Par} and give $\loc \mapsto k$ to the left thread,} \\
        \text{\phantom{(* } and $\loc' \mapsto 37$ to the right thread *)} \\
        $\left(
        \begin{array}{l||l}
            \begin{array}{@{}l@{}}
                \RES{\loc \mapsto k} \\
                \CODE{\Let n = \deref \loc in} \\
                \COMMENT{Use \ruleref{Hoare-Load}} \\
                \RES{n = k \ast \loc \mapsto n} \\
                \CODE{\loc \gets 2 \cdot n} \\
                \COMMENT{Use \ruleref{Hoare-Store}} \\
                \RES{n = k \ast \loc \mapsto 2 \cdot k} \\
                \COMMENT{Use \ruleref{Sep-Weaken}} \\
                \RES{\loc \mapsto 2 \cdot k}
            \end{array} &
            \begin{array}{@{}l@{}}
                \RES{\loc' \mapsto 37} \\
                \CODE{\Let m = \deref \loc' in} \\
                \COMMENT{Use \ruleref{Hoare-Load}} \\
                \RES{m = 37 \ast \loc' \mapsto 37} \\
                \CODE{\loc' \gets m + 3} \\
                \COMMENT{Use \ruleref{Hoare-Store}} \\
                \RES{m = 37 \ast \loc' \mapsto 40} \\
                \COMMENT{Use \ruleref{Sep-Weaken}} \\
                \RES{\loc' \mapsto 40}
            \end{array}
        \end{array}
        \right)$ \\
        \RES{\loc \mapsto (2 \cdot k) \ast \loc' \mapsto 40} \\
        \CODE{\loc'} \\
        \COMMENT{Use \ruleref{Hoare-Val}} \\
        \RES{\Ret \loc'. \loc \mapsto (2 \cdot k) \ast \loc' \mapsto 40}
    \end{proofoutline*}
    \caption{Proof sketch of $\parExecName$.}\label{fig:prelims-par-exec-proof}
\end{figure}

\begin{example}[Proof of specification of $\parExecName$]
    Before we discuss the proof sketch given in Figure~\ref{fig:prelims-par-exec-proof},
    we need the following proof rules:
    \begin{mathpar}
        \inferH{Hoare-Par}
            {\hoare{P_1}{e_1}{Q_1} \and \hoare{P_2}{e_2}{Q_2}}
            {\hoare{P_1 \ast P_2}{e_1 \mathbin{||} e_2}{Q_1 \ast Q_2}}
        \and
        \axiomH{Hoare-Alloc}
            {\hoare{\TRUE}{\Alloc(v)}{\Ret \loc. \loc \mapsto v}}
        \and
        \axiomH{Hoare-Load}
            {\hoare{\loc \mapsto v}{\deref \loc}{\Ret w. w = v \ast \loc \mapsto v}}
        \and
        \axiomH{Hoare-Store}
            {\hoare{\loc \mapsto v}{\loc \gets w}{\Ret u. u = \TT \ast \loc \mapsto w}}
    \end{mathpar}
    We discuss them on demand, whenever we come across a use of them.

    We start with the precondition $\loc \mapsto k$.
    We then use \ruleref{Hoare-Alloc} which gives us a fresh location together with its points-to predicate $\loc' \mapsto 37$.
    By framing $\loc \mapsto k$ implicitly, we end up with $\loc \mapsto k \ast \loc' \mapsto 37$.
    We now need to use \ruleref{Hoare-Par}. It allows us to split our precondition and prove Hoare triples for both threads separately.
    We then get both postconditions of the individual threads, as a postcondition of the par operation.
    The reason that this rule is sound, is that if $e_1, e_2$ use disjoint resources, their execution order does not matter.
    We can thus focus on the individual threads. Their postconditions still describe disjunct resources, so we can safely take the separating conjunction of them.
    In our proof sketch, we only discuss the left thread, as the right thread has a completely analogous proof.
    We first use \ruleref{Hoare-Load} to load the current value from $\loc$.
    It says that the return value, is the value asserted by the points-to predicate.
    We furthermore get the points-to predicate back via the postcondition.
    We then use \ruleref{Hoare-Store} to update the value stored at $\loc$.
    It takes a points-to predicate as a precondition and gives it back with the value stored updated to $w$.
    We now know that $n = k \ast \loc \mapsto 2 \cdot k$ holds.
    As we do not need the fact that $n = k$ for the rest of the proof, we can use \ruleref{Hoare-Conseq} and \ruleref{Sep-Weaken} to forget this.
    Now that we have proven Hoare triples for both threads, we know that both their postconditions hold.
    Finally, the program returns $\loc'$ so we conclude by using \ruleref{Hoare-Val}.
\end{example}

\begin{figure}[t]
    \begin{proofoutline*}
        \RES{\isList{v_1}{\vec{x_1}} \ast \isList{v_2}{\vec{x_2}}} \\
        \MatchML v_1 with
          \None =>
        \begin{array}[t]{@{}l@{}}
            \RES{v_1 = \None \ast \isList{v_1}{\vec{x_1}} \ast \isList{v_2}{\vec{x_2}}} \\
            \COMMENT{$\vec{x_1} = []$ as $v_1 = \None$} \\
            \RES{\vec{x_1} = [] \ast \isList{v_2}{\vec{x_2}}} \\
            \COMMENT{$\vec{x_2} = \vec{x_1} \app \vec{x_2}$ as $\vec{x_1} = []$} \\
            \RES{\isList{v_2}{\vec{x_1} \app \vec{x_2}}} \\
            \CODE{v_2} \\
            \COMMENT{Use \ruleref{Hoare-Val}} \\
            \RES{\Ret v. \isList{v}{\vec{x_1} \app \vec{x_2}}}
        \end{array}
        | \Some(\mathit{hd}) =>
        \begin{array}[t]{@{}l@{}}
            \RES{v_1 = \Some(\mathit{hd}) \ast \isList{v_1}{\vec{x_1}} \ast \isList{v_2}{\vec{x_2}}} \\
            \COMMENT{$\vec{x_1} = \cons{a}{\vec{x_1'}}$ as $v_1 = \Some(\mathit{hd})$, unfold $\isList{v_1}{\vec{x_1}}$} \\
            \RES{\vec{x_1} = \cons{a}{\vec{x_1'}} \ast \mathit{hd} \mapsto (a, w_1) \ast \isList{w_1}{\vec{x_1'}} \ast \isList{v_2}{\vec{x_2}}} \\
            \CODE{\Let x = \Fst (\deref \mathit{hd}) in} \\
            \COMMENT{Use \ruleref{Hoare-Load}} \\
            \RES{x = a \ast \mathit{hd} \mapsto (a, w_1) \ast \isList{w_1}{\vec{x_1'}} \ast \isList{v_2}{\vec{x_2}}} \\
            \CODE{\Let \mathit{xs} = \Snd (\deref \mathit{hd}) in} \\
            \COMMENT{Use \ruleref{Hoare-Load}} \\
            \RES{\mathit{xs} = w_1 \ast \mathit{hd} \mapsto (a, w_1) \ast \isList{w_1}{\vec{x_1'}} \ast \isList{v_2}{\vec{x_2}}} \\
            \CODE{\Let v_1' = \mathit{append}(\mathit{xs}, v_2) in} \\
            \COMMENT{Induction hypothesis} \\
            \RES{\mathit{hd} \mapsto (a, w_1) \ast \isList{v_1'}{\vec{x_1'} \app \vec{x_2}}} \\
            \CODE{\mathit{hd} \gets (x, v_1');} \\
            \COMMENT{Use \ruleref{Hoare-Store} and fact that $x = a$} \\
            \RES{\mathit{hd} \mapsto (a, v_1') \ast \isList{v_1'}{\vec{x_1'} \app \vec{x_2}}} \\
            \COMMENT{Fold $\isListName$} \\
            \RES{\isList{\Some(\mathit{hd})}{\cons{a}{\vec{x_1'} \app \vec{x_2}}}} \\
            \CODE{\Some(\mathit{hd})} \\
            \COMMENT{Use \ruleref{Hoare-Val}} \\
            \RES{\Ret v. \isList{v}{\cons{a}{\vec{x_1'} \app \vec{x_2}}}} \\
            \COMMENT{Rewrite $\vec{x_1} = \cons{a}{\vec{x_1'}}$} \\
            \RES{\Ret v. \isList{v}{\vec{x_1} \app \vec{x_2}}}
        \end{array}
        end {} \\
        \RES{\Ret v. \isList{v}{\vec{x_1} \app \vec{x_2}}}
    \end{proofoutline*}
    \caption{Proof sketch of $\appendName$.}\label{fig:prelims-list-append-proof}
\end{figure}

\begin{example}[Proof of specification of $\appendName$]
\label{example:list-append-proof}
    We now prove the following specification for $\appendName$:
    \begin{gather*}
        \hoare{\isList{v_1}{\vec{x_1}} \ast \isList{v_2}{\vec{x_2}}}{\append{v_1}{v_2}}{\Ret v. \isList{v}{\vec{x_1} \app \vec{x_2}}}
    \end{gather*}
    We prove this by induction on $\vec{x_1}$. Its proof sketch is given in Figure~\ref{fig:prelims-list-append-proof}.
    We prove this specification by showing that it holds for both cases of the $\langkw{match}$ statement.
    We first discuss the first case, where we know that $v_1 = \None$.
    If we look at the definition of $\isListName$, which we gave in Example~\ref{example:list-append-spec},
    we see that it has to be the case that $\vec{x_1} = \None$. We can thus conclude that $v_2$ represents $\vec{x_1} \app \vec{x_2}$.
    We then use \ruleref{Hoare-Val}, to get the desired postcondition.
    In the $\Some$ case, we can reason in a similar way to the $\None$ case to conclude that $\vec{x_1} = \cons{a}{\vec{x_1'}}$,
    for some value $a$ and list of values $\vec{x_1'}$. If we then unfold $\isListName$, we get the value $w_1$, which represents $\vec{x_1'}$.
    We thus have $\mathit{hd} \mapsto (a, w_1) \ast \isList{w_1}{\vec{x_1'}}$.
    We then use \ruleref{Hoare-Load} twice to read the values of the pair stored in $\mathit{hd}$.
    We can use our induction hypothesis for the inductive call to $\mathit{append}$.
    We know $\isList{w_1}{\vec{x_1'}} \ast \isList{v_2}{\vec{x_2}}$ and $\vec{x_1'}$ is the parameter we can use our induction hypothesis for.
    By our induction hypothesis, we know that $\isList{v_1'}{\vec{x_1'} \app \vec{x_2}}$ holds.
    We now use \ruleref{Hoare-Store} to update the tail of our list.
    Finally, we fold the definition of $\isListName$ and use \ruleref{Hoare-Val} to conclude our postcondition.
\end{example}
