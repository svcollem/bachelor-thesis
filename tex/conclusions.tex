\chapter{Conclusions and Future Work}\label{conclusions}

In this thesis we formally verified two of the three barrier implementations discussed in~\parencite{Dodds_et_al_2016}.
To do so, we designed two new constructions: \emph{recursively nested wands} and \emph{authoritative ordering}.
These constructions allowed us to define a different invariant, which required less bookkeeping,
ultimately leading to more straightforward proofs.

One possible venture for future work is a formal verification of the third barrier implementation discussed in~\parencite{Dodds_et_al_2016}.
This implementation uses a tree structure, instead of a linear chain.
The tree has the invariant that if the signal flag of a node is set, all the flags of its children are also set.
This means that $\waitName$ has to check less flags, resulting in a more efficient implementation.
We reckon that this implementation can be verified in a similar way to our verification presented in Chapter~\ref{chapter:formalization}.
To do so, one would need to generalize \emph{authoritative ordering}.
Currently, it models the ordering of a list, which can be seen as a path graph or linear graph.
This could however be extended to arbitrary finite graphs,
allowing one to model the $\earlier$-predicate for the tree based implementation.

Another possibility for future work, is to add deallocation to the implementation and specification.
Currently, it is not possible to deacllocate a barrier.
To implement deallocation, one could use reference counting.
However, we cannot describe the absence of memory leaks in the specification,
as Iris is an \emph{affine} separation logic~\parencite[\S9.5]{iris_from_the_ground_up}:
using \ruleref{Sep-Weaken}, one is able to throw away a $\send{b}{P}$ predicate,
which means the barrier will never be deallocated.
To completely eliminate the possibility of memory leaks,
one could use Iron~\parencite{iron}.
This is a separation logic modelled on top of Iris,
which allows one to reason about the absence of leaked resources.

Finally, we only verified partial correctness for the barriers,
i.e. if the $\waitName$ calls terminate, the appropriate resource is given to the client.
This does mean that a client cannot use these specifications to get a total correctness proof of their program.
However, there are many possible programs, where a client would like to use a barrier,
but still have the guarantee that their program eventually terminates.
It would thus be interesting to see if it is possible to write a specification which guarantees termination of $\waitName$ under fair use,
i.e. if someone owns $\send{b}{P}$, they eventually call $\signal{b}$.
However, Iris cannot be used to prove termination under fair scheduling.
TaDA Live~\parencite{tada_live} is a separation logic which allows to reason about termination of blocking fine-grained concurrent programs.
Our barrier is such a program, as $\waitName$ is essentially a busy wait loop.
However, TaDA Live is not higher order, which means we cannot write the specification using higher order predicates like $\send{b}{P}$ and $\recv{b}{P}$,
with $P$ being an arbitrary separation logic proposition.
