\chapter{Related Work}\label{relatedwork}

In this chapter we discuss related work.
We start of by discussing the differences between our work and the verification by \Citeauthor{Dodds_et_al_2016}.
We then compare our two new constructions, \emph{authoritative ordering} and \emph{recursively nested wands},
to earlier work, and conclude by discussing a separation logic with Pthreads-style barriers.

\paragraph{Specification and implementation.}
Our specification from Figure~\ref{fig:chainable-barrier-specification} is slightly different from the one presented by \cite{Dodds_et_al_2016}.
First of all, we added a $\cloneName$ operation which allows for send splitting.
We furthermore added \ruleref{Recv-Weaken-Chainable} and \ruleref{Send-Strengthen-Chainable}.
\cite{ho_ghost_state} already added \ruleref{Recv-Weaken-Chainable} to the specification in their Coq formalization,
and we added \ruleref{Send-Strengthen-Chainable} in a similar way.

Our specification of $\extendName$ is also slightly different from \cite{Dodds_et_al_2016}.
This is because their $\extendName$ function returned a tuple of nodes, where the second node was the same as the argument.
However, this was not clear from the specification. Our $\extendName$ only returns the new node,
which allows for a shorter \ruleref{Extend-Spec-Chainable} rule.

\cite{Dodds_et_al_2016} had to reason about \emph{stability} of resources~\parencite{explicit_stabilisation} in their specification.
They had to do this because some iCAP propositions are not \emph{stable},
which means that a proposition may be invalidated after another thread has executed.
We, however, did no have to reason about stability, as all Iris propositions are stable~\parencite[\S7.1]{jung_thesis}.

Finally, we had to add a $\CAS$ loop to the implementation of $\extendName$.
By introducing send splitting, it is possible that multiple threads try to extend a node at the same time.
To deal with this data race, we use a $\CAS$ loop.
As \cite{Dodds_et_al_2016} did not have send splitting, they were sure that their was only one thread allowed to extend a node,
hence why they did not have to take care of the data race in their implementation.


\paragraph{Logical state.}
The main difference between our verification in Chapter~\ref{chapter:formalization} and \cite[Section~6]{Dodds_et_al_2016},
is how we defined our logical state.
Their $\mathsf{CNode}$ structure, which corresponds to our $\nodeStruct$ structure,
has an additional $\mathcal{W}$ field, which holds a finite set of ghost names.%
\footnote{The terminology and field names they use is a bit different, e.g. they have \emph{region identifiers} instead of ghost names,
    and their $\mathcal{I}$ field corresponds to our $\recvs$ field.
    For simplicity, we stick to the terminology used in this thesis.}
With this field, each node keeps track of the resources that are renounced on earlier nodes,
and to which they will later get access.
In our verification, when we renounce a resource we directly move it from the \emph{earlier} to the \emph{later} node.
They instead keep the resource in the $\recvs$ set of the earlier node, but also add it to the $\mathcal{W}$ set of the later node.
Their resource invariant then roughly becomes the following:%
\footnote{One can clearly see that our resource invariant is inspired by theirs.}
\begin{gather*}
    \Sep_{n {}\in{} \chain(n_{\mathit{hd}})}
        \left(\smashoperator[r]{\Sep_{\gamma \in n.\sends \cup n.\mathcal{W}}} \Exists P. \later P \ast \gamma \Mapsto P\right) \wand
        \left(\smashoperator[r]{\Sep_{\gamma \in n.\recvs}} \Exists P. \later P \ast \gamma \Mapsto P\right)
\end{gather*}
A resource $P \in n.\recvs$ can thus only be received once all resources in $n.\sends$ and $n.\mathcal{W}$ have been supplied.
This makes their resource invariant simpler, as their is no need for recursively nested wands.
However, receiving a renounced resource becomes more involved.
They show that, for a node $n$, once all previous and its own counter are set to 0,
one can remove all resources from $n.\mathcal{W}$, by cancelling them with earlier renounced resources (Lemma 6.3).
To prove this they need an additional property in their invariant,
namely well-formedness, which states that for each node $n$, the resources stored in $n.\mathcal{W}$ are actually available from nodes earlier in the chain.
Proving preservation of well-formedness is conceptually not too hard,
but it does require a lot of bookkeeping, which can become quite involved in a proof assistant.
We do not have to do this bookkeeping, as we move resources immediately when they get renounced,
as opposed to doing it when all resources are sent.

\paragraph{State transition systems.}
Our verification made use of \emph{resource algebras},
while both \cite{Dodds_et_al_2016} and \cite{ho_ghost_state} used \emph{state transitions systems} (STS) for the verification of the barriers.%
\footnote{The verification of the simple barrier by \cite{ho_ghost_state} was later redone using resource algebras, see \url{https://gitlab.mpi-sws.org/iris/examples/-/merge_requests/16}.}
State transition systems, whose use in separation logic was pioneered by CaReSL~\parencite{caresl_model,caresl},
are an intuitive way to write down how different threads interact with a program.
It consists of a set of states, transitions between them and a state interpretation function.
Some transitions are only allowed by certain threads, which is modelled by the transition requiring a certain token from a thread.
The state interpretation function relates the state of the STS with both the physical and the logical state.

While a STS is an intuitive representation of a protocol on paper,
reasoning with it in Coq is rather tedious.
In Iris, it is more common to reason with resources algebras~\parencite[\S2.1]{iris_from_the_ground_up}.
As \cite{jung_thesis} stated in his PhD thesis: ``This is probably the biggest hurdle someone has to overcome to become proficient in Iris''.
However, once proficient with them, proofs typically become smaller.%
\footnote{For example, when the simple barrier verification in Iris was rewritten to use resources algebras instead of a STS, there was a \texttt{+87,-224} line difference \url{https://gitlab.mpi-sws.org/iris/examples/-/merge_requests/16/diffs}.}


\paragraph{Authoritative ordering.}
Our authoritative ordering construction~(\S\ref{sec:formal-auth-ordering}) is inspired by a construction which models partial bijections \parencite[\texttt{iris/base\_logic/lib/gset\_bij.v}]{iris_coq}.
In a similar way to authoritative ordering, there is an authoritative element which keeps track of the complete partial bijection,
and fragments which keep track of individual pairs which are related by the bijection.
They make use of the \emph{view camera}~\parencite[\texttt{iris/algebra/view.v}]{iris_coq},
which is a generalization of the \emph{authoritative camera}~\parencite[\S3.6]{iris_1} we use.
If we want to generalize the authoritative ordering construction, on which we shortly comment in Chapter~\ref{conclusions},
it would be good to use \emph{view camera} as well,
although that probably means we would need another abstraction layer to close the fragmental elements under transitivity.


\paragraph{Recursively nested wands.}
The recursively nested wands construction is somewhat similar to the \emph{accessor} pattern in Iris~\parencite[\S5.6]{jung_thesis},
which is related to a \emph{ramification}~\parencite{ramification}. A ramification is of the following form:
\begin{gather*}
    R \proves P \ast (Q \wand R')
\end{gather*}
If one has ownership of $R$, they can exchange it for ownership of $P$, some smaller part of the resource.
They can then update that resource to $Q$ and update the whole resource to $R'$ using $Q \wand R'$.
This allows one to temporarily focus on a specific part of $R$ and then update it to $R'$.

If we compare this to the resource invariant, we can see a similar pattern.
As an illustration, let us take the following resource invariant, which we used as an example in~\S\ref{sec:chainable-verification-invariant}:
\begin{gather*}
    \underbrace{(P \ast Q)}_{b_2.\sends} \wand (\underbrace{P}_{b_2.\recvs} \ast\ (\underbrace{R}_{b_1.\sends} \wand \underbrace{(Q \ast R)}_{b_1.\recvs}))
\end{gather*}
Before one can receive, or access, $R$, all of $P,Q,R$ have to be sent.
The main difference is that these resources are gradually being sent.
One typically uses the accessor pattern to temporarily access a small part of the resource,
where as the premises of the resource invariant are gradually satisfied until one can receive a resource.


\paragraph{Pthreads-style barrier.}
\newcommand{\hoborBarrierCall}[1]{\textbf{barrier}\spac #1}
\newcommand{\hoborBarrier}[3]{\operatorname{\mathsf{barrier}}(#1, #2, #3)}
\newcommand{\lookupMove}[4]{\operatorname{\mathsf{lookup\_move}}(#1, #2, #3, #4)}
\cite{hobor_barrier} designed a separation logic with pthreads-style barriers as a primitive.
This is different from our work, as we implemented a barrier with low-level operations,
while they took barriers as a primitive and assumed operational semantics for them.
Using this operational semantics they proved a higher order specification for the barriers.
They verified the soundness of their separation logic using Coq.

The semantics of the pthreads barriers studied by \Citeauthor{hobor_barrier}, differs slightly from the barriers studied by us:
a $\hoborBarrierCall{\mathit{bn}}$ call, signals the barrier,
but also waits until a number of other threads made a $\hoborBarrierCall{\mathit{bn}}$ call as well.
They can thus be used to synchronize multiple threads.
After the threads are synchronized, the barrier is reset and can be used again for another synchronization.
This is different from our implementation, where the $\signalName$ and $\waitName$ functions are split up,
and a barrier cannot be reset after its counter is set to 0.

Their key insight was that these barrier are used to redistribute ownership of resources between threads.
They furthermore noticed that this can be modelled using a finite automata,
where each barrier synchronization corresponds to a transition in the automata.
To model this in their separation logic, they added a $\hoborBarrier{\mathit{bn}}{\pi}{\mathit{cs}}$ proposition,
which states that barrier $\mathit{bn}$, owned with fractional permission $\pi$~\parencite{Boyland2003,Bornat_et_al_2005}, is in state $\mathit{cs}$.
To make sure that a state transition from $\mathit{cs}$ to $\mathit{ns}$ only redistributes resources,
they added the following conditions:
\begin{gather}
    \left.\begin{alignedat}{3}
        &\textstyle\Sep_{i} \mathit{Pre}_i &{}={}& F \ast \hoborBarrier{\mathit{bn}}{\blacksquare}{\mathit{cs}} \\
        &\textstyle\Sep_{i} \mathit{Post}_i &{}={}& F \ast \hoborBarrier{\mathit{bn}}{\blacksquare}{\mathit{ns}}
    \end{alignedat}\right\}\label{eq:hobor-pre-post}
    \\
    \Exists \pi. \mathit{Pre}_i \Rightarrow \top \ast \hoborBarrier{\mathit{bn}}{\pi}{\mathit{cs}}\label{eq:hobor-pre-implies-barrier}
\end{gather}

For a thread $i$, its pre and postconditions of the $\hoborBarrierCall{bn}$ are $\mathit{Pre}_i, \mathit{Post}_i$ respectively.
Equation~\ref{eq:hobor-pre-post} requires that the combination of all preconditions should include full ownership of the barrier, denoted by the $\blacksquare$.
Furthermore, the combination of all postconditions should be the almost the same:
only the barrier resource is allowed to transition from state $\mathit{cs}$ to $\mathit{ns}$.
Equation~\ref{eq:hobor-pre-implies-barrier} requires that each thread has ownership of some part of the barrier resource.
Using these requirements,\footnote{and a few more technical requirements} a user can define a automata and add it to the context $\Gamma$.
This gives rise to their Hoare rule for $\hoborBarrierCall{\mathit{bn}}$:
\begin{gather*}
    \inferrule
        {\Gamma[\mathit{bn}] = \mathit{bd} \and \lookupMove{\mathit{bd}}{\mathit{cs}}{\mathit{dir}}{\mathit{mv}} = (P, Q)}
        {\Gamma \proves \hoare{P}{\hoborBarrierCall{\mathit{bn}}}{Q}}
\end{gather*}
If there is a transition in the automata $\mathit{bd}$ with precondition $P$ and postcondition $Q$,
then one can use these as pre and postconditions of the $\hoborBarrierCall{\mathit{bn}}$ call.
Note by construction of the automata, condition~\ref{eq:hobor-pre-implies-barrier} has to hold for $P$.

In follow-up work, \cite{hobor_barrier_tool} integrated this logic into the HIP/SLEEK program verification toolset~\parencite{hip,sleek}.
In their earlier work, they showed how one could define a barrier automata in their Coq development.
Even for a quite simple automata, with only 4 states and 4 transitions,
they had a Coq file of 2700 lines, to verify that this automata was well defined.
Verification of this script took approximately 48 seconds.
The definition of the same automata in SLEEK consisted of only 20 lines,
and was verified in less than $2.3$ seconds.
