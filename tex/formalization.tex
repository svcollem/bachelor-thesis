\chapter{Formalization}\label{chapter:formalization}
In this chapter we present our formalization to verify the specification of the chainable barrier (Figure~\ref{fig:chainable-barrier-specification}).
Like we did in the verification of the simple barrier~(\S\ref{sec:simple-barrier-verification}),
we have to find the right definitions for the abstract $\sendName$, $\recvName$ and $\earlier$-predicates.
Given the right definitions, we verify the rules from Figure~\ref{fig:chainable-barrier-specification} using the proof rules of Iris.
With the verification of the barrier at hand, we can use it to verify clients,
like we did in Figures~\ref{fig:recv-split-example-proof},~\ref{fig:send-split-example-proof},~\ref{fig:renunciation-example-proof}.
To obtain confidence that our barrier specification is indeed the right one, we can use Iris' adequacy theorem~\parencite[\S7.4]{iris_from_the_ground_up}
to obtain a closed proof stating that the client is safe.

In order to find the right definitions, we use existing Iris techniques,
like we did in the verification of the simple barrier~(\S\ref{sec:simple-barrier-verification}),
and combine these with two new constructions.
The first new construction is called \emph{authoritative ordering}, which we present in~\S\ref{sec:formal-auth-ordering}.
It can be used to keep track of the order of elements in a list via ghost state.
The second new construction is \emph{recursively nested wands}.
This construction is discussed together with the definitions of the abstract $\sendName, \recvName$ and $\earlier$-predicates in~\S\ref{sec:formal-defs}.
This chapter is quite technical,
so readers are assumed to have sufficient knowledge from ``Iris from the group up''~\parencite{iris_from_the_ground_up}.


\section{Authoritative ordering}
\label{sec:formal-auth-ordering}
\newcommand{\orderOwnAuthSymbol}{\operatorname{\mathsf{auth}}_\gamma}
\newcommand{\orderOwnOrderedSymbol}{\earlier_\gamma}
\newcommand{\orderOwnAuth}[2]{\operatorname{\mathsf{auth}}_{#1}(#2)}
\newcommand{\orderOwnOrdered}[3]{#2 \earlier_{#1} #3}
\newcommand{\orderOwnOrderedPre}[3]{#2 \earlier_{#1}^{\text{pre}} #3}
\newcommand{\NoDup}{\operatorname{\textsf{NoDup}}}
\newcommand{\FinSet}{\textdom{FinSet}}
To model the $\earlier$-relation we need a resource which keeps track of the order of the nodes.
In this section we describe a construction which does exactly that.
This construction does no depend on the use of locations.
In fact, we can use this construction to assert an ordering on lists of any type.
The orderings we can describe using this construction are persistent,
and we can always extend an ordering by adding an element somewhere in the ordering.
However, we cannot remove elements from the ordering.
The design of \emph{authoritative ordering} is inspired by a similar construction in Iris,
which models partial bijections~\parencite[\texttt{iris/base\_logic/lib/gset\_bij.v}]{iris_coq}.
We discuss its differences and similarities in Chapter~\ref{relatedwork}.
We now look at the specification of \emph{authoritative ordering}, and then discuss its implementation.

\paragraph{Specification.}
We write $\orderOwnAuth{\gamma}{\vec{x}}$ for \emph{authoritative} ownership of the ordering on list $\vec{x}$.
The ordering of the elements is according to their order in $\vec{x}$.
The owner of this resource allowed to update the ordering by adding new elements to $\vec{x}$.
No one else can own this resource as it is exclusive.
We therefore call this ownership authoritative.
We write $\orderOwnOrdered{\gamma}{a}{b}$ for \emph{fragmental} ownership of the same ordering.
It asserts that $a$ has a lower index in $\vec{x}$ than $b$.
This ownership is fragmental, as it does not say anything about the other elements in $\vec{x}$.
The ghost variable $\gamma$ relates the fragments to the authoritative ordering.
For example, given $\gamma \neq \gamma'$, if we own $\orderOwnAuth{\gamma}{\vec{x}} \ast \orderOwnOrdered{\gamma}{a}{b}$
and $\orderOwnAuth{\gamma'}{\vec{x'}}$,
we know that $a$ occurs earlier in $\vec{x}$ than $b$,
but we do not know anything about the order of $a$ and $b$ in $\vec{x'}$, or if they are even elements of $\vec{x'}$.

Let us now look at the specification of these resources.
\begin{mathpar}
    \axiomhref{$\orderOwnAuthSymbol$-Timeless}{OrderOwnAuth-Timeless}
        {\timeless{\orderOwnAuth{\gamma}{\vec{x}}}}

    \axiomhref{$\orderOwnOrderedSymbol$-Timeless}{OrderOwnOrdered-Timeless}
        {\timeless{\orderOwnOrdered{\gamma}{a}{b}}}

    \axiomhref{$\orderOwnAuthSymbol$-Exclusive}{OrderOwnAuth-Exclusive}
        {\orderOwnAuth{\gamma}{\vec{x}} \ast \orderOwnAuth{\gamma}{\vec{x'}} \proves \FALSE}

    \axiomhref{$\orderOwnOrderedSymbol$-Persistent}{OrderOwnOrdered-Persistent}
        {\orderOwnOrdered{\gamma}{a}{b} \proves \always (\orderOwnOrdered{\gamma}{a}{b})}

    \axiomhref{$\orderOwnOrderedSymbol$-Trans}{OrderOwnOrdered-Trans}
        {\orderOwnOrdered{\gamma}{a}{b} \ast \orderOwnOrdered{\gamma}{b}{c} \proves \orderOwnOrdered{\gamma}{a}{c}}

    \inferhref{$\orderOwnOrderedSymbol$-Get}{OrderOwnOrdered-Get}
        {\Exists i < j. \land \vec{x}(i) = a \land \vec{x}(j) = b}
        {\orderOwnAuth{\gamma}{\vec{x}} \proves \orderOwnOrdered{\gamma}{a}{b}}

    \axiomhref{$\orderOwnOrderedSymbol$-Indices}{OrderOwnOrdered-Indices}
        {\orderOwnAuth{\gamma}{\vec{x}} \ast \orderOwnOrdered{\gamma}{a}{b} \proves \Exists i < j. \land \vec{x}(i) = a \land \vec{x}(j) = b}

    \axiomhref{$\orderOwnAuthSymbol$-NoDup}{OrderOwnAuth-NoDup}
        {\orderOwnAuth{\gamma}{\vec{x}} \proves \NoDup(\vec{x})}

    \axiomhref{$\orderOwnAuthSymbol$-Alloc}{OrderOwnAuth-Alloc}
        {\NoDup(\vec{x}) \proves \upd{} \Exists \gamma. \orderOwnAuth{\gamma}{\vec{x}}}

    \inferhref{$\orderOwnAuthSymbol$-Update}{OrderOwnAuth-Update}
        {a \notin \vec{x} \mathbin{++} \vec{x'}}
        {\orderOwnAuth{\gamma}{\vec{x} \mathbin{++} \vec{x'}} \proves \upd{} \orderOwnAuth{\gamma}{\vec{x} \mathbin{++} a \mathbin{::} \vec{x'}}}
\end{mathpar}
Both $\orderOwnAuthSymbol$ and $\orderOwnOrderedSymbol$ are timeless, which allows us to remove $\later$ modalities around them when we are proving Hoare triples.
The exclusivity of $\orderOwnAuthSymbol$ is captured by \ruleref{OrderOwnAuth-Exclusive}.
As we use the authoritative ordering to model the $\earlier$-relation on barrier nodes,
it makes sense that we have \ruleref{OrderOwnOrdered-Persistent} and \ruleref{OrderOwnOrdered-Trans}.
It does however mean that one cannot remove elements from the ordering, as $\orderOwnOrderedSymbol$ is persistent.

The \ruleref{OrderOwnOrdered-Get} rule states that the owner of the authoritative element can prove that $\orderOwnOrdered{\gamma}{a}{b}$
by showing that the index of $a$ in $\vec{x}$ is smaller than the index of $b$.
On the other hand, if the owner of the authoritative element knows that $\orderOwnOrdered{\gamma}{a}{b}$,
they can get the indices of $a$ and $b$ using \ruleref{OrderOwnOrdered-Indices}.

The rule \ruleref{OrderOwnAuth-NoDup} states that an ordering cannot contain duplicate elements.
Without this requirement, one could use \ruleref{OrderOwnOrdered-Trans} and \ruleref{OrderOwnOrdered-Indices}
to prove that $c$ has a lower index than $b$ in the list $[a, b, c, a]$.

To allocate a new ordering, one can use \ruleref{OrderOwnAuth-Alloc}.
The premise requires that the list $\vec{x}$ does not contain duplicate elements, for the same reason as explained above.
The conclusion states that one receives a ghost variable $\gamma$ and ownership of $\orderOwnAuth{\gamma}{\vec{x}}$,
after executing a ghost state update. Recall that this can be done during program execution using \ruleref{Hoare-Conseq-Upd}.

Finally, one can use \ruleref{OrderOwnAuth-Update} to add a new element to the ordering, given that it was not in the ordering before.
This element is added to the ordering after a ghost state update has happened.

\paragraph{Implementation.}
To implement this, we use the following definitions:
\begin{mathpar}
    \begin{array}{@{}l@{}}
        \orderOwnAuth{\gamma}{\vec{x}} \eqdef{} \Exists X.
        \begin{array}[t]{@{}l@{}}
            \NoDup(\vec{x}) \ast \ownGhost{\gamma}{\authfull X} \ast \ownGhost{\gamma}{\authfrag X} \\
            {}\ast{} \All a, b. ((a, b) \in X \leftrightarrow \Exists i < j. \land \vec{x}(i) = x \land \vec{x}(j) = y)
        \end{array}
    \end{array}

    \orderOwnOrderedPre{\gamma}{a}{b} \eqdef{} \ownGhost{\gamma}{\authfrag \{(a,b)\}}

    \begin{array}{@{}l@{}}
        \orderOwnOrdered{\gamma}{a}{b} \eqdef{} a \mathbin{(\earlier_{\gamma}^{\text{pre}})^{+}} b
    \end{array}
\end{mathpar}
The resource algebra being used is $\operatorname{Auth}((\FinSet(A \times A), \cup))$, where $A$ is any type.
The authoritative element quantifies over a set $X$.
This set contains pairs $(a,b)$, whenever $a$ has a lower index in $\vec{x}$ than $b$.
Furthermore, we assert that $\vec{x}$ does not contain duplicates,
and we assert ownership of both $\ownGhost{\gamma}{\authfull X}$ and $\ownGhost{\gamma}{\authfrag X}$.%
\footnote{The ownership of the fragment $\ownGhost{\gamma}{\authfrag X}$ may seem a bit weird,
    but it is actually needed to prove \ruleref{OrderOwnOrdered-Get}.
    This trick was also used in the partial bijection construction we mentioned earlier.}

To define $\earlier_{\gamma}$, we first define $\earlier_{\gamma}^{\text{pre}}$,
which asserts ownership of the fragment $\ownGhost{\gamma}{\authfrag \{(a,b)\}}$.
Due to the definition of $\orderOwnAuthSymbol$, this makes sure that $a$ has a lower index in the list than $b$.
Finally, to allow us to prove \ruleref{OrderOwnOrdered-Trans},
we define $\earlier_{\gamma}$ as the transitive closure.%
\footnote{This is the transitive closure for Iris propositions,
    which we added to Iris as part of this thesis: \url{https://gitlab.mpi-sws.org/iris/iris/-/merge_requests/862}}
of $\earlier_{\gamma}^{\text{pre}}$

The proof that this implementation satisfies the implementation can be found in the Coq formalization~\parencite{coq_formalization}.
Let us next look at how we can use this construction to define the $\sendName$, $\recvName$ and $\earlier$-predicates.


\section{Definitions}
\label{sec:formal-defs}
\newcommand{\FinMap}{\textdom{FinMap}}
\renewcommand{\List}{\textdom{List}}
\newcommand{\BName}{\textdom{BName}}
\newcommand{\resources}{\operatorname{resources}}
\newcommand{\chain}{\operatorname{chain}}
\newcommand{\meta}{\operatorname{\textsf{meta}}}

\begin{figure}
    \begin{mathpar}\footnotesize
        \begin{array}{@{}l@{}}
            \begin{array}{@{}l@{\quad\quad\quad\quad\quad}l@{}}
                \quad\quad\quad\quad\quad\quad\quad
                \begin{array}[t]{@{}l@{}}
                    \nodeStruct \eqdef{} \{ \\
                    \begin{array}[t]{@{\quad}l@{\phantom{i}:\phantom{i}}l}
                        \location & \Loc, \\
                        \sends    & \FinSet(\GName), \\
                        \recvs    & \FinSet(\GName), \\
                        \prev     & \Option(\nodeStruct)
                    \end{array} \\
                    \}
                \end{array}
                &
                \begin{array}[t]{@{}l@{}}
                    \BName \eqdef{} \{ \\
                    \begin{array}[t]{@{\quad}l@{\phantom{i}:\phantom{i}}l}
                        s & \GName, \\
                        o & \GName, \\
                        r & \GName
                    \end{array} \\
                    \}
                \end{array}
            \end{array}
            \\\\
            \begin{array}{@{}l@{}}
                \chain \colon \nodeStruct \to \List(\nodeStruct) \\
                \chain(n) \eqdef{} \\
                \quad \begin{cases}
                    [n] & \text{if } n.\prev = \mathsf{None} \\
                    n \mathbin{::} \chain(n') & \text{if } n.\prev = \mathsf{Some}(n')
                \end{cases}
            \end{array}
            \\\\
            \begin{array}{@{}l@{}}
                \resources \colon \nodeStruct \to \iProp \to \iProp \\
                \resources(n, P_{\mathit{acc}}) \eqdef{} \\
                \quad \begin{array}{@{}l@{}}
                    \Let P'_{\mathit{acc}} =
                        \left(\displaystyle\smashoperator[r]{\Sep_{\gamma \in n.\sends}} \Exists P. \later P \ast \gamma \Mapsto P\right) \wand
                        \left(\left(\displaystyle\smashoperator[r]{\Sep_{\gamma \in n.\recvs}} \Exists P. \later P \ast \gamma \Mapsto P\right) \ast P_{\mathit{acc}}\right) in \\
                    \begin{cases}
                        P'_{\mathit{acc}} & \text{if } n.\prev = \mathsf{None} \\
                        \resources(n', P'_{\mathit{acc}}) & \text{if } n.\prev = \mathsf{Some}(n')
                    \end{cases}
                \end{array}
            \end{array}
            \\\\
            \begin{array}{@{}l@{}}
                I \colon \BName \to \iProp \\
                I(\gamma) \eqdef{} \Exists n_{\mathit{hd}}.
                    \begin{array}[t]{@{}l@{\ }l@{}}
                        \ownGhost{\gamma.s}{\authfull \left(\bigcup\, \{ n.\location \mapsto n.\sends \mid n \in \chain(n_{\mathit{hd}}) \}\right)} \ast {}
                            & \textlabel{formalization-inv-part-sends}{(1)} \\
                        \ownGhost{\gamma.r}{\authfull \left(\biguplus\, \{ n.\location \mapsto n.\recvs \mid n \in \chain(n_{\mathit{hd}}) \}\right)} \ast {}
                            & \textlabel{formalization-inv-part-recvs}{(2)} \\
                        \orderOwnAuth{\gamma.o}{[ n.\location \mid n \in \chain(n_{\mathit{hd}})]} \ast {}
                            & \textlabel{formalization-inv-part-order}{(3)} \\
                        \resources(n_{\mathit{hd}}, \TRUE) \ast {}
                            & \textlabel{formalization-inv-part-resources}{(4)} \\
                        \left(\displaystyle\Sep_{n \in \chain(n_{\mathit{hd}})} \begin{cases}
                            n.\location \mapsto_{\always} 0 \ast \phantom{aaaa\rvert\,} (n.\location \Ptradd 1) \mapsto \lceil n.\prev.\location \rceil & \text{if } \lvert n.\sends \rvert = 0 \\
                            n.\location \mapsto \lvert n.\sends \rvert \ast (n.\location \Ptradd 1) \mapsto \lceil n.\prev.\location \rceil & \text{otherwise} \\
                        \end{cases}\right)
                            & \textlabel{formalization-inv-part-physical}{(5)}
                    \end{array}
            \end{array}
            \\\\
            \begin{array}{@{}l@{}}
                \sendName \colon \Val \to \iProp \to \iProp \\
                \send{\loc}{P} \eqdef{} \Exists \gamma, \gamma_P, P'.
                    \begin{array}[t]{@{}l@{}}
                        \loc \in \Loc
                        \ast (P \wand P')
                        \ast \ownGhost{\gamma.s}{\authfrag \{\loc \mapsto \{\gamma_P\}\}} \ast{} \\
                        \gamma_P \Mapsto P'
                        \ast \meta(\loc, \namesp, \gamma)
                        \ast \knowInv{\namesp}{I(\gamma)}
                    \end{array}
            \end{array}
            \\\\
            \begin{array}{@{}l@{}}
                \recvName \colon \Val \to \iProp \to \iProp \\
                \recv{\loc}{P} \eqdef{} \Exists \gamma, \gamma_P, P'.
                    \begin{array}[t]{@{}l@{}}
                        \loc \in \Loc
                        \ast (P' \wand P)
                        \ast \ownGhost{\gamma.r}{\authfrag \{\loc \mapsto \{\gamma_P\}\}} \ast{} \\
                        \gamma_P \Mapsto P'
                        \ast \meta(\loc, \namesp, \gamma)
                        \ast \knowInv{\namesp}{I(\gamma)}
                    \end{array}
            \end{array}
            \\\\
            \begin{array}{@{}l@{}}
                \earlier \colon \Val \to \Val \to \iProp \\
                \loc \earlier \loc' \eqdef{} \Exists \gamma.
                    \loc \in \Loc
                    \ast \loc' \in \Loc
                    \ast \meta(\loc, \namesp, \gamma)
                    \ast \meta(\loc', \namesp, \gamma)
                    \ast \orderOwnOrdered{\gamma.o}{\loc'}{\loc}
            \end{array}
        \end{array}
    \end{mathpar}
    \caption{Definitions}
    \label{fig:formal-defs}
\end{figure}
The complete definitions can be found in Figure~\ref{fig:formal-defs}.
Let us start of by discussing the logical state.
The $\nodeStruct$ record is similar to the one from~\S\ref{sec:chainable-verification}.
The only difference is that the $\sends$ and $\recvs$ fields no longer have type $\FinMSet(\iProp)$,
but rather $\FinSet(\GName)$. This is because we use \emph{saved propositions}~\parencite[Section~5.1]{Dodds_et_al_2016}
to keep track of the resources that still needs to be sent or received.
That means that $\sends$ is a finite set of ghost names, where each ghost name corresponds with a resource that still needs to be sent.
There can be multiple ghost names with the same associated resource, so we can still send the same resource multiple times.
The $\recvs$ field stores the same information, just for resources that still need to be received.
Again, the counter of a node is stored implicitly in this record, as it corresponds to the size of $\sends$.

Now that we have a notion of a logical state, let us look at the barrier invariant $I$.
It takes a structure $\gamma$ which contains three ghost names.
We use $\gamma.s$ to store the resources which still have to be sent,
$\gamma.r$ to store the resources which still have to be received, and $\gamma.o$ to store the order of elements in the chain.
The invariant starts by extensionally quantifying over a node $n_{\mathit{hd}}$, which represents the head of the chain.
The rest of the invariant can be split into five separate parts.
Parts~\ref{formalization-inv-part-sends},~\ref{formalization-inv-part-recvs} keep track of all the resources that still have to be sent or received.
We use $\chain(n_{\mathit{hd}})$ to get an ordered list of all the nodes in the chain,
and map all their locations to their corresponding $\sends$ and $\recvs$ sets.
We use the $\operatorname{Auth}(\FinMap(\Loc, (\FinSet(\GName), \uplus)))$ resource algebra to store these.
This allows us to add new resources with fresh identifiers, and remove resources if we have ownership of some fragment $\authfrag \{ \loc \mapsto \gamma_P \}$.
It is important to note that we use a disjoint union $\uplus$ in~\ref{formalization-inv-part-recvs}.
This asserts that the values of the map, i.e. $n.\recvs$ for $n \in \chain(n_{\mathit{hd}})$, are distinct.
This is essential for the \emph{passing on} of resources we discussed in~\S\ref{sec:intuitive-receiving}.
In this case we need to pass on an identifier $\gamma_P$, which is only possible whenever $\gamma_P$ is not yet part of the $\recvs$ field of the previous node.
We can keep all $n.\recvs$ sets distinct, as we can always allocate saved propositions with fresh identifiers.

In~\ref{formalization-inv-part-order}, the invariant asserts the authoritative ordering of the locations in the chain.
It is important to note that $\chain(n)$ returns a list where the order of the nodes is reversed,
i.e. the last node in the list corresponds to the \emph{earliest} node.
We take this into account in the definition of the $\earlier$-predicate.

In~\ref{formalization-inv-part-resources}, the resource invariant is asserted using $\resources(n, \TRUE)$.
It is similar to how we defined it for chains with a maximal length of 2 in~\S\ref{sec:chainable-verification-invariant},
but it has a few technical differences. First, the big separating conjunctions ranges over ghost names $\gamma$ due to the use of saved propositions.
We must therefore extensionally quantify over a resource $P$ which is saved at $\gamma$, as asserted by $\gamma \Mapsto P$.
Second, all resources are guarded by a $\later$ modality.
This is because saved propositions do not agree directly, as can be seen in the following rule:
\begin{mathpar}
    \axiomH{Saved Prop-Agree}
        {\gamma \Mapsto P \ast \gamma \Mapsto Q \proves \later (P \leftrightarrow Q)}
\end{mathpar}
By guarding all resources with a $\later$ modality, we can use their agreement at a later step index.
This has the consequence that the resource received in $\waitName$ is received under a $\later$.
However, the resource is received after the previous node is read.
Afterwards the program still does some steps, which allows us to eliminate the $\later$ modality.
Finally, to allow for chains of arbitrary size, the $\resources$ function is recursive.
It uses a resource accumulator $P_{\mathit{acc}}$, which is nested under a wand each recursive call.
This is what we call a \emph{recursively nested wands} construction.
It has the consequence that the $n_{\mathit{hd}}.\recvs$ resources are nested under a wand for each earlier node.
This is exactly the behavior we want, as this means all the $\sends$ resources of the earlier nodes have to be sent,
before we can receive any resource of $n_{\mathit{hd}}.\recvs$.

Finally, the invariant relates the logical state to the physical state in~\ref{formalization-inv-part-physical}.
It holds the points-to predicates of all the nodes in the chain, asserting their counter and previous values.
We write $\lceil n.\prev.\location \rceil$ to lift the location of the optional previous node from the meta logic to a HeapLang optional location value.
Once a counter has become 0, it can never increase again.
We therefore make its points-to predicate persistent in that case.
The reason why we do this, has to do with the implementation of $\waitName$.
In Figure~\ref{fig:chainable-barrier-implementation}, we can see that we read both $\mathit{counter}$ and $\mathit{prev}$,
with two separate reads. We thus need to open the invariant twice,
and we want to be sure that the counter is still 0 when we do the second read.
This then allows us to either pass on the resource to the previous node,
or extract it from the resource invariant and give it to the postcondition,
like we discussed in~\S\ref{sec:intuitive-receiving}.

The $\sendName$ and $\recvName$ predicates are similar to each other.
We first discuss the $\sendName$ predicate, and then discuss the differences with $\recvName$.
The $\send{\loc}{P}$ predicate states that the value $\loc$ is actually a location.
It furthermore extensionally quantifies over $\gamma, \gamma_P$ and $P'$.
The $\gamma$ is used to assert the barrier invariant $\knowInv{\namesp}{I(\gamma)}$.
The resource $P'$ is saved at $\gamma_P$, as asserted by $\gamma_P \Mapsto P'$.
It relates to $P$ by $P \wand P'$.
This allows to verify \ruleref{Send-Strengthen-Chainable}, as $P$, the resource we send,
may be stronger than the stored resource $P'$.
Ownership of $\ownGhost{\gamma.s}{\authfrag \{\loc \mapsto \{\gamma_P\}\}}$
indicates that $\gamma_P$ is indeed one of the identifiers which has not been sent yet.
Due to the saved proposition we thus know that $P'$ is part of the resource invariant.
Finally, we need to make sure that when two nodes are in the same chain, they agree on the same invariant,
and thus, agree on the same value for $\gamma$.
We do this using $\meta$ predicates, which allows us to associate logical data with locations~\parencite[\texttt{iris/base\_logic/lib/gen\_heap.v}]{iris_coq}.
We explain how this asserts the agreement on $\gamma$ record, when we discuss the $\earlier$-predicate.

The differences between the $\sendName$ and $\recvName$ predicates are subtle.
To verify \ruleref{Recv-Weaken-Chainable}, we assert $P' \wand P$,
as $P$, the resource a client receives, may be weaker than the stored resource $P'$.
Furthermore, the ownership of $\ownGhost{\gamma.s}{\authfrag \{\loc \mapsto \{\gamma_P\}\}}$ is changed
to ownership of $\ownGhost{\gamma.r}{\authfrag \{\loc \mapsto \{\gamma_P\}\}}$
(the ghost name is changed from $\gamma.s$ to $\gamma.r$),
as $\recv{b}{P}$ should assert that $P$ is yet to be received.

The $\loc \earlier \loc'$ predicate again states that the values $\loc, \loc'$ are actual locations.
It also asserts ownership of $\orderOwnOrdered{\gamma.o}{\loc'}{\loc}$.
Note that we swapped the order here: $\loc \earlier \loc'$ asserts that $\orderOwnOrdered{\gamma.o}{\loc'}{\loc}$.
This is because, as explained earlier, the authoritative ordering in the invariant~\ref{formalization-inv-part-order} is reversed.
Finally, we use $\meta$ predicates to associate $\gamma$ with both locations.
Two $\meta$ predicates always agree on the associated data with a location:
\begin{gather*}
    \meta(\loc, \namesp, \gamma) \ast \meta(\loc, \namesp, \gamma') \proves \gamma = \gamma'
\end{gather*}
Thus, if we have ownership of $\recv{\loc}{P}$, $\send{\loc'}{Q}$ and $\loc \earlier \loc'$, like in the verification of \ruleref{Renunciation-Chainable},
we can conclude that both nodes agree on the same invariant, as $\loc \earlier \loc'$ states that they are both associated with the same $\gamma$.
