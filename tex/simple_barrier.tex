\chapter{Simple Barrier}\label{chapter:simple_barrier}
\newcommand{\parName}{\textlang{par}}

In this chapter we give an implementation of a simple barrier~(\S\ref{sec:simple-barrier-implementation}).
This implementation is a direct port of the one given in~\parencite[Figure~8]{Dodds_et_al_2016}
and was also used in~\parencite[Figure~9]{ho_ghost_state}.
We proceed by giving the specification~(\S\ref{sec:simple-barrier-specification}) presented in~\parencite[Section~4]{Dodds_et_al_2016}.
Finally, we discuss how we could verify this specification~(\S\ref{sec:simple-barrier-verification})
and conclude with a proof sketch of the specification.
Our verification and proof go in a different fashion than those in~\parencite{Dodds_et_al_2016,ho_ghost_state},
as we do not use \emph{state transition systems}.%
\footnote{We discuss this difference in Chapter~\ref{relatedwork}}
This chapter mainly serves as an Iris tutorial.
Readers familiar with both Iris and barriers may want to skip this chapter.

\section{Implementation}\label{sec:simple-barrier-implementation}
We start by giving an implementation for a simple barrier in HeapLang.
We define 3 methods: $\newbarrierName$ creates a new barrier,
$\signalName$ gets a barrier and signals it,
and $\waitName$ also gets a barrier and waits until the barrier is signalled.
The code of this barrier is straightforward. We only need to store whether our barrier has been signalled yet.
That means that $\newbarrierName$ can return a reference to $\False$, $\signalName$ updates this reference to $\True$
and $\waitName$ recursively calls itself until the reference is set to $\True$.
\begin{align*}
    \newbarrierName &\eqdef \Lam \any. \Alloc (\False) \\
    \signalName &\eqdef \Lam b. b \gets \True \\
    \waitName &\eqdef \Rec \mathit{wait} (b)= \If \deref b then \TT \Else \mathit{wait}\spac b
\end{align*}

\newcommand{\exampleName}{\textlang{example}}
\newcommand{\expensiveBoolName}{\textlang{expensive\_bool}}
\newcommand{\expensiveIntName}{\textlang{expensive\_int}}
Before we give the specification, let us look at a motivating example.
Suppose we have two computationally expensive functions $\expensiveBoolName$ and $\expensiveIntName$,
which return a boolean and an integer respectively. Now consider the following code:
\begin{gather*}
    \begin{array}[t]{@{}l@{}}
        \exampleName \eqdef{} \\
        \quad \Lam r.
        \begin{array}[t]{@{}l@{}}
            \begin{array}[t]{@{}l@{}}
                \Let x = \Alloc(\False) in \\
                \Let b = \newbarrier{\TT} in
            \end{array} \\
            \left(\begin{array}{l||l}
                \begin{array}{@{}l@{}}
                    x \gets \expensiveBoolName\spac \TT; \\
                    \signal{b}
                \end{array} &
                \begin{array}{@{}l@{}}
                    \Let n = \expensiveIntName\spac \TT in \\
                    \wait{b} \\
                    \Let b' = \deref x in \\
                    \If b' \\
                    \quad then r \gets 2 \cdot n \\
                    \quad \Else r \gets n
                \end{array}
            \end{array}\right)
        \end{array}
    \end{array}    
\end{gather*}
Recall that the double line in the middle is notation for the $\parName$ operation, which executes both expressions in parallel.
The left thread first computes $\expensiveBoolName$ and then signals the barrier to indicate that that computation has finished.
The right threads starts by computing $\expensiveIntName$. It then needs to wait for the left thread to finish,
as it needs to be sure that the $\expensiveBoolName$ computation has finished.
This is achieved by waiting on the barrier. Finally, when it is done waiting,
it checks the value of $x$, and based on that it makes a decision on what to store in $r$.

When we want to prove a specification for this example we can see that both threads need to have access to $x$.
We are thus unable to split our resources which means we cannot use the \ruleref{Hoare-Par} rule directly.
However, we as the programmer, know that this code is thread safe:
We do not use $x$ in the left thread after the barrier got signalled,
and we only access it in the right thread after we are done waiting on the barrier.
We would thus desire a specification for our barrier where we can transfer the ownership of $x$ from the left to the right thread.


\section{Specification}\label{sec:simple-barrier-specification}
To give a specification for our barrier, we make use of two abstract predicates: $\send{b}{P}$ and $\recv{b}{P}$.
For now, we do not look at their definitions yet, but rather at their use in the specification.
The specifications of our methods are as follows:
\begin{mathpar}
    \axiomhref{New Barrier-Spec}{New Barrier-Spec-Simple}
        {\hoare{\TRUE}{\newbarrier \TT}{\Ret b. \recv{b}{P} \ast \send{b}{P}}}    

    \axiomhref{Signal-Spec}{Signal-Spec-Simple}
        {\hoare{\send{b}{P} \ast P}{\signal{b}}{\TRUE}}

    \axiomhref{Wait-Spec}{Wait-Spec-Simple}
        {\hoare{\recv{b}{P}}{\wait{b}}{P}}
\end{mathpar}
Intuitively, ownership of $\send{b}{P}$ represents the fact that when someone owns resource $P$,
they should be able to signal barrier $b$ and give up ownership of $P$.
Ownership of $\recv{b}{P}$ represents the fact that when someone waits on $b$, they should get ownership of $P$ after $b$ got signalled.
The specification of $\newbarrierName$ just states that it returns a barrier $b$
together with the predicates $\recv{b}{P}$ and $\send{b}{P}$ for a resource $P$ specified by the user.
This makes it possible to verify $\signalName$ and $\waitName$ calls on the returned barrier.

\begin{figure}
    \begin{proofoutline*}
        \RES{r \mapsto v} \\
        \CODE{\Let x = \Alloc(\False) in} \\
        \RES{x \mapsto \False \ast r \mapsto v} \\
        \COMMENT{Use \ruleref{Hoare-Alloc}} \\
        \CODE{\Let b = \newbarrier{\TT} in} \\
        \COMMENT{Use \ruleref{New Barrier-Spec-Simple} with $P \eqdef x \mapsto \True$} \\
        \RES{x \mapsto \False \ast r \mapsto v \ast \send{b}{P} \ast \recv{b}{P}} \\
        \text{(* Give $x \mapsto \False \ast \send{b}{P}$ to the left thread,} \\
        \text{\phantom{(* } and $r \mapsto v \ast \recv{b}{P}$ to the right thread *)} \\
        $\left(\begin{array}{l||l}
            \begin{array}{@{}l@{}}
                \RES{x \mapsto \False \ast\spac \send{b}{P}} \\
                \CODE{x \gets \expensiveBoolName\spac \TT;} \\
                \COMMENT{Spec of $\expensiveBoolName$} \\
                \RES{x \mapsto \True \ast\spac \send{b}{P}} \\
                \CODE{\signal{b}} \\
                \COMMENT{Use \ruleref{Signal-Spec-Simple}} \\
                \RES{\TRUE}
            \end{array} &
            \begin{array}{@{}l@{}}
                \RES{r \mapsto v \ast \recv{b}{P}} \\
                \CODE{\Let n = \expensiveIntName\spac \TT in} \\
                \COMMENT{Spec of $\expensiveIntName$} \\
                \RES{n = 37 \ast r \mapsto v \ast \recv{b}{P}} \\
                \CODE{\wait{b}} \\
                \COMMENT{Use \ruleref{Wait-Spec-Simple}} \\
                \RES{r \mapsto v \ast x \mapsto \True} \\
                \CODE{\Let b' = \deref x in} \\
                \COMMENT{Use \ruleref{Hoare-Load}} \\
                \RES{b' = \True \ast r \mapsto v \ast x \mapsto \True} \\
                \CODE{\langkw{if}\spac b'} \\
                \CODE{\quad \langkw{then}\spac}
                \begin{array}[t]{@{}l@{}}
                    \RES{r \mapsto v \ast x \mapsto \True} \\
                    \COMMENT{Use \ruleref{Hoare-Store}} \\
                    \CODE{r \gets 2 \cdot n} \\
                    \RES{r \mapsto 74 \ast x \mapsto \True} \\
                    \COMMENT{Use \ruleref{Sep-Weaken}} \\
                    \RES{r \mapsto 74}
                \end{array} \\
                \quad \CODE{\Else}
                \begin{array}[t]{@{}l@{}}
                    \RES{\False = \True \ast r \mapsto v \ast x \mapsto \True} \\
                    \COMMENT{Contradiction} \\
                    \CODE{r \gets n} \\
                    \RES{r \mapsto 74}
                \end{array} \\
                \RES{r \mapsto 74}
            \end{array}
        \end{array}\right)$ \\
        \COMMENT{Use \ruleref{Hoare-Par} to combine the postconditions of both threads} \\
        \RES{r \mapsto 74}
    \end{proofoutline*}
   \caption{Proof sketch of example.}\label{fig:simple-example-proof}
\end{figure}

To verify that this specification is indeed strong enough for our example,
we prove the following specification for $\exampleName$:
\begin{gather*}
    \hoare{r \mapsto v}{\exampleName\spac r}{r \mapsto 74}
\end{gather*}
To prove this we assume that $\expensiveBoolName, \expensiveIntName$ return $\True, 37$ respectively:
\begin{gather*}
    \hoare{\TRUE}{\expensiveBoolName\spac \TT}{\Ret b. b = \True} \\
    \hoare{\TRUE}{\expensiveIntName\spac \TT}{\Ret z. z = 37}
\end{gather*}
The decorated program can be found in Figure~\ref{fig:simple-example-proof}.
We start by using \ruleref{Hoare-Alloc} to allocate a reference $x$ and store $\False$ in it.
Before we can use \ruleref{New Barrier-Spec-Simple}, we should decide which resource we want to send and receive.
We can see that the right thread needs access to the points-to predicate of $x$.
As $\expensiveBoolName$ always returns $\True$, we know that $\True$ is stored at $x$ when we call $\signal{b}$.
We thus use
\begin{gather*}
    P \eqdef{} x \mapsto \True
\end{gather*}
as the resource to send and receive.
We can now create a new barrier using \ruleref{New Barrier-Spec-Simple}.
We give $x \mapsto \False \ast \send{b}{P}$ to the left thread and the $r \mapsto v \ast \recv{b}{P}$ to the right.
In the left thread we first compute $\expensiveBoolName$ and use \ruleref{Hoare-Store} to store the result in $x$.
As we now know that $x \mapsto \True$, we can signal the barrier using \ruleref{Signal-Spec-Simple}.
In the right thread we start by computing $\expensiveIntName$.
After that we can wait on the barrier using \ruleref{Wait-Spec-Simple}.
After we are done waiting, the right threads owns $x \mapsto \True$.
This allows us to use \ruleref{Hoare-Load} and conclude that $b' = \True$.
In the then-case, we use \ruleref{Hoare-Store} to store 74 in $r$,
after which we use \ruleref{Sep-Weaken}.
In the else-case, we have a contradiction $\False = \True$.
As we can conclude anything from a contradiction, we can also conclude that $r \mapsto 74$ holds.
As we showed that $r \mapsto 74$ holds after both cases, we can conclude that $r \mapsto 74$ holds after the if statement.
Finally, we conclude by using \ruleref{Hoare-Par} to combine the postconditions of both threads,
and conclude with $r \mapsto 74$ as the final postcondition.


\section{Verification}\label{sec:simple-barrier-verification}
\newcommand{\token}{\operatorname{token}(\gamma)}
We can now define the $\sendName$ and $\recvName$ predicates.
These predicates should relate the physical state, i.e. the values stored on the heap,
to a suitable notion of \emph{logical} or \emph{ghost state}.
This is a type of state which, unlike physical state, is not present during the execution of a program.
Rather, it is only available during the verification of a program.
We can read and write information to this logical state by indexing it with a \emph{ghost name} $\gamma$.
Making use of this logical state, allows us to prove specifications of more complex programs,
where multiple threads may read or write to the same location on the heap.
In this section, we design a logical state for this barrier.
This allows us to define the $\sendName$ and $\recvName$ predicates.
Afterwards, we show that these definitions are indeed correct,
by giving decorated programs to prove the \ruleref{New Barrier-Spec-Simple},
\ruleref{Signal-Spec-Simple}, \ruleref{Wait-Spec-Simple} rules from~\S\ref{sec:simple-barrier-specification}.

Both $\signalName$ and $\waitName$ need to read or write to the stored boolean.
We can thus see that both the $\sendName$ and $\recvName$ predicates need knowledge about the points-to predicate of the stored boolean.
As we cannot split this points-to predicate,%
\footnote{Technically we can, but we would get two read only points-to predicates,
    i.e. ${\loc \xmapsto{1/2} b \ast \loc \xmapsto{1/2} b}$,
    which means we can no longer store new values~\parencite{Boyland2003,Bornat_et_al_2005}.}
we cannot give it to both $\send{b}{P}$ and $\recv{b}{P}$.

In situations like this, we can use an invariant.
The proposition $\knowInv{\namesp}{I}$ states that proposition $I$ holds at any moment, hence being an invariant.
Each invariant has a namespace $\namesp$, which use we discuss later.
To allocate an invariant we use the following rule:
\begin{mathpar}
    \inferH{Hoare-Inv-Alloc}
        {\hoare{\knowInv{\namesp}{I} \ast P}{e}{\Ret v. Q}}
        {\hoare{I \ast P}{e}{\Ret v. Q}}
\end{mathpar}
At first it does not seem like this rule is useful.
However, invariants are duplicable:
$\knowInv{\namesp}{I}$ states that $I$ always holds,
so duplicating this fact is sound. We thus get the following rule:
\begin{mathpar}
    \axiomH{Inv-Dup}{\knowInv{\namesp}{I} \proves \knowInv{\namesp}{I} \ast \knowInv{\namesp}{I}}
\end{mathpar}
This means we can give the invariant to multiple threads.
This obviously comes at a cost on how we can access $I$, the resource inside the invariant.
Any thread may want to temporarily break the invariant, do some computations and then satisfy it again.
The problem is that this could mean that the invariant is also broken for all other threads, who may do some computations while the invariant is broken.
This would break the promise that the invariant holds at any time.
To prevent this, we require that $\knowInv{\namesp}{I}$ can only be broken, or rather opened, around atomic expressions.
Atomic expressions reduce to a value in one step. That means that no other threads execute during an atomic expression so we can safely open the invariant.
This gives rise to the following rule:
\begin{mathpar}
    \inferH{Hoare-Inv-Open}
        {\atomic(e) \and \namesp \subseteq \mask \and \hoare{\later I \ast P}{e}{\Ret v. \later I \ast Q}[\mask \setminus \namesp]}
        {\hoare{\knowInv{\namesp}{I} \ast P}{e}{\Ret v. \knowInv{\namesp}{I} \ast Q}[\mask]}
\end{mathpar}
The essential part of this rule is that we can temporarily open invariants around atomic expressions.
It furthermore has some technicalities, which we discuss now.
Opening the same invariant twice could lead to inconsistencies.
To prevent this we use a mask $\mask$.
Hoare triples are written with a mask subscript, indicating which invariants may still be opened in its proof.%
\footnote{Note that we typically do not write this subscript in proof rules when the mask is $\top$, indicating that no invariants are currently open.}
When opening an invariant we do not directly get access to $I$, rather to $\later I$.
The $\later$ is a \emph{later} modality. Intuitively, $\later P$ means that $P$ holds after one program step.
Without the $\later$ guard in \ruleref{Hoare-Inv-Open}, Iris would become inconsistent~\parencite[\S 8.2]{iris_from_the_ground_up}.
Therefore, Iris internally uses \emph{step indexing}~\parencite{Appel_McAllester_2001, Birkedal_step_indexing},
which results in these $\later$ modalities appearing in the rule.
However, in many cases we can get rid of these $\later$ modalities as we discuss in~\S\ref{sec:simple-signal-proof}.
To introduce a later modality, one can use the following rule:
\begin{mathpar}
    \axiomH{Later-Intro}{P \proves \later P}
\end{mathpar}
The idea behind this rule is that if $P$ already holds,
then it will also hold after a program step.

Going back to the specification of our barrier, we need to define a suitable invariant.
We can see that for each barrier there can be three possible states:
\begin{enumerate}[label=\arabic*.]
    \item The barrier is not signalled yet.
    \item The barrier is signalled but the resource is not received yet.
    \item The barrier is signalled and the resource is received.
\end{enumerate}
In the first state the $\False$ is stored in the reference, while in the other two states, $\True$ is stored.
To distinguish the last two states, we notice that when the barrier gets signalled, $P$ is given up by the sender.
We can thus store this resource in the invariant. When transitioning to the last state, we want to transfer this ownership from the invariant to the receiver.
However, the receiver should only be able to receive $P$ once, otherwise we could for example duplicate points-to predicate by receiving them multiple times.
We therefore make use of an exclusive token, denoted as $\token$.%
\footnote{
    In this chapter we consider $\token$ as a primitive.
    In Iris, it is defined using \emph{ghost state}.
    We could formally define it, using the \emph{exclusive} RA,
    as $\token \eqdef \ownGhost{\gamma}{\exinj \TT}$~\parencite[\S 3]{iris_from_the_ground_up}.}
If a receiver owns this token, they can exchange it with the invariant for the resource $P$.
This makes sure they can only receive $P$ once.
The exclusivity of $\token$ refers to the following rule:
\begin{mathpar}
    \axiomH{Token-Exclusive}
        {\token \ast \token \proves \FALSE}
\end{mathpar}
This is similar to the points-to predicate where we have
\begin{gather*}
    \loc \mapsto v \ast \loc \mapsto v' \proves \FALSE
\end{gather*}
We can allocate an exclusive token as follows:
\begin{mathpar}
    \inferH{Token-Alloc}
        {\hoare{P \ast \Exists \gamma. \token}{e}{\Ret v. Q}}
        {\hoare{P}{e}{\Ret v. Q}}
\end{mathpar}
The $\gamma$ one gets by this rule is a ghost name, which, as we discussed earlier, serves as a key into the logical state.

We can now define the invariant:
\begin{gather*}
    I(\loc, \gamma, P) \eqdef
        \underbrace{\loc \mapsto \False}_{\substack{\text{Barrier is not} \\ \text{signalled yet}}}
        \lor \underbrace{(\loc \mapsto \True \ast P)}_{\substack{\text{Barrier is signalled,} \\ \text{P is not received yet}}}
        \lor \underbrace{(\loc \mapsto \True \ast \token)}_{\substack{\text{Barrier is signalled,} \\ \text{P is received}}}
\end{gather*}
The three disjuncts corresponds with the three states we described above.
To transition from the first to the second state, the reference should be updated to $\True$ and $P$ should be given to the invariant.
This transition happens when $\signalName$ gets called, as the reference will be updated and $P$ is provided via the precondition of \ruleref{Signal-Spec-Simple}.
To transition from the second to the last state, one should provide $\token$ and gets $P$ in return.
This transition will be done by the receiver, so we initially give the ownership of $\token$ to the receiver.
Although this invariant exactly describes the barrier's state system,
it can be a bit cumbersome to work with because each time we open the invariant, we have to do a case distinction just to get the points-to predicate.
We thus use the following invariant, which is logically equivalent, but a bit nicer to work with:
\begin{gather*}
    I(\loc, \gamma, P) \eqdef \Exists b. \loc \mapsto b \ast (\If b then (\token \lor P) \Else \TRUE)
\end{gather*}
Note that the we use a meta level (Coq) if statement here, not the one from the HeapLang program syntax.

We can now use this invariant to define our $\sendName$ and $\recvName$ predicates.
Both of them should have type $\Val \rightarrow \iProp \rightarrow \iProp$,
as they take the value returned by $\newbarrier$ and the resource being received or sent.
We define them as follows:
\begin{align*}
    \send{\loc}{P} &\eqdef \Exists \gamma. \loc \in \Loc \ast \knowInv{\namesp}{I(\loc, \gamma, P)} \\
    \recv{\loc}{P} &\eqdef \Exists \gamma. \loc \in \Loc \ast \token \ast \knowInv{\namesp}{I(\loc, \gamma, P)}
\end{align*}
Both predicates assert that the value should be a location and that the invariant should hold.
Furthermore, $\recv{\loc}{P}$ has ownership of $\token$.
This can be used to transition from the second to the last state and receive $P$.
We can also be sure that the invariant is never in state 3 when we have a $\recvName$ predicate:
otherwise both the invariant and $\recv{\loc}{P}$ own $\token$, allowing us to prove $\FALSE$ using \ruleref{Token-Exclusive}.

With these definitions, we can proceed to verify the specification
by giving a decorated program for each of $\newbarrierName, \signalName, \waitName$.


\subsection[Verification of New Barrier-Spec]{Verification of \ruleref{New Barrier-Spec-Simple}}

\begin{figure}[t]
    \begin{proofoutline*}
        \RES{\TRUE} \\
        \CODE{\Alloc (\False)} \\
        \COMMENT{Use \ruleref{Hoare-Alloc}} \\
        \RES{\Ret \loc. \loc \mapsto \False} \\
        \COMMENT{Use \ruleref{Token-Alloc}} \\
        \RES{\Ret \loc. \loc \mapsto \False \ast \token} \\
        \COMMENT{Fold definition of $I$} \\
        \RES{\Ret \loc. I(\loc, \gamma, P) \ast \token} \\
        \COMMENT{Use \ruleref{Hoare-Inv-Alloc}} \\
        \RES{\Ret \loc. \knowInv{\namesp}{I(\loc, \gamma, P)} \ast \token} \\
        \COMMENT{Use \ruleref{Inv-Dup}} \\
        \RES{\Ret \loc. \knowInv{\namesp}{I(\loc, \gamma, P)} \ast \knowInv{\namesp}{I(\loc, \gamma, P)} \ast \token} \\
        \COMMENT{Fold definitions of $\sendName, \recvName$} \\
        \RES{\Ret \loc. \send{\loc}{P} \ast \recv{\loc}{P}}
    \end{proofoutline*}
   \caption{$\newbarrierName$ proof sketch.}\label{fig:simple-newbarrier-proof}
\end{figure}

The decorated program is given in Figure~\ref{fig:simple-newbarrier-proof}.
We start by allocating a location where $\False$ is stored.
We then allocate the exclusive token using \ruleref{Token-Alloc}.
We are now ready to allocate the invariant. To do so, we first need to prove $I(\loc, \gamma, P)$.
This holds because we have $\loc \mapsto \False$.
Now that we know $\knowInv{\namesp}{I(\loc, \gamma, P)}$, we can duplicate it using \ruleref{Inv-Dup}.
Finally we can split our resources and prove
\begin{gather*}
    \knowInv{\namesp}{I(\loc, \gamma, P)} \proves \send{\loc}{P} \\
    \knowInv{\namesp}{I(\loc, \gamma, P)} \ast \token \proves \recv{\loc}{P}
\end{gather*}
by instantiating the existential quantifiers and framing.

\subsection[Verification of Signal-Spec]{Verification of \ruleref{Signal-Spec-Simple}}
\label{sec:simple-signal-proof}
\begin{figure}[t]
    \begin{proofoutline*}
        \RES{\send{\loc}{P} \ast P} \\
        \COMMENT{Unfold $\sendName$} \\
        \RES{\knowInv{\namesp}{I(\loc, \gamma, P)} \ast P}
        \IND
            \COMMENT{Open invariant} \\
            \RES{\later I(\loc, \gamma, P) \ast P} \\
            \COMMENT{Unfold $I$} \\
            \RES{\later (\Exists b. \loc \mapsto b \ast (\If b then (\token \lor P) \Else \TRUE)) \ast P} \\
            \COMMENT{Distribute $\later$} \TAGL{later-commuting-rules} \\
            \RES{\later (\loc \mapsto b) \ast \later (\If b then (\token \lor P) \Else \TRUE) \ast P} \\
            \COMMENT{Timelessness of $\loc \mapsto b$} \TAGL{timelessness} \\
            \RES{\loc \mapsto b \ast \later (\If b then (\token \lor P) \Else \TRUE) \ast P} \\
            \CODE{\loc \gets \True} \\
            \COMMENT{Use \ruleref{Hoare-Store} and \ruleref{Hoare-Later-Eliminate}} \TAGL{program-step} \\
            \RES{\loc \mapsto \True \ast (\If b then (\token \lor P) \Else \TRUE) \ast P} \\
            \COMMENT{Weaken} \\
            \RES{\loc \mapsto \True \ast P} \\
            \COMMENT{Introduce $\lor$} \\
            \RES{\loc \mapsto \True \ast (\token \lor P)} \\
            \COMMENT{Fold $I$} \\
            \RES{I(\loc, \gamma, P)} \\
            \COMMENT{Use \ruleref{Later-Intro}} \\
            \RES{\later I(\loc, \gamma, P)} \\
            \COMMENT{Close invariant}
        \UNIND
        \RES{\knowInv{\namesp}{I(\loc, \gamma, P)}} \\
        \COMMENT{Weaken} \\
        \RES{\TRUE}
    \end{proofoutline*}
   \caption{$\signalName$ proof sketch.}\label{fig:simple-signal-proof}
\end{figure}

\newcommand{\timelessName}{\textlog{timeless}}
The decorated program is given in Figure~\ref{fig:simple-signal-proof}.
We start by destructing the $\sendName$ predicate and beta reducing the lambda.
We now want to prove $\hoare{\knowInv{\namesp}{I(\loc, \gamma, P)} \ast P}{\loc \gets \True}{\TRUE}$
As storing a value is an atomic expression, we can open up the invariant.
We now want to store $\True$ in $\loc$ but this is not possible yet.
If we unfold $I$, we can see that we have the following:
\begin{gather*}
    \later (\Exists b. \loc \mapsto b \ast (\If b then (\token \lor P) \Else \TRUE))
\end{gather*}
but we need $\loc \mapsto b$ before we can store a value in $\loc$.
This $\later$ modality is introduced by the \ruleref{Hoare-Inv-Open} rule.
We earlier stated that we can get rid of this later modality if most cases, which we now explain.
There are basically three ways to eliminate later modalities:%
\footnote{Since Iris 4.0, there is also a fourth way, later credits~\parencite{Spies_et_al_2022},
    but we do not discuss that method in this thesis.}
\begin{enumerate}
    \item Commuting rules for $\later$
    \item Timelessness
    \item Program steps
\end{enumerate}
These three ways correspond to the numbers is Figure~\ref{fig:simple-signal-proof}.
We first distribute the $\later$ using the following commuting rules:%
\footnote{We normally do not write the domain of an existential variable,
    but for the \ruleref{Later-Exists} rule it is needed as that type should have an inhabitant.}
\begin{mathpar}
    \inferH{Later-Exists}
        {\tau \text{ is inhabited} \and \later \Exists x : \tau. P}
        {\Exists x : \tau. \later P}

    \inferHB{Later-Sep}
        {\later (P \ast Q)}
        {\later P \ast \later Q}
\end{mathpar}
We now have $\later (\loc \mapsto b)$. To strip the later here we can make use of the fact that points-to predicates are $\timelessName$.
Intuitively, a proposition $P$ is $\timelessName$, written as $\timeless{P}$,
if it is first-order, does not refer to other invariants and does not contain nested Hoare triples~\parencite[\S5.7]{iris_from_the_ground_up}.
Because of this property we can strip the $\later$ from a $\timelessName$ proposition in the precondition using the following rule:
\begin{mathpar}
    \inferH{Hoare-Timeless-Pre}
        {\timeless{P} \and \hoare{P \ast Q}{e}{\Ret v. R}}
        {\hoare{\later P \ast Q}{e}{\Ret v. R}}
\end{mathpar}
We now own $\loc \mapsto b$ which means $\loc$ can be updated.
We are also allowed to remove the only remaining $\later$ as we did a program step.
As explained earlier, $\later P$ intuitively mean that $P$ holds after one program step.
This intuition is captured by the following rule:
\begin{mathpar}
    \inferH{Hoare-Later-Eliminate}
        {e \text{ is not a value} \and \hoare{P}{e}{\Ret v. \Phi(v)}}
        {\hoare{\later R \ast P}{e}{\Ret v. R \ast \Phi(v)}}
\end{mathpar}
We can now prove $I(\loc, \gamma, P)$ and use \ruleref{Later-Intro} to close the invariant.
The postcondition we want to prove is $\TRUE$, so we can conclude by weakening.

\subsection[Verification of Wait-Spec]{Verification of \ruleref{Wait-Spec-Simple}}
\label{sec:simple-wait-proof}
\begin{figure}[t]
    \begin{proofoutline*}
        \RES{\recv{\loc}{P}} \\
        \COMMENT{Unfold $\recvName$} \\
        \RES{\knowInv{\namesp}{I(\loc, \gamma, P)} \ast \token} \\
        \CODE{\langkw{let}\spac f =}
            \begin{array}[t]{@{}l@{}}
                \RES{\knowInv{\namesp}{I(\loc, \gamma, P)} \ast \token} \\
                \COMMENT{Open invariant} \\
                \RES{\later I(\loc, \gamma, P) \ast \token} \\
                \COMMENT{Unfold $I$} \\
                \RES{\later (\Exists b. \loc \mapsto b \ast (\If b then (\token \lor P) \Else \TRUE)) \ast \token} \\
                \COMMENT{Distribute $\later$ and timelessness of $\loc \mapsto b$} \\
                \RES{\loc \mapsto b \ast \later (\If b then (\token \lor P) \Else \TRUE) \ast \token} \\
                \CODE{\deref \loc} \\
                \COMMENT{Dereference $\loc$, remove $\later$ by program step} \\
                \RES{\Ret b. \loc \mapsto b \ast (\If b then (\token \lor P) \Else \TRUE) \ast \token} \\
                \COMMENT{See note \textlabel{simple-barrier-wait-verification-note}{(1)}} \\
                \RES{\Ret b. \loc \mapsto b \ast (\If b then (\token \lor P) \Else \TRUE) \ast (\If b then P \Else \token)} \\
                \COMMENT{Fold $I$} \\
                \RES{\Ret b. I(\loc, \gamma, P) \ast (\If b then P \Else \token)} \\
                \COMMENT{Introduce $\later$} \\
                \RES{\Ret b. \later I(\loc, \gamma, P) \ast (\If b then P \Else \token)} \\
                \COMMENT{Close invariant} \\
                \RES{\Ret b. \knowInv{\namesp}{I(\loc, \gamma, P)} \ast (\If b then P \Else \token)} \\
            \end{array} \\
        \RES{f = b \ast \knowInv{\namesp}{I(\loc, \gamma, P)} \ast (\If b then P \Else \token)} \\
        \CODE{\If f then}
            \IND
            \RES{b = \True \ast \knowInv{\namesp}{I(\loc, \gamma, P)} \ast (\If b then P \Else \token)} \\
            \COMMENT{Reduce if} \\
            \RES{\knowInv{\namesp}{I(\loc, \gamma, P)} \ast P} \\
            \CODE{\TT} \\
            \COMMENT{Weaken} \\
            \RES{P}
            \UNIND
        \CODE{\Else}
            \IND
            \RES{b = \False \ast \knowInv{\namesp}{I(\loc, \gamma, P)} \ast (\If b then P \Else \token)} \\
            \COMMENT{Reduce if} \\
            \RES{\knowInv{\namesp}{I(\loc, \gamma, P)} \ast \token} \\
            \COMMENT{Fold $\recvName$} \\
            \RES{\recv{\loc}{P}} \\
            \CODE{\wait{\loc}} \\
            \COMMENT{Induction hypothesis} \\
            \RES{P}
            \UNIND
        \RES{P}
    \end{proofoutline*}
   \caption{$\waitName$ proof sketch.}\label{fig:simple-wait-proof}
\end{figure}

The decorated program is given in Figure~\ref{fig:simple-wait-proof}.
As $\waitName$ is a recursive function, we need to use induction.
However, unlike the verification of $\appendName$ in Example~\ref{example:list-append-proof},
we do not have a inductive argument to perform induction on.
In such a case, we can use L\"ob induction:
\begin{mathpar}
    \inferhref{L\"ob Induction}{Lob}
        {\later P \proves P}
        {\proves P}
\end{mathpar}
If we want to prove $P$, it is enough to assume the induction hypothesis $\later P$,
and then prove $P$. At first this rule may seem weird, but it makes sense if instantiate it with a Hoare triple:
\begin{gather*}
    \inferrule*[vcenter]
        {\later \left(\hoare{\recv{b}{P}}{\wait{b}}{P}\right) \proves \hoare{\recv{b}{P}}{\wait{b}}{P}}
        {\hoare{\recv{b}{P}}{\wait{b}}{P}}
\end{gather*}
Our induction hypothesis now states that the Hoare triple holds, but under a $\later$ modality.
This means that we can use it, after we did at least one program step.
We can thus use it when we do a recursive call.
This is sound because Hoare triples only assert partial correctness.

In this proof we did a let expansion on $\deref \loc$.
This is because we want to open the invariant which we can only do around an atomic expression.
Using the following rule we can bind to the first argument of a let expression:
\begin{mathpar}
    \inferH{Hoare-Let}
        {\hoare{P}{e_1}{\Ret x. Q} \and \All v. \hoare{Q[v/x]}{e_2[v/x]}{\Ret w. R}}
        {\hoare{P}{\Let x := e_1 in e_2}{\Ret w. R}}
\end{mathpar}
In our proof sketch, this allows us to open up the invariant around $\deref \loc$.

In actual proofs, this method of doing a let expansion is unpractical.
We would have to show that the semantics of our program does not change after this expansion.
One possible method is to restrict our language to only allow programs which are in A-normal form~\parencite{Flanagan_et_al_1993},
which means the programmer has to let expand all their code by hand.
Iris handles this problem is a different way, by introducing evaluation contexts and the following bind rule:
\begin{mathpar}
    \inferH{Hoare-Bind}
        {\text{$K$ is an evaluation context} \and \hoare{P}{e}{\Ret v. Q} \and \All v. \hoare{Q}{K[v]}{\Ret w. R}}
        {\hoare{P}{K[e]}{\Ret w. R}}
\end{mathpar}
In our proof sketch we could take $K$ as $\If \bullet then \TT \Else \wait{\loc}$ and $K[e]$ would fill $e$ into the hole in our context, which is denoted by $\bullet$.
However, this approach does not have a nice representation in our decorated program,
hence why we do the let expansion and use \ruleref{Hoare-Let}.%
\footnote{One may notice that \ruleref{Hoare-Let} is an instantiation of \ruleref{Hoare-Bind}.}

Now that we can open the invariant around $\deref \loc$, we can again distribute the later and use the timelessness of $\loc \mapsto b$.
We dereference $\loc$ and remove a $\later$ because we did a program step.
For~\ref{simple-barrier-wait-verification-note} we argue that the following holds:
\begin{gather*}
    (\If b then (\token \lor P) \Else \TRUE) \ast \token \proves \\
    (\If b then (\token \lor P) \Else \TRUE) \ast (\If b then P \Else \token)
\end{gather*}
We do so by case distinction on $b$.
Suppose $b$ is $\True$. This means that we have $(\token \lor P) \ast \token$.
As we know by \ruleref{Token-Exclusive}, $\token$ is exclusive which means that $P \ast \token$ holds.
We can then prove our goal by giving $\token$ to the left and $P$ to the right hand side of the $\ast$.
When $b$ is $\False$ we need to prove $\TRUE \ast \token \proves \TRUE \ast \token$ which holds trivially.
We can now close the invariant again and continue to the if statement.
If $b$ is $\True$, then we own $P$, so after evaluating $\TT$ we are done.
If $b$ is $\False$, then we still own $\token$.
If we combine this with $\knowInv{\namesp}{I(\loc, \gamma, P)}$ we get $\recv{\loc}{P}$ again.
We now need to execute $\wait{\loc}$.
We can use our induction hypothesis and get $P$ via the postcondition of the recursive call.
As we receive $P$ in both cases of the if statement, we can conclude that afterwards we always own $P$, which concludes the proof.
