\chapter{Introduction}\label{introduction}

Tony Hoare was one of the first to provide a program logic to reason about imperative programs,
in what we now call Hoare logic~\parencite{Hoare_1969}.
In Hoare logic we have Hoare triples $\hoare{P}{e}{Q}$, which should intuitively be read as the following:
if precondition $P$ holds and program $e$ terminates, then postcondition $Q$ holds.
Although this logic worked great for simple imperative languages,
for many decades, there did not seem to be an extension to languages with a heap to store values.
In the early 2000s, Peter O'Hearn, John Reynolds and Hongseok Yang presented separation logic~\parencite{OHearn_et_al_2001,Reynolds_2002}.
This extends Hoare logic with the idea of \emph{ownership} of \emph{resources}.
For example, a program can \emph{own} the points-to resource $\loc \mapsto v$,
which asserts that value $v$ is stored at location $\loc$ in the heap.
Ownership of this resource allows us to verify read and write operations to $\loc$,
with the guarantee that $\loc$ does not get updated by another program.

A few years later, separation logic was extended to reason about concurrent programs~\parencite{OHearn_2007,Brookes_2007}.
Ownership of resources is now given to threads,
allowing a thread to reason about a piece of memory with the guarantee that no other thread changes its content.
This allows for verification of a single thread without one having to worry about interference from other threads.
However, in concurrent programs, it is common that there is some form of communication between threads.
One way to do this, is via the use of barriers.
A programmer can use a barrier to let one thread \emph{wait} until another thread \emph{signals} the first thread to continue.
As an example, let us consider the following program.
\begin{gather*}
    \begin{array}[t]{@{}l@{}}
        \Let \loc = \Alloc(37) in \\
        \Let b = \newbarrier{\TT} in
    \end{array} \\
    \left(
    \begin{array}{l||l}
        \begin{array}{@{}l@{}}
            \wait{b}; \\
            \Let n = \deref \loc in \\
            \loc \gets n + 3
        \end{array} &
        \begin{array}{@{}l@{}}
            \Let n = \deref \loc in \\
            \loc \gets n + 2; \\
            \signal{b}
        \end{array}
    \end{array}
    \right)
\end{gather*}
The program starts by storing $37$ on the heap and returning its location (i.e. a pointer),
after which a new barrier $b$ is created.
It then executes the two programs separated by the double line in separate threads.
The left thread first has to wait until the barrier is signalled.
After that, it reads the current value from $\loc$ and increments it by 3.
The right thread first reads the current value, then increments it by 2, and finally signals the barrier.
The left thread can thus only update $\loc$ after the right thread has finished executing.

To verify programs like these, we need an abstract specification for the barrier.
A client can then use this during verification, without bother thinking about the barrier's implementation details,
which typically involve low-level atomic operations.
\citeauthor{Dodds_et_al_2011} (\citeyear{Dodds_et_al_2011}) presented such a specification
which they verified using \emph{Concurrent Abstract Predicates} (CAP)~\parencite{Dinsdale-Young_et_al_2010},
a separation logic designed to reason about concurrent modules in a abstract way.
They later strengthened the specification in~\parencite{Dodds_et_al_2016}.
They verified three different barrier implementations using \emph{impredicative Concurrent Abstract Predicates} (iCAP)~\parencite{icap},
a descendant from CAP which furthermore allows to reason about higher-order code.

However, the verification of their second barrier implementation requires a lot of bookkeeping,
and has not been mechanized in a proof assistant.
Furthermore, there has been a lot of follow-on work on iCAP.
Iris~\parencite{iris_from_the_ground_up} is one such separation logic,
which is implemented and verified in the Coq proof assistant~\parencite{ipm}.
This allows a user to verify their programs in Coq,
which was not possible for \Citeauthor{Dodds_et_al_2016}.

In this thesis, we verify the first two barrier implementations discussed in~\parencite{Dodds_et_al_2016}.
Our verification is fully formalized in Coq using Iris.
The verification of the first barrier has already been done in Iris~\parencite[\S4]{ho_ghost_state}.
However, our verification uses a different technique compared to \Citeauthor{Dodds_et_al_2016} and \Citeauthor{ho_ghost_state},
which we elaborate in Chapter~\ref{relatedwork}, and also serves as a stepping stone to the more complex verification of the second barrier.
The invariant used in the verification of the second implementation is significantly different from the one presented by \Citeauthor{Dodds_et_al_2016}.
Finally, we add a new $\langkw{clone}$ operation to the second barrier implementation of \Citeauthor{Dodds_et_al_2016},
and extend the specification with a rule for $\langkw{clone}$.

We argue that our verification of the second barrier implementation is cleaner compared to \Citeauthor{Dodds_et_al_2016}.
To achieve this we design a suitable notion of logical state for the barriers.
This allows us to define a different resource invariant using a \emph{recursively nested wands} construction,
which is, to the best of our knowledge, a new construction.
Using this invariant, we are able to eliminate the bookkeeping steps used by \citeauthor{Dodds_et_al_2016},
resulting in cleaner proofs.
Finally, we design a construction to keep track of an ordering using ghost state, called \emph{authoritative ordering}.

It has to be noted that there are some limitations to our work.
First, we implement the barriers in HeapLang.
This is a ML-like language part of Iris, mainly used for program verification.
It has some low-level constructs allowing for fine-grained concurrency,
but it is quite different from C, which would be a more common language to implement low-level constructs like barriers.
Second, we assume a sequentially consistent memory model,
as opposed to a relaxed memory model which is used by multiprocessors nowadays.
However, these limitations are regular in this field of study.

We start this thesis by introducing HeapLang, the programming language we use in this thesis,
and give an introduction to separation logic in Chapter~\ref{preliminaries}.
In Chapter~\ref{chapter:simple_barrier}, we verify the simple barrier implementation given in~\parencite[Section~4]{Dodds_et_al_2016}.
This chapter mainly serves as an introduction to Iris.
Chapter~\ref{chapter:chainable-barrier} discusses the barrier implementation that supports chains~\parencite[Section~6]{Dodds_et_al_2016},
and an intuitive verification is presented.
The formalization of this verification is discussed in Chapter~\ref{chapter:formalization}.
Finally, we discuss related work in Chapter~\ref{relatedwork},
and we conclude in Chapter~\ref{conclusions} with future work.

The Coq formalization, accompanied with installation instructions
and references to the relevant section, can be found here~\parencite{coq_formalization}.
